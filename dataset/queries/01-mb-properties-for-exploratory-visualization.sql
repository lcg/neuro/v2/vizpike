SELECT
	unit.unit_id,
    expr.subject,
	site.hemisphere,
	site.area,
	site.depth_in_um,
    spot.cytox_band,
	spot.channel,
    (
        CASE expr_site.channel_mapping
        WHEN 'ROW_MAJOR' THEN mod(spot.channel, expr.channels)
        WHEN 'COL_MAJOR' THEN div(spot.channel, expr.channels)
        END
    ) AS channel_x,
    (
        CASE expr_site.channel_mapping
        WHEN 'ROW_MAJOR' THEN div(spot.channel, expr.channels)
        WHEN 'COL_MAJOR' THEN mod(spot.channel, expr.channels)
        END
    ) AS channel_y,
	det.waveform_samples,
	det.samples_before_alignment,
	src.sampling_in_hz,
	unit."type",
    unit."label",
    unit.basal_rate_in_hertz,
    unit.firing_rate_means_in_hertz,
    unit.firing_rate_deviations_in_hertz,
    unit.waveform_deviation_in_mv,
    unit.waveform_mean_in_mv,
    unit.latency_in_ms,
    unit.peak_response,
    unit.polargram,
    unit.response_cutoff,
    unit.rf_area_in_squared_degree,
    unit.rf_aspect_ratio,
    unit.rf_center_in_degree,
    unit.rf_contour_in_degree,
    unit.rf_convex_area_in_squared_degree,
    unit.rf_eccentricity_in_degree,
    unit.rf_equivalent_diameter_in_degree,
    unit.rf_euler_number,
    unit.rf_major_axis_in_degree,
    unit.rf_minor_axis_in_degree,
    unit.rf_perimeter_in_degree,
    unit.rf_polar_contour_in_degree
FROM derived."Unit" unit
	INNER JOIN original."Spot" spot ON unit.spot_id = spot.spot_id
	INNER JOIN derived."Property" prop ON unit.property_id = prop.property_id
	INNER JOIN original."Site" site ON spot.site_id = site.site_id
	INNER JOIN derived."Sorting" srt ON prop.new_sorting_id = srt.new_sorting_id
	INNER JOIN derived."Detection" det ON srt.detection_id = det.detection_id
	INNER JOIN original."Source" src ON det.source_id = src.source_id
	INNER JOIN original."Experiment" expr ON src.experiment_id = expr.experiment_id
    INNER JOIN original."ExperimentSite" expr_site ON (expr_site.experiment_id = expr.experiment_id AND expr_site.site_id = site.site_id)
    INNER JOIN original."MEA" mea ON mea.mea_id = expr_site.mea_id
WHERE
	prop."type" = 'MOVING_BARS'