FROM python:3.9-slim AS BASE

RUN apt-get update \
    && apt-get install -y \
        git \
        wait-for-it \
    && rm -rf /var/lib/apt/lists/*

RUN pip install "poetry>=1.1"

ARG GID=1000
RUN groupadd \
    --gid ${GID} \
    vizpike

ARG UID=1000
RUN useradd \
    --create-home --home-dir /home/vizpike \
    --gid vizpike --uid ${UID} \
    vizpike

USER vizpike

RUN poetry config virtualenvs.create true
RUN poetry config virtualenvs.in-project false
