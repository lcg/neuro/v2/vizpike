![](logo.svg) VizPike
=====================

[![][version badge]][latest tag]
![][pipeline status]

[[_TOC_]]

## First deployment

These instructions assume you're running on a publicly accessible Ubuntu server (a VPS with properly set DNS entries for it) and that you have root SSH access to it.
Some command arguments refer to hypothetical environment variables, expressed with the `${VAR}` syntax (that is, using curly braces), just for consistency of the explanation.
Refer to the section on [CI variables](#ci-variables) if you're unsure about their meaning.

After the first successful manual deployment, configuring the CI variables correctly should make sure that the applicaiton is updated after pushing to the `develop` and `master` branches.

1. Get system requirements
    * [Docker] &ge; 19.03
    * [Docker Compose] &ge; 3.8
2. Prepare the system:
   1. Create a dedicated user for deployment and add it to the `docker` group:
      ```bash
      adduser ${SSH_USER} && usermod -aG docker ${SSH_USER}
      ```
   1. Switch to the newly-created user (```su ${SSH_USER}```), then
      1. Generate an SSH key-pair:
         ```bash
         ssh-keygen -t rsa
         ```
      1. Append the public key to `$HOME/.ssh/known_hosts`:
         ```bash
         cat ~/.ssh/id_rsa.pub >> ~/.ssh/known_hosts
         ```
      1. Copy the private key (`~/.ssh/id_rsa`) text content to use as the value of `SSH_KEY` variable, then erase the file.
   1. Go back to `root`, then
      1. Make sure the deployment user has writing permission on the `${HOST_DIR}` directory:
         ```bash
         chown ${SSH_USER}:root ${HOST_DIR}
         ```
      1. Make sure port 443 is open to the outside world.
         You may do so by installing the [Apache2] HTTP server, obtaining the SSL certificate using the directions below, then visiting your server over HTTPS.
         You may remove Apache2 aftewards.
      1. Obtain and install a SSL certificate for your domain (using [Certbot], for instance).
3. Prepare the app environment (run everything as the deployment user, unless otherwise stated):
   1. Clone the repository:
      ```bash
      git clone git@gitlab.com:lcg/neuro/v2/vizpike.git "${HOST_DIR}"
      # or git clone https://gitlab.com/lcg/neuro/v2/vizpike.git
      ```
   1. **(As root):** Create a `./ssl` directory, then copy/hardlink SSL certificate files `{cert,chain,privkey}.pem` to it and own the directory to the deployment user, for example:
      ```bash
      # If SSL certificate was obtained via Certbot, it will look like this:
      mkdir -p ssl
      ln /etc/letsecrypt/archive/domain.name/*.pem ssl
      chown -R root:${SSH_USER} ssl
      ```
4. Deploy the app:
   1. Export the `COMPOSE_FILE` environment variable accordingly:
      ```bash
      # In development
      export COMPOSE_FILE=docker-compose.base.yml:docker-compose.dev.yml
      # In production
      export COMPOSE_FILE=docker-compose.base.yml:docker-compose.prod.yml
      ```
   2. Create an `.env` file and configure necessary environment variables (refer to [Environment variables](#environment-variables)).
      **Note:** set the `UID` and `GID` variables to the respective values of the deployment user (get with `id -u` and `id -g`).
   3. Build containers:
      ```bash
      docker-compose build
      ```
   4. Download data files with DVC:
      ```bash
      docker-compose run dashboard bash -c 'poetry install && poetry run dvc pull $(cat dvc-dependencies.txt)'
      ```
      **Note:** if running DVC for the first time, this will require browser authentication, so follow the instructions on the terminal.
      **Note:** this command may report failure for a few files, so just re-run it if that happens.
   5. Initialize the database:
      ```bash
      docker-compose run dashboard poetry install
      docker-compose run dashboard poetry run python -m vizpike db --fresh-start populate
      ```
   6. Create users:
      ```bash
      docker-compose run dashboard poetry run python -m vizpike users create
      ```
   7. Create tasks and assign them to users:
      ```bash
      docker-compose run dashboard poetry run python -m vizpike tasks create --everyone --demo PAIRED_MB_GRATINGS_SORTING_SELECTION
      docker-compose run dashboard poetry run python -m vizpike tasks create --everyone PAIRED_MB_GRATINGS_SORTING_SELECTION
      ```
   8. Precache widgets for improved performance:
      ```bash
      docker-compose run dashboard poetry run python -m vizpike app precache
      ```
      **Note:** in the current implementation, the `dashboard-cache` service's memory usage will peak at approximately 1GB as a result of that command. 
   10. Bring up the app:
       ```bash
       docker-compose up -d
       ```

**Note:** The cache and database services' ports are only exposed in the development configuration, unlike the application port, which is always bound to some port on the host (defined via `VIZPIKE_PORT`). 

## Environment variables

|Variable|Meaning|
|---|---|
|`GID`|Group ID of deployment user. Get with `id -g`. Tipically `1000`.|
|`UID`|User ID of deployment user. Get with `id -g`. Tipically `1000`.|
|`VIZPIKE_CACHE_PORT`|Redis port exposed internally (only for development). Example: 6379.|
|`VIZPIKE_DATABASE_DB`|Name of PostgreSQL database. Example: `vizpike`.|
|`VIZPIKE_DATABASE_PASSWORD`|Password for database user.|
|`VIZPIKE_DATABASE_PORT`|Database port exposed internally (only for development). Example: 5432.|
|`VIZPIKE_DATABASE_USER`|Name of PostgreSQL database user. Example: `vizpike`.|
|`VIZPIKE_HOST_DVC_CACHE_DIR`|Full path of directory used as DVC cache. If you don't change the default, it should be set to `/${HOST_DIR}/.dvc/cache`.|
|`VIZPIKE_IMAGE_PREFIX`|Prefix used for tagging Docker images. Something like `gitlab.com/lcg/neuro/v2/vizpike`.|
|`VIZPIKE_PORT`|Host port that exposes application. In production, probably `443`.|
|`VIZPIKE_SECRET_KEY`|Secret server key. Generate one with `openssl rand -base64 32`.|
|`VIZPIKE_WORKERS`|Number of worker processes. Recommended: 2-4 times the number of CPUs. Defaults to 4. Only used in production. If the workers try to boot, fail, then retry indefinitely (watch the logs with `docker-compose logs -f dashboard`), try decreasing this number.|
|`VIZPIKE_WORKER_THREADS`|Number of threads per process. It makes sense to increase this if it becomes apparent that a high number of concurrent accesses is causing an I/O bottleneck (database access). Defaults to 1. Only used in production.|

## CI variables

The variables below must be setup at the repository/group-level (*settings → CI/CD*) for [Gitlab CI] to work properly.
These variables are also used as placeholders in the [First deployment](#first-deployment) instructions.

|Variable|Type|Example|Explanation|
|---|---|---|---|
|GIT_SSH_KEY|Variable|`/home/gitlab/.ssh/id_rsa`|Path to SSH RSA/DSA key used for git-cloning/pulling on the deployment host.|
|GIT_SSH_PASSWORD|Variable|`5str0ngP@$sW*rd`|Plain-text password of the `GIT_SSH_KEY`. Don't use the example, for the might of the Big Bang!|
|HOST_DIR|Variable|`/app`|Host path to deployment directory, where the repository is cloned.|
|SSH_HOST|Variable|`example.com`|Deployment host URL.|
|SSH_HOST_KEY|File|`example.com ssh-rsa ABCDC5NzbC1xd7FAEEEEAQABAAABgQDMZ...`|SSH RSA/DSA key of deployment host. Obtain it with `ssh-keyscan -H ${SSH_HOST} 2>/dev/null`.|
|SSH_KEY|File|`-----BEGIN OPENSSH PRIVATE KEY-----...`|SSH RSA/DSA private key for logging in to deployment host from the CI. Generate it with `ssh-keygen -t rsa # other options...` on the host, then copy/paste the generated private key (usually `~/.ssh/id_rsa`) here.|
|SSH_PORT|Variable|`22`|Port that the deployment host's SSH server listens on.|
|SSH_USER|Variable|`gitlab`|SSH user account, preferably dedicated to deployment, for security reasons.|

**Beware:** The `GIT_SSH_{KEY,PASSWORD}` variables refer to an SSH key path and its corresponding password residing in the deployment host, whereas the `SSH_KEY` is a private key file used in Gitlab CI' environment to connect to the deployment host (where the corresponding public key was added to `/home/${SSH_USER}/.ssh/authorized_keys`, as detailed in [First deployment](#first-deployment)).

## Maintenance

To back up the database:

```bash
docker-compose run --rm database pg_dumpall -cU postgres | xz -c9 >vizpike.bak.sql.xz
```

To restore it:

```bash
xzcat vizpike.bak.sql.xz | docker-compose run --rm database psql -U postgres -d vizpike
```

A backup file `vizpike.bak.sql.xz` is already provided as a DVC dependency.

[Apache2]: https://httpd.apache.org/docs/
[Certbot]: https://certbot.eff.org/
[Docker]: https://docs.docker.com/
[Docker Compose]: https://docs.docker.com/compose/
[Gitlab CI]: https://docs.gitlab.com/ee/ci/
[latest tag]: https://gitlab.com/psa-exe/dsc/tags/1.0.1
[pipeline status]: https://gitlab.com/lcg/neuro/v2/vizpike/badges/develop/pipeline.svg
[Python 3.8]: https://docs.python.org/3.8/
[version badge]: https://img.shields.io/badge/version-0.1.0-orange.svg
