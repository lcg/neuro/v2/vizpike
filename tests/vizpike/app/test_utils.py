import pytest
from pydantic import BaseModel

from vizpike.app import marshall
from vizpike.app.utils import decode_url, encode_url
from vizpike.types import JSON


@pytest.mark.parametrize(
    "path", ["a", "/a", "a/", "/a/", "a/b", "/a/b", "a/b/", "/a/b/"],
)
@pytest.mark.parametrize(
    "query",
    [
        {
            "a": 42,
            "b": "spam",
            "c": True,
            "d": 3.14,
            "e": [4, 8, 15],
            "f": {"hey": "ho"},
        }
    ],
)
def test_encode_url_and_decode_url_are_inverse_functions(path: str, query: JSON):
    assert decode_url(encode_url(path, query)) == (path.rstrip("/"), query)


def test_GIVEN_function_with_pydantic_typed_args_parameter_WHEN_decorated_with_marshall_THEN_wrapper_function_will_convert_arguments():
    class Model(BaseModel):
        a: int
        b: str

    def function(*models: Model):
        return [m.a * m.b for m in models]

    assert marshall(function)({"a": 2, "b": "ab"}, {"a": 3, "b": "de"}) == [
        "abab",
        "dedede",
    ]


def test_GIVEN_function_with_pydantic_typed_kwargs_parameter_WHEN_decorated_with_marshall_THEN_wrapper_function_will_convert_arguments():
    class Model(BaseModel):
        a: int
        b: str

    def function(**models: Model):
        return {k: v.a * v.b for k, v in models.items()}

    assert marshall(function)(x={"a": 2, "b": "ab"}, y={"a": 3, "b": "de"}) == {
        "x": "abab",
        "y": "dedede",
    }


def test_GIVEN_function_with_pydantic_typed_positional_parameter_WHEN_decorated_with_marshall_THEN_wrapper_function_will_convert_arguments():
    class Model(BaseModel):
        a: int
        b: str

    def function(model: Model):
        return model.a * model.b

    assert marshall(function)({"a": 2, "b": "ab"}) == "abab"


def test_GIVEN_function_with_pydantic_typed_return_WHEN_decorated_with_marshall_THEN_wrapper_function_will_convert_output():
    class Model(BaseModel):
        a: int
        b: str

    def function(n: int) -> Model:
        return Model(a=n, b=n * "ab")

    return_value = marshall(function)(2)

    assert return_value == {"a": 2, "b": "abab"}
    assert type(return_value) is dict
