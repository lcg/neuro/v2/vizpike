from functools import lru_cache
from itertools import chain
from multiprocessing import Pool, cpu_count

from tqdm import tqdm

from vizpike import services
from vizpike.app.app import make_app
from vizpike.app.task1.control import precache_task_item
from vizpike.app.utils import redis_cache
from vizpike.db import get_session
from vizpike.orm.schemas.app import TaskType
from vizpike.orm.schemas.app.json import SortingSelectionTaskDetails

from . import vizpike_cli


@vizpike_cli.group("app")
def app_cli():
    pass


@app_cli.command("run")
def run_app():
    make_app().dash.run_server(host="0.0.0.0", port=8050, debug=True)


@app_cli.command("precache")
def precache():
    db = get_session()
    tasks = services.tasks(db).get_by_type(
        TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION
    )

    with Pool(max(1, cpu_count() - 2)) as pool:
        work_items = list(chain.from_iterable((task.details.items for task in tasks)))

        for _ in tqdm(
            pool.imap_unordered(_precache_process, work_items),
            desc="Task items",
            total=len(work_items),
        ):
            pass

        pool.close()

    redis_cache.save()


def _precache_process(task_item: SortingSelectionTaskDetails.TaskItem):
    try:
        precache_task_item(_get_db(), task_item)
    except TypeError:
        pass


@lru_cache()
def _get_db():
    return get_session()
