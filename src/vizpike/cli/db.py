from typing import Tuple

import click
from pydantic import BaseModel

from vizpike.db import get_engine, tools

from . import vizpike_cli


class CommonOptions(BaseModel):
    echo: bool
    fresh_start: bool


@vizpike_cli.group("db")
@click.option("--echo", is_flag=bool, default=False, help="Echo SQL statements.")
@click.option(
    "--fresh-start", is_flag=bool, default=False, help="Erase and recreate everything."
)
@click.pass_context
def db_cli(ctx: click.Choice, echo: bool, fresh_start: bool):
    ctx.obj = CommonOptions(echo=echo, fresh_start=fresh_start,)


@db_cli.command("init")
@click.pass_obj
def db_init(opt: CommonOptions):
    """Initialize the database, creating all schemas and tables.
    """
    engine = get_engine(echo=opt.echo)
    tools.initialize(engine, fresh_start=opt.fresh_start, logging_function=print)


@db_cli.command("populate")
@click.option(
    "--skip-derived",
    type=click.Choice(tools.entity_csv_to_model_map.keys()),
    multiple=True,
    default=(),
    help="Skip these entity types.",
)
@click.pass_obj
def db_populate(opt: CommonOptions, skip_derived: Tuple[str]):
    """Populate the database with initial data after creating any missing tables.
    """
    engine = get_engine(echo=opt.echo)
    tools.populate(engine, fresh_start=opt.fresh_start, skip_derived=skip_derived, logging_function=print)
