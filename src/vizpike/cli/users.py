import click

from vizpike.db import DBSession, get_session
from vizpike.services import users

from . import vizpike_cli


@vizpike_cli.group("users")
@click.pass_context
def users_cli(ctx: click.Context):
    ctx.obj = db = ctx.with_resource(get_session())

    @ctx.call_on_close
    def commit_and_close_db():
        db.commit()
        db.close()


@users_cli.command("create")
@click.pass_obj
def users_create(db: DBSession):
    users(db).create(
        name=click.prompt(text="Name"),
        email=click.prompt(text="Email"),
        password=_prompted_password_or_random(),
    )


@users_cli.command("reset-password")
@click.argument("EMAIL", type=str)
@click.pass_obj
def users_reset_password(db: DBSession, email: str):
    users(db).reset_password(
        users(db).get_by_email(email), password=_prompted_password_or_random()
    )


def _prompted_password_or_random() -> str:
    return click.prompt(
        text="Password", default=users.random_password(), hide_input=True
    )
