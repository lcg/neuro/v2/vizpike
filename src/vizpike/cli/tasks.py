import lzma
from typing import List, Set, Tuple

import click
import pandas as pd
from prettytable import PrettyTable
from sqlalchemy import inspect
from sqlalchemy.types import Enum
from tqdm import tqdm

from vizpike.db import DBSession, get_session
from vizpike.orm import Base
from vizpike.orm.schemas.app import Task, TaskType
from vizpike.orm.schemas.app.json import ActionSequence
from vizpike.orm.schemas.dataset.derived import Detection, Sorting
from vizpike.orm.schemas.dataset.original import Source
from vizpike.services import sessions, tasks, units, users

from . import vizpike_cli


@vizpike_cli.group("tasks")
@click.pass_context
def tasks_cli(ctx: click.Context):
    ctx.obj = db = ctx.with_resource(get_session())

    @ctx.call_on_close
    def commit_and_close_db():
        db.commit()
        db.close()


_everyone_option = click.option(
    "--everyone",
    "-e",
    is_flag=True,
    default=False,
    help="Apply this operation to all users.",
)


@tasks_cli.command("assign")
@click.argument("TASK_ID", type=int)
@click.argument("USER_IDS", type=int, nargs=-1)
@_everyone_option
@click.pass_obj
def task_assign(db: DBSession, task_id: int, user_ids: Tuple[int], everyone: bool):
    task = tasks(db).get_by_id(task_id)

    if everyone:
        tasks(db).assign_to(task, users(db).get_all())
    else:
        tasks(db).assign_to(task, users(db).get_by_id(*user_ids))


@tasks_cli.command("create")
@click.argument("TASK_TYPE", type=click.Choice([tt.name for tt in TaskType]))
@click.option(
    "--demo",
    "-d",
    is_flag=True,
    default=False,
    help="Create a demonstration task with only 4 channels.",
)
@_everyone_option
@click.pass_obj
def task_create(db: DBSession, task_type, everyone: bool, demo: bool):
    task = tasks(db).create(
        TaskType[task_type], task_kwargs={"mb_id": 21, "gr_id": 22, "demo": demo}
    )

    if everyone:
        tasks(db).assign_to(task, users(db).get_all())


@tasks_cli.command("export")
@click.argument("TASK_IDS", nargs=-1, type=int, required=True)
@click.pass_obj
def task_export(db: DBSession, task_ids: Tuple[int]):
    # TODO: Refactor task export logic (which is task type-specific) to tasks service.
    selected_tasks = list(tasks(db).get_by_id(*task_ids, as_iterable=True))

    users_table = pd.DataFrame(
        [
            {
                "user_id": ut.user_id,
                "user_email": ut.user.email,
                "user_name": ut.user.name,
            }
            for task in selected_tasks
            for ut in task.users
        ]
    ).set_index("user_id")
    tasks_table = pd.DataFrame(
        [
            {
                "task_id": task.id,
                "task_endpoint": task.endpoint,
                "task_num_items": task.length,
                "task_gratings_id": task.details.gratings_experiment_id,
                "task_moving_bars_id": task.details.moving_bars_experiment_id,
                "task_title": task.title,
                "task_description_md": task.description_in_markdown,
            }
            for task in selected_tasks
        ]
    ).set_index("task_id")
    multi_units_table = _multi_units_table(selected_tasks, db)
    single_units_table = _single_units_table(selected_tasks, db)

    task_items_table = pd.DataFrame(
        [
            {
                "task_id": task.id,
                "item_channel": item.channel,
                "item_gratings_ids": item.gratings_ids,
                "item_moving_bars_ids": item.moving_bars_ids,
            }
            for task in tqdm(selected_tasks, desc="Gathering task items")
            for item in task.details.items
        ]
    ).set_index(["task_id", "item_channel"])
    
    user_selections_table = pd.DataFrame(
        [
            {
                "task_id": task.id,
                "user_id": ut.user_id,
                "item_channel": item.channel,
                "selection_finished": item.is_finished,
                "selection_approved_gratings_ids": item.approved_gratings_unit_ids,
                "selection_rejected_gratings_ids": item.rejected_gratings_unit_ids,
                "selection_approved_moving_bars_ids": item.approved_moving_bars_unit_ids,
                "selection_rejected_moving_bars_ids": item.rejected_moving_bars_unit_ids,
            }
            for task in selected_tasks
            for ut in task.users
            for item in ut.details.items
        ]
    ).set_index(["task_id", "user_id", "item_channel"])
    user_actions_table = pd.DataFrame(
        [
            {
                "task_id": task.id,
                "user_id": ut.user_id,
                "item_channel": ch,
                "sequence_started_at": seq.start_timestamp,
                "sequence_finished_at": seq.finish_timestamp,
                "session_uuid": seq.session_uuid,
                "action_type": action.type.value,
                "action_time": action.timestamp,
                "action_target": action.multi_unit_id,
            }
            for task in selected_tasks
            for ut in task.users
            for ch, seq in [
                (ut.details.items[seq.item_index].channel, seq)
                for seq in [
                    ActionSequence.parse_raw(seq) for seq in ut.action_sequences or []
                ]
            ]
            for action in seq.actions
        ]
    )
    user_sessions_table = pd.DataFrame(
        [
            {
                "session_uuid": session.uuid,
                "user_id": session.user_id,
                "session_login_at": session.login_at,
                "session_logout_at": session.logout_at,
            }
            for session in [
                sessions(db).get_by_uuid(uuid)
                for uuid in user_actions_table["sequence_session"].unique()
            ]
    ]).sort_values("session_login_at").set_index("session_uuid")

    user_sessions = [
        sessions(db).get_by_uuid(uuid)
        for uuid in user_actions_table["session_uuid"].unique()
    ]
    user_sessions_table = pd.DataFrame(
        [
            {
                "session_uuid": session.uuid,
                "session_login_at": session.login_at,
                "session_logout_at": session.logout_at,
                "user_id": session.user_id,
            }
            for session in user_sessions
        ]
    ).set_index("session_uuid")

    _write_csv_xz(users_table, "users.csv.xz")
    _write_csv_xz(tasks_table, "tasks.csv.xz")
    _write_csv_xz(multi_units_table, "multi_units.csv.xz")
    _write_csv_xz(single_units_table, "single_units.csv.xz")
    _write_csv_xz(task_items_table, "task_items.csv.xz")
    _write_csv_xz(user_selections_table, "user_selections.csv.xz")
    _write_csv_xz(user_actions_table, "user_actions.csv.xz")
    _write_csv_xz(user_sessions_table, "user_sessions.csv.xz")


@tasks_cli.command("list")
@click.pass_obj
def task_list(db: DBSession):
    all_tasks = tasks(db).get_all()
    table = PrettyTable()
    table.field_names = ["ID", "Endpoint", "Items", "Users", "Title"]
    table.add_rows(
        (
            (task.id, task.endpoint, task.length, len(task.users), task.title)
            for task in all_tasks
        )
    )
    click.echo(table.get_string())


@tasks_cli.command("reset")
@click.argument("TASK_ID", type=int)
@click.argument("USER_IDS", type=int, nargs=-1)
@_everyone_option
@click.pass_obj
def task_reset(db: DBSession, task_id: int, user_ids: Tuple[int], everyone: bool):
    task = tasks(db).get_by_id(task_id)

    if everyone:
        tasks(db).reset_for(task, users(db).get_all())
    else:
        tasks(db).reset_for(task, users(db).get_by_id(*user_ids))


def _entity_scalar_and_enum_columns(entity_type: Base) -> Tuple[Set[str], Set[str]]:
    enum_columns = set()
    scalar_columns = set()

    for entity_type in inspect(entity_type).polymorphic_iterator():
        for column_name, column in entity_type.columns.items():
            if ("hdf" not in column_name) and (column_name[-2:] != "id"):
                if isinstance(column.type, Enum):
                    enum_columns.add(column_name)
                else:
                    scalar_columns.add(column_name)

    return scalar_columns, enum_columns


def _multi_units_table(selected_tasks: List[Task], db: DBSession):
    multi_units = pd.DataFrame(
        [
            {
                "unit_id": unit_id,
                "item_channel": item.channel,
                "stimulus": stimulus,
                "single_unit_count": len(
                    units(db).get_single_unit_splits_for(units(db).get_by_id(unit_id))
                ),
            }
            for task in selected_tasks
            for item in task.details.items
            for stimulus in ["gratings", "moving_bars"]
            for unit_id in getattr(item, f"{stimulus}_ids")
        ]
    ).set_index(["unit_id"])
    _fill_units_table(multi_units, db)
    return multi_units


def _single_units_table(selected_tasks: List[Task], db: DBSession):
    single_units = pd.DataFrame(
        [
            {
                "single_unit_id": unit_id,
                "multi_unit_id": unit_group.multi_unit_id,
                "item_channel": item.channel,
                "stimulus": stimulus,
            }
            for task in selected_tasks
            for item in task.details.items
            for stimulus in ["gratings", "moving_bars"]
            for unit_group in getattr(item, f"{stimulus}_units")
            for unit_id in unit_group.single_unit_ids
        ]
    ).set_index(["single_unit_id"])
    _fill_units_table(single_units, db)
    return single_units


def _fill_units_table(units_table: pd.DataFrame, db: DBSession):
    common_functional_properties = ["basal_rate_in_hertz", "total_spikes"]

    gratings_functional_properties = [
        "active_direction_cv",
        "active_direction_index",
        "active_orientation_cv",
        "active_orientation_index",
        "active_preferred_direction_in_degree",
        "active_preferred_orientation_in_degree",
        "direction_cv",
        "direction_index",
        "orientation_cv",
        "orientation_index",
        "preferred_direction_in_degree",
        "preferred_orientation_in_degree",
    ]

    moving_bars_functional_properties = [
        "latency_in_ms",
        "rf_area_in_squared_degree",
        "rf_aspect_ratio",
        "rf_convex_area_in_squared_degree",
        "rf_eccentricity_in_degree",
        "rf_equivalent_diameter_in_degree",
        "rf_euler_number",
        "rf_major_axis_in_degree",
        "rf_minor_axis_in_degree",
        "rf_peak_disparity",
        "rf_peak_response",
        "rf_perimeter_in_degree",
        "rf_total_response",
        "rf_response_cutoff",
    ]

    all_functional_properties = (
        common_functional_properties
        + gratings_functional_properties
        + moving_bars_functional_properties
    )

    detection_scalar_fields, detection_enum_fields = _entity_scalar_and_enum_columns(
        Detection
    )
    sorting_scalar_fields, sorting_enum_fields = _entity_scalar_and_enum_columns(
        Sorting
    )
    source_scalar_fields, source_enum_fields = _entity_scalar_and_enum_columns(Source)

    for unit_id in tqdm(units_table.index, desc="Functional properties"):
        db_unit = units(db).get_by_id(unit_id)
        db_spot = db_unit.spot
        db_site = db_spot.site
        db_sorting = db_unit.property.sorting
        db_detection = db_sorting.detection
        db_source = db_detection.source
        db_experiment = db_source.experiment
        db_protocol = db_experiment.protocol
        db_stimuli = sorted(
            [
                stim
                for stim in db_protocol.stimuli
                if stim.is_stimulus and not stim.is_neutral
            ],
            key=lambda stim: stim.direction_in_deg,
        )

        mea_xy = db_spot.mea_xy

        units_table.loc[unit_id, "experiment_title"] = db_experiment.title
        units_table.loc[unit_id, "experiment_time"] = db_experiment.date_time
        units_table.loc[unit_id, "site_cytox_band"] = db_spot.cytox_band.name
        units_table.loc[unit_id, "site_depth_um"] = db_site.depth_in_um
        units_table.loc[unit_id, "site_area"] = db_site.area.name
        units_table.loc[unit_id, "site_hemisphere"] = db_site.hemisphere.name
        units_table.loc[unit_id, "site_x"] = mea_xy[0]
        units_table.loc[unit_id, "site_y"] = mea_xy[1]

        for field in detection_enum_fields:
            units_table.loc[unit_id, f"detection_{field}"] = getattr(
                getattr(db_detection, field, None), "name", None
            )

        for field in sorting_enum_fields:
            units_table.loc[unit_id, f"sorting_{field}"] = getattr(
                getattr(db_sorting, field, None), "name", None
            )

        for field in source_enum_fields:
            units_table.loc[unit_id, f"source_{field}"] = getattr(
                getattr(db_source, field, None), "name", None
            )

        for field in detection_scalar_fields:
            units_table.loc[unit_id, f"detection_{field}"] = getattr(
                db_detection, field, None
            )

        for field in sorting_scalar_fields:
            units_table.loc[unit_id, f"sorting_{field}"] = getattr(
                db_sorting, field, None
            )

        for field in source_scalar_fields:
            units_table.loc[unit_id, f"source_{field}"] = getattr(
                db_source, field, None
            )

        for fp in all_functional_properties:
            try:
                units_table.loc[unit_id, fp] = getattr(db_unit, fp)
            except KeyError:
                # May happen for non-exported properties that result in HDF5 data loading
                pass

        polargram = db_unit.polargram
        if polargram is not None:
            for p, stim in zip(polargram, db_stimuli):
                units_table.loc[unit_id, f"polargram_{int(stim.direction_in_deg)}"] = p

            for i, p in enumerate(sorted(polargram, reverse=True)):
                units_table.loc[unit_id, f"polargram_strongest_{i}"] = p


def _write_csv_xz(df: pd.DataFrame, path: str):
    with lzma.open(path, "w") as xz_file:
        df.to_csv(xz_file)
