import random
from typing import Iterable, List, TypeVar

T = TypeVar("T")


def shuffled(it: Iterable[T]) -> List[T]:
    it_list = [x for x in it]
    random.shuffle(it_list)
    return it_list
