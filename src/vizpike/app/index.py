from typing import List

import dash_bootstrap_components as dbc
from dash import dcc, html
from dash.dependencies import Input, Output, State

from vizpike import services
from vizpike.app import DashApp, SessionData
from vizpike.app.utils import callbacks as cb
from vizpike.db import DBSession
from vizpike.orm.schemas.app import TaskState, UserTask


def index(app: DashApp, name: str) -> html.Div:
    id_prefix = f"{app.id_prefix}-{name}"

    page_load = dcc.Interval(f"{id_prefix}-page-load", max_intervals=1)
    content = dbc.Col(id=f"{id_prefix}-content", width=12)

    layout = html.Div([page_load, dbc.Container(dbc.Row(content))])

    @app.dash.callback(
        Output(content.id, "children"),
        Input(page_load.id, "n_intervals"),
        State(app.session_data.id, "data"),
    )
    @cb.inject_db
    @cb.marshall
    def on_load(db: DBSession, _: int, session_data: SessionData):
        user_tasks = services.tasks(db).get_by_session_uuid(session_data.session_uuid)
        sorted_user_tasks = sorted(user_tasks, key=task_sorting_key)

        table = task_table(sorted_user_tasks)
        tooltips = task_tooltips(sorted_user_tasks)

        return html.Div([table, *tooltips])

    def task_table(user_tasks: List[UserTask]):
        return dbc.Table(
            [
                html.Thead(
                    [
                        html.Th("Title"),
                        html.Th("Num. sites"),
                        html.Th("Progress", className="th text-center"),
                        html.Th("Link", className="th text-center"),
                    ]
                ),
                html.Tbody(
                    [
                        html.Tr(
                            [
                                html.Td(task_title(ut)),
                                html.Td(ut.task.length),
                                html.Td(task_progress(ut)),
                                html.Td(task_link(ut), className="td text-center"),
                            ],
                            id=row_id(i),
                        )
                        for i, ut in enumerate(user_tasks)
                    ]
                ),
            ],
            class_name="table table-hover table-stripped",
            style={"max-width": "48em"},
        )

    def task_tooltips(user_tasks: List[UserTask]):
        return [
            dbc.Tooltip(
                dcc.Markdown(ut.task.description_in_markdown),
                target=row_id(i),
                placement="right",
            )
            for i, ut in enumerate(user_tasks)
        ]

    def row_id(i):
        return f"{id_prefix}-tasks-table-row-{i}"

    def task_link(ut: UserTask):
        url = _task_url(ut)
        link_for = {
            TaskState.ASSIGNED: dcc.Link("Start", href=url),
            TaskState.STARTED: dcc.Link("Continue", href=url),
            TaskState.FINISHED: None,
        }
        return link_for[ut.task_state]

    def task_progress(ut: UserTask):
        completed, total = services.tasks.completion(ut)
        rate = completed / total
        percent = 100 * rate
        return dbc.Progress(value=percent, label=f"{percent:.0f}%",)

    def task_sorting_key(ut: UserTask):
        completed, total = services.tasks.completion(ut)
        return -completed / total, ut.task_id

    def task_title(ut: UserTask):
        return html.Span(ut.task.title)

    return layout


def _task_url(ut: UserTask) -> str:
    first_unfinished_item = services.tasks.first_unfinished_item_if_any(ut)
    if first_unfinished_item is None:
        index = 0
    else:
        index = first_unfinished_item[0]

    path = ut.task.endpoint.rstrip("/")
    query_params = [f"task_id={ut.task_id}", f"item_index={index}"]
    href = f"{path}?{'&'.join(query_params)}"
    return href
