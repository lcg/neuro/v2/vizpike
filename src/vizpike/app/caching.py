import pickle
from abc import ABCMeta, abstractmethod
from collections import OrderedDict, defaultdict
from datetime import datetime, timedelta
from enum import Enum
from functools import wraps
from inspect import signature
from typing import Callable, Generic, List, Optional, Tuple, Type, TypeVar, Union

import numpy as np
from redis import Redis

from vizpike.app import redis_cache

T = TypeVar("T")


def cache_size(*, enums: List[Type[Enum]] = None, bools: int = 0, **kwargs: int) -> int:
    """Utility function to compute the number of items a cached function might need,
    based on the rante of values that its signature expects. It multiplies the number
    of entries in each enumerated type, two for every boolean flag, and also an
    indefinite number of integer-typed keyword arguments that may be specified
    separately for inline documentation purposes.
    """
    return np.product(
        [*(len(enum) for enum in enums or []), 2 ** bools, *kwargs.values()]
    )


def cache_on(
    key: Callable[..., str],
    *,
    save: Callable[[T], bytes] = pickle.dumps,
    load: Callable[[bytes], T] = pickle.loads,
    l1_size: int = None,
    l2_expiry: Union[int, timedelta] = None,
) -> Callable[[Callable[..., T]], Callable[..., T]]:
    """Decorate a deterministic function with a two-layer cache. The first layer (L1)
    is an in-memory LRU cache similar to :func:`functools.lru_cache` that speeds up
    computation within the same Python process, whereas the second layer (L2) uses
    Redis to broadcast results to multiple Python processes. The full caching key is
    composed of the function's full-qualified module name plus a suffix that is
    computed by the ``key`` function, which must accept the same parameters as the
    decorated function and be biunivocal on its arguments. The ``save`` and ``load``
    methods are used to serialize to/deserialize from L2. If ``l1_size=0`` (number of
    entries), then this becomes a L2-only cache, and if ``l2_expiry=0`` (key expiry,
    in seconds), then this becomes a L1-only cache.
    """

    def decorator(function: Callable[..., T]) -> Callable[..., T]:
        if l1_size == 0:
            l1_cache = NoCache()
        else:
            l1_cache = L1Cache(size=l1_size)

        if l2_expiry == 0:
            l2_cache = NoCache()
        else:
            l2_cache = L2Cache(
                redis=redis_cache, save=save, load=load, expiry=l2_expiry
            )

        f_signature = signature(function)

        def full_key(*args, **kwargs):
            return f"{function.__module__}.{function.__name__}:{key(*args, **kwargs)}"

        @wraps(function)
        def cached_function(*args, **kwargs) -> T:
            f_bind = f_signature.bind(*args, **kwargs)
            f_bind.apply_defaults()

            cache_key = full_key(*f_bind.args, **f_bind.kwargs)

            try:
                result = l1_cache[cache_key]
            except CacheMiss:
                try:
                    result = l2_cache[cache_key]
                except CacheMiss:
                    result = function(*args, **kwargs)
                    l2_cache[cache_key] = result
                l1_cache[cache_key] = result
            return result

        cached_function.__original__ = function
        cached_function.__key__ = full_key
        cached_function.__l1__ = l1_cache
        cached_function.__l2__ = l2_cache

        return cached_function

    return decorator


class Cache(Generic[T]):
    @abstractmethod
    def __getitem__(self, key: str) -> T:
        pass

    @abstractmethod
    def __setitem__(self, key: str, val: T):
        pass


class CacheMiss(Exception):
    def __init__(self, cache: Cache, key: str):
        super().__init__(f"Cache miss: {cache} does not contain {key}")
        self.cache = cache
        self.key = key


class L1Cache(Cache[T]):
    def __init__(self, *, size: int = None):
        # TODO: Replace with env-var
        self._cache = OrderedDict()
        self._maxsize = size or 64

    def __getitem__(self, key: str) -> T:
        try:
            self._cache.move_to_end(key)
            return self._cache[key]
        except KeyError:
            raise CacheMiss(self, key)

    def __setitem__(self, key: str, val: T):
        self._cache[key] = val
        if len(self._cache) > self._maxsize:
            self._cache.popitem(last=False)


class L2Cache(Cache[T]):
    def __init__(
        self,
        redis: Redis,
        *,
        save: Callable[[T], bytes] = pickle.dumps,
        load: Callable[[T], bytes] = pickle.loads,
        expiry: Union[int, timedelta] = None,
    ):
        # TODO: Replace with env-var
        self._cache = redis
        self._save = save
        self._load = load
        self._expiry = expiry

    def __getitem__(self, key: str) -> T:
        value = self._cache.get(key)
        if value is None:
            raise CacheMiss(self, key)
        else:
            if self._expiry is not None:
                self._cache.expire(key, self._expiry)
            return self._load(value)

    def __setitem__(self, key: str, val: T):
        self._cache.set(key, self._save(val), ex=self._expiry)


class NoCache(Cache[T]):
    def __getitem__(self, key: str) -> T:
        raise CacheMiss(self, key)

    def __setitem__(self, key: str, val: T):
        pass


class StagingMeta(ABCMeta):
    def __new__(mcs, class_name: str, bases: Tuple[type], attrs: dict):
        new_attrs = {}
        annotations = attrs.pop("__annotations__")

        for name, ann in annotations.items():
            attr = attrs.get(name)
            if attr is _field:
                new_attrs[name] = _getter_and_setter(name, ann)
            else:
                if "__annotations__" not in attrs:
                    attrs["__annotations__"] = {}
                attrs["__annotations__"][name] = ann

        for name, attr in attrs.items():
            if name not in annotations:
                new_attrs[name] = attr

        return super().__new__(mcs, class_name, bases, new_attrs)


class UnsetField(Exception):
    def __init__(self, redis: Redis, group: str, field: str):
        super().__init__(
            f"Cannot load field {field} from hash {group} in redis instance {redis}: it is not set."
        )


class _Field:
    pass


def field():
    return _field


_field = _Field()


def _field_identity(x):
    return x


def _getter_and_setter(ann_name: str, ann_type: type):
    save = _known_savers[ann_type]
    load = _known_loaders[ann_type]

    @property
    def getter(self) -> ann_type:
        return load(self.redis, self.group, f"{self.prefix}.{ann_name}",)

    @getter.setter
    def setter(self, value: ann_type):
        self.redis.hset(self.group, f"{self.prefix}.{ann_name}", save(value))

    return setter


def make_loader(
    loader: Callable[[Optional[bytes]], T]
) -> Callable[[Redis, str, str], T]:
    @wraps(loader)
    def wrapped_loader(redis: Redis, group: str, name: str) -> T:
        buffer = redis.hget(group, name)
        if buffer is None:
            raise UnsetField(redis, group, name)
        else:
            return loader(buffer)

    return wrapped_loader


_known_loaders = defaultdict(
    lambda: make_loader(pickle.loads),
    {
        int: make_loader(int),
        float: make_loader(float),
        str: make_loader(lambda x: x.decode("utf-8")),
        bytes: make_loader(_field_identity),
        datetime: make_loader(lambda x: datetime.fromisoformat(x.decode("utf-8"))),
    },
)
_known_savers = defaultdict(
    lambda: pickle.dumps,
    {
        int: _field_identity,
        float: _field_identity,
        str: lambda x: x.encode("utf-8"),
        bytes: _field_identity,
        datetime: lambda x: x.isoformat(),
    },
)
