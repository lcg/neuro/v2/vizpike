from functools import lru_cache

from pydantic import BaseSettings, SecretStr


class Config(BaseSettings):
    debug: bool
    debug_callbacks: bool = False
    secret_key: SecretStr

    class Config:
        env_prefix = "VIZPIKE_"


@lru_cache(maxsize=1)
def config() -> Config:
    return Config()
