from datetime import datetime
from typing import List, Optional, Tuple

import dash
import dash_bootstrap_components as dbc
from dash import callback_context, dcc, html
from dash.dependencies import ALL, MATCH, Input, Output, State
from dash.exceptions import PreventUpdate

from vizpike.app import DashApp, SessionData, widgets
from vizpike.app.utils import ProfilerInfo
from vizpike.app.utils import callbacks as cb
from vizpike.app.utils import trigger_ids, unfold_prop_ids_in_triggers
from vizpike.app.widgets import GratingsAnalysis, GratingsResponse, WidgetSize
from vizpike.app.widgets.lib import (
    FigureFactoryWithCachedJSONFactory,
    FigureJSONFactory,
)
from vizpike.db import DBSession, get_session
from vizpike.orm.schemas.app.json import Action
from vizpike.services.base import UnboundService

from . import control as c
from . import model as m
from . import view as v


def task1(app: DashApp, id_prefix: str) -> html.Div:
    ns = v.ComponentNamespaces(id_prefix)

    layout = html.Div(
        [
            timer_auto_save := dcc.Interval(
                f"{id_prefix}-auto-save-timer", interval=10_000
            ),
            serial_auto_save := dcc.Store(f"{id_prefix}-auto-save-signal"),
            stored_detailed_unit_id := dcc.Store(
                f"{id_prefix}-detailed-unit-id", storage_type="session"
            ),
            serial_task_details := dcc.Store(
                f"{id_prefix}-task-details", storage_type="session"
            ),
            serial_item_indices := dcc.Store(
                f"{id_prefix}-item-indices", storage_type="session",
            ),
            serial_widgets_precache := dcc.Store(
                f"{id_prefix}-widgets-precache", storage_type="session"
            ),
            div_multi_unit_choices := html.Div(id=f"{id_prefix}-multi-unit-choices"),
            html.Div(id="link-changes"),
            dbc.Container(
                [
                    dbc.Row(
                        dbc.Col(
                            dbc.Nav(
                                [
                                    nav_prev_unfinished_item := dbc.NavLink(
                                        [
                                            html.I(
                                                className="bi bi-chevron-double-left"
                                            ),
                                            "Prev site (unfinished)",
                                        ],
                                        id=f"{id_prefix}-nav-prev-unfinished",
                                        href="/",
                                        disabled=True,
                                    ),
                                    nav_prev_item := dbc.NavLink(
                                        [
                                            html.I(className="bi bi-chevron-left"),
                                            "Prev site",
                                        ],
                                        id=f"{id_prefix}-nav-prev-item",
                                        href="/",
                                        disabled=True,
                                    ),
                                    nav_next_item := dbc.NavLink(
                                        [
                                            "Next site",
                                            html.I(className="bi bi-chevron-right"),
                                        ],
                                        id=f"{id_prefix}-nav-next",
                                        href="/",
                                        disabled=True,
                                    ),
                                    nav_next_unfinished_item := dbc.NavLink(
                                        [
                                            "Next site (unfinished)",
                                            html.I(
                                                className="bi bi-chevron-double-right"
                                            ),
                                        ],
                                        id=f"{id_prefix}-nav-next-unfinished",
                                        href="/",
                                        disabled=True,
                                    ),
                                    nav_leave_study := dbc.NavLink(
                                        [
                                            html.I(className="bi bi-door-closed"),
                                            "Save and leave",
                                        ],
                                        id=f"{id_prefix}-nav-leave",
                                        href="/",
                                    ),
                                ]
                            ),
                            width=6,
                        ),
                        justify="center",
                    ),
                    row_item_progress := dbc.Row(
                        id=f"{id_prefix}-item-progress", justify="center"
                    ),
                    dbc.Row(
                        dbc.Col(
                            detail_panel := dbc.Collapse(
                                [
                                    detail_title := html.H4(
                                        "Multi-unit detail",
                                        id=f"{id_prefix}-detail-title",
                                    ),
                                    detail_buttons := dbc.ButtonGroup(
                                        [
                                            detail_close := dbc.Button(
                                                html.I(className="bi bi-x-lg"),
                                                color="light",
                                                id=f"{id_prefix}-detail-close",
                                            ),
                                        ],
                                        id=f"{id_prefix}-detail-buttons",
                                    ),
                                    detail_content := html.Div(
                                        id=f"{id_prefix}-detail-content"
                                    ),
                                ],
                                id=f"{id_prefix}-detail-panel",
                                is_open=False,
                            ),
                            width=12,
                        )
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    html.H4(
                                        "Moving bars stimulus",
                                        style={"text-align": "center"},
                                    ),
                                    dbc.Table(
                                        [
                                            html.Thead(
                                                html.Tr(
                                                    [
                                                        html.Th(
                                                            "", className="text-center"
                                                        ),
                                                        html.Th(
                                                            "Response map",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Polargram",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Sorted waveforms",
                                                            className="text-center",
                                                        ),
                                                    ]
                                                )
                                            ),
                                            rows_of_mb_choices := v.loading_table_body(
                                                f"{id_prefix}-mb-choices", 2, 3
                                            ),
                                        ],
                                    ),
                                ],
                                width=4,
                            ),
                            dbc.Col(
                                [
                                    html.H4(
                                        "Gratings stimulus",
                                        style={"text-align": "center"},
                                    ),
                                    dbc.Table(
                                        [
                                            html.Thead(
                                                html.Tr(
                                                    [
                                                        html.Th(
                                                            "", className="text-center"
                                                        ),
                                                        html.Th(
                                                            "Contrast x dir.",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Frequency (1/°) x dir.",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Speed (Hz.) x dir.",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Color x dir.",
                                                            className="text-center",
                                                        ),
                                                        html.Th(
                                                            "Sorted waveforms",
                                                            className="text-center",
                                                        ),
                                                    ]
                                                ),
                                                style={"text-align": "center"},
                                            ),
                                            rows_of_gratings_choices := v.loading_table_body(
                                                f"{id_prefix}-gr-choices", 3, 2
                                            ),
                                        ],
                                    ),
                                ],
                                width=7,
                            ),
                        ],
                        justify="center",
                    ),
                ],
                fluid=True,
            ),
        ]
    )

    @app.dash.callback(
        Output(row_item_progress.id, "children"),
        Output(serial_task_details.id, "data"),
        Output(serial_item_indices.id, "data"),
        Input(app.location.id, "pathname"),
        Input(app.location.id, "search"),
        Input(app.session_data.id, "data"),
        State(serial_task_details.id, "data"),
        State(serial_item_indices.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    @cb.inject_db
    def ON_CHANGED_location_UPDATE_progress_bar_AND_task_details_AND_item_indices(
        db: DBSession,
        url_path: str,
        url_query: str,
        session: SessionData,
        task_details_n: int,
        item_indices_n: int,
    ) -> Tuple[List[dbc.Col], int, int]:
        if url_path != "/task1":
            raise PreventUpdate()

        controller = c.Task1Controller(session.session_uuid)
        controller.navigate(
            db,
            task_id=v.get_task_id_from_query(url_query),
            item_index=v.get_item_index_from_query(url_query),
        )

        progress_items = v.progress_bar(
            id_prefix=id_prefix,
            task_details=controller.data.task_details,
            indices=controller.data.item_indices,
        )

        return (
            progress_items,
            inc_serial(task_details_n),
            inc_serial(item_indices_n),
        )

    @app.dash.callback(
        Output(serial_widgets_precache.id, "data"),
        Input(serial_task_details.id, "data"),
        Input(serial_item_indices.id, "data"),
        State(serial_widgets_precache.id, "data"),
        State(app.session_data.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    @cb.inject_db
    def ON_CHANGED_item_indices_AND_task_details_DO_precache_adjacent_item_widgets(
        db: DBSession,
        task_details_n: int,
        item_indices_n: int,
        widgets_precache_n: int,
        session: SessionData,
    ):
        controller = c.Task1Controller(session.session_uuid)
        controller.precache_adjacent_items_widgets(db)
        return inc_serial(widgets_precache_n)

    @app.dash.callback(
        output=dict(
            prev_disabled=Output(nav_prev_item.id, "disabled"),
            next_disabled=Output(nav_next_item.id, "disabled"),
            prev_unfinished_disabled=Output(nav_prev_unfinished_item.id, "disabled"),
            next_unfinished_disabled=Output(nav_next_unfinished_item.id, "disabled"),
            prev_href=Output(nav_prev_item.id, "href"),
            next_href=Output(nav_next_item.id, "href"),
            prev_unfinished_href=Output(nav_prev_unfinished_item.id, "href"),
            next_unfinished_href=Output(nav_next_unfinished_item.id, "href"),
        ),
        inputs=[
            Input(serial_item_indices.id, "data"),
            State(app.location.id, "pathname"),
            State(app.session_data.id, "data"),
        ],
    )
    @app.profile
    @cb.log
    @cb.marshall
    def ON_CHANGED_item_indices_UPDATE_navigation_links(
        item_indices_n: int, url_path: str, session: SessionData
    ) -> dict:
        controller = c.Task1Controller(session_uuid=session.session_uuid)
        links = controller.links_state(url_path)
        return links.dict()

    @app.dash.callback(
        Output(serial_auto_save.id, "data"),
        Input(timer_auto_save.id, "n_intervals"),
        Input(nav_prev_item.id, "n_clicks"),
        Input(nav_next_item.id, "n_clicks"),
        Input(nav_prev_unfinished_item.id, "n_clicks"),
        Input(nav_next_unfinished_item.id, "n_clicks"),
        Input(nav_leave_study.id, "n_clicks"),
        State(app.session_data.id, "data"),
        State(serial_auto_save.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    @cb.inject_db
    def ON_auto_save_timer_OR_item_navigation_DO_save_actions(
        db: DBSession,
        n_intervals: int,
        prev_clicks: int,
        next_clicks: int,
        next_unf_clicks: int,
        prev_unf_clicks: int,
        leave_clicks: int,
        session: SessionData,
        auto_save: int,
    ):
        if not callback_context.triggered:
            raise PreventUpdate()

        controller = c.Task1Controller(session.session_uuid)
        controller.save_actions(db)

        return inc_serial(auto_save)

    @app.dash.callback(
        Output(rows_of_mb_choices.id, "children"),
        Output(rows_of_gratings_choices.id, "children"),
        Input(serial_task_details.id, "data"),
        Input(serial_item_indices.id, "data"),
        State(app.session_data.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    def ON_CHANGED_item_indices_AND_task_details_UPDATE_multi_unit_tables(
        task_details_n: int, item_indices_n: int, session: SessionData
    ) -> Tuple[List[html.Tr], List[html.Tr]]:
        controller = c.Task1Controller(session.session_uuid)

        data = controller.data
        task_details = data.task_details
        current_item = task_details.items[data.item_indices.current]

        return v.moving_bars_and_gratings_table_rows(
            current_item, id_prefix=id_prefix, ns=ns
        )

    @app.dash.callback(
        Output(stored_detailed_unit_id.id, "data"),
        Input({**ns.detail_button, "unit_id": ALL}, "n_clicks"),
        Input(serial_item_indices.id, "data"),
        State(app.session_data.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    def ON_details_button_pressed_DO_append_action_UPDATE_detailed_unit_id(
        n_clicks: List[Optional[int]], item_indices_n: str, session: SessionData
    ) -> Optional[int]:
        """When the user clicks on a details button, store the selected multi-unit ID
        on the user's session storage.
        """
        unfold_prop_ids_in_triggers()
        trigger_0 = callback_context.triggered[0]

        if (
            (trigger_0["prop_id"] != serial_item_indices.id)
            and n_clicks
            and any(n_clicks)
        ):
            # When the user explicitly clicks a button, there is only one trigger. Otherwise, when loading the page,
            # the list of triggers contains all buttons that can be matched, and the list of clicks contains Nones.
            # Navigating causes the stored indices to change as well, but in this case, the detailed ID should be reset
            # in order to close the details panel.
            unit_id = trigger_0["prop_id"]["unit_id"]

            c.Task1Controller(session.session_uuid).append_action(
                Action.detail(datetime.now(), unit_id)
            )

            return unit_id
        else:
            return None

    @app.dash.callback(
        Output(div_multi_unit_choices.id, "children"),
        Input(serial_task_details.id, "data"),
        State(serial_item_indices.id, "data"),
        State(app.session_data.id, "data"),
    )
    @cb.log
    @cb.marshall
    def ON_CHANGED_item_indices_AND_task_details_UPDATE_multi_unit_choices_inputs(
        task_details_n: int, item_indices_n: int, session: SessionData
    ) -> List[dcc.Input]:
        """When the user's task details and current item index change, update the
        multi-unit-choices div by creating the appropriate hidden inputs there.
        """
        controller = c.Task1Controller(session.session_uuid)
        data = controller.data
        task_details = data.task_details
        indices = data.item_indices

        current_item = task_details.items[indices.current]

        return v.hidden_decision_inputs(current_item, ns=ns)

    @app.dash.callback(
        Output({**ns.add_overlay, "unit_id": MATCH, "where": ALL}, "color"),
        Output({**ns.add_overlay, "unit_id": MATCH, "where": ALL}, "className"),
        Input({**ns.multi_unit_choice, "unit_id": MATCH}, "value"),
    )
    @app.profile
    @cb.marshall
    def ON_CHANGED_multi_unit_choices_inputs_UPDATE_badges(
        action: m.UserAction,
    ) -> Tuple[List[str], List[str]]:
        """When the ID of a multi-unit row and the user's choices change, update
        the badges of the matching multi-unit to reflect user's approval/rejection
        or no-choice.
        """
        color_outputs = [
            v.action_bg_color(action) for _ in callback_context.outputs_list[0]
        ]
        class_name_outputs = [
            v.action_fg_color(action) for _ in callback_context.outputs_list[1]
        ]
        return color_outputs, class_name_outputs

    @app.dash.callback(
        Output({**ns.multi_unit_choice, "unit_id": MATCH}, "value"),
        Input(
            {**ns.choice_button, "unit_id": MATCH, "approve": True, "detail": ALL},
            "n_clicks",
        ),
        Input(
            {**ns.choice_button, "unit_id": MATCH, "approve": False, "detail": ALL},
            "n_clicks",
        ),
        State(app.session_data.id, "data"),
    )
    @app.profile
    @cb.log
    @cb.marshall
    def ON_choice_button_pressed_DO_append_action_UPDATE_MATCHING_multi_unit_choice(
        approve_clicks: List[int], reject_clicks: List[int], session: SessionData,
    ) -> m.UserAction:
        triggered = dash.callback_context.triggered
        if triggered and (any(approve_clicks) or any(reject_clicks)):
            unfold_prop_ids_in_triggers()

            who_triggered = triggered[0]["prop_id"]
            now = datetime.now()

            unit_id = who_triggered["unit_id"]
            approve = who_triggered["approve"]

            controller = c.Task1Controller(session.session_uuid)

            if approve:
                controller.append_action(Action.approve_one(now, unit_id))
                return m.UserAction.APPROVE
            else:
                controller.append_action(Action.reject_one(now, unit_id))
                return m.UserAction.REJECT
        else:
            raise PreventUpdate()

    @app.dash.callback(
        Output({**ns.multi_unit_row, "unit_id": ALL}, "style"),
        Input(stored_detailed_unit_id.id, "data"),
    )
    @app.profile
    @cb.log
    def ON_CHANGED_detailed_unit_id_UPDATE_ALL_multi_unit_tr_styles(
        unit_id: int,
    ) -> List[dict]:
        """When a multi-unit details button is clicked, the corresponding table row is
        styled to highlight the selection.
        """
        return [
            v.tr_highlighted_style()
            if (out["id"]["unit_id"] == unit_id)
            else v.tr_common_style()
            for out in dash.callback_context.outputs_list
        ]

    @app.dash.callback(
        Output(detail_panel.id, "is_open"),
        Output(detail_content.id, "children"),
        Output(detail_title.id, "children"),
        Output(detail_buttons.id, "children"),
        Input(stored_detailed_unit_id.id, "data"),
        Input(detail_close.id, "n_clicks"),
        State(app.session_data.id, "data"),
    )
    @app.profile
    @cb.marshall
    def ON_CHANGED_detailed_unit_id_OR_close_click_UPDATE_details_panel(
        unit_id: int, close_clicks: int, session: SessionData
    ):
        """When the user session-stored detailed multi-unit ID changes, update the
        contents of the multi-unit details display.
        """
        trigger_id = trigger_ids()[0]

        close_button_was_clicked = (close_clicks is not None) and (
            trigger_id == detail_close.id
        )
        if (unit_id is None) or (close_button_was_clicked):
            is_open = False
            content = None
            title = None
            buttons = [detail_close]
        else:
            controller = c.Task1Controller(session.session_uuid)
            panel = v.MultiUnitDetailsPanel.build(
                unit_id,
                current_item=controller.data.current_item,
                id_prefix=id_prefix,
                ns=ns,
            )

            is_open = True
            content = panel.content
            title = panel.title
            buttons = [detail_close, *panel.buttons]

        return is_open, content, title, buttons

    def add_gratings_figure_retrieval_callback(
        namespace: dict, size: WidgetSize, analysis: GratingsAnalysis
    ):
        add_generic_figure_retrieval_callback(
            namespace,
            lambda db, unit_id: widgets.gratings.kaleidomap.__json__(
                db,
                unit_id,
                analysis,
                response=GratingsResponse.FIRING_RATE,
                size=size,
                colorbar=(analysis is GratingsAnalysis.COLOR_DIRECTION),
            ),
        )

    def add_non_gratings_figure_retrieval_callback(
        namespace: dict, widget: FigureFactoryWithCachedJSONFactory, size: WidgetSize
    ):
        add_generic_figure_retrieval_callback(
            namespace, lambda db, unit_id: widget.__json__(db, unit_id, size=size),
        )

    def add_generic_figure_retrieval_callback(
        namespace: dict, widget: FigureJSONFactory
    ):
        @app.dash.callback(
            Output({**namespace, **ns.add_figure_json, "unit_id": MATCH}, "value"),
            Input({**namespace, **ns.add_graph, "unit_id": MATCH}, "id"),
        )
        @app.profile
        def update_generic_figure(namespace_obj: dict) -> str:
            unit_id = int(namespace_obj["unit_id"])
            try:
                figure_json = widget(None, unit_id)
            except UnboundService:
                # If the figure is not cached, the units service will be queried with
                # the null connection passed above, resulting in this exception. Retry
                # with an actual connection. The happy path saves up to two orders of
                # magnitude in performance.
                db = get_session()
                figure_json = widget(db, unit_id)
                db.close()
            raise ProfilerInfo(result=figure_json, message=namespace_obj)

    gratings_namespaces_and_sizes_by_analysis = [
        (
            [
                (ns.detailed_kaleidomap_color, WidgetSize.MEDIUM),
                (ns.preview_kaleidomap_color, WidgetSize.SMALL),
            ],
            GratingsAnalysis.COLOR_DIRECTION,
        ),
        (
            [
                (ns.detailed_kaleidomap_contrast, WidgetSize.MEDIUM),
                (ns.preview_kaleidomap_contrast, WidgetSize.SMALL),
            ],
            GratingsAnalysis.CONTRAST_DIRECTION,
        ),
        (
            [
                (ns.detailed_kaleidomap_frequency, WidgetSize.MEDIUM),
                (ns.preview_kaleidomap_frequency, WidgetSize.SMALL),
            ],
            GratingsAnalysis.FREQUENCY_DIRECTION,
        ),
        (
            [
                (ns.detailed_kaleidomap_speed, WidgetSize.MEDIUM),
                (ns.preview_kaleidomap_speed, WidgetSize.SMALL),
            ],
            GratingsAnalysis.SPEED_DIRECTION,
        ),
    ]
    for namespaces_and_sizes, analysis in gratings_namespaces_and_sizes_by_analysis:
        for namespace, size in namespaces_and_sizes:
            add_gratings_figure_retrieval_callback(namespace, size, analysis)

    non_gratings_namespaces_by_widgets_by_sizes = [
        (ns.detailed_polargram, widgets.mb.polargram, WidgetSize.MEDIUM),
        (ns.detailed_response_map, widgets.mb.response_map, WidgetSize.MEDIUM),
        (ns.detailed_waveforms, widgets.common.waveforms, WidgetSize.MEDIUM),
        (ns.preview_polargram, widgets.mb.polargram, WidgetSize.SMALL),
        (ns.preview_response_map, widgets.mb.response_map, WidgetSize.SMALL),
        (ns.preview_waveforms, widgets.common.waveforms, WidgetSize.SMALL),
    ]
    for namespace, widget, size in non_gratings_namespaces_by_widgets_by_sizes:
        add_non_gratings_figure_retrieval_callback(namespace, widget, size)

    def add_generic_figure_parsing_client_side_callback(namespace: dict):
        app.dash.clientside_callback(
            "(figure_json) => (figure_json !== undefined) ? JSON.parse(figure_json) : {}",
            Output({**namespace, **ns.add_graph, "unit_id": MATCH}, "figure"),
            Input({**namespace, **ns.add_figure_json, "unit_id": MATCH}, "value"),
        )

    figure_namespaces = [
        ns.detailed_kaleidomap_color,
        ns.detailed_kaleidomap_contrast,
        ns.detailed_kaleidomap_frequency,
        ns.detailed_kaleidomap_speed,
        ns.detailed_polargram,
        ns.detailed_response_map,
        ns.detailed_waveforms,
        ns.preview_kaleidomap_color,
        ns.preview_kaleidomap_contrast,
        ns.preview_kaleidomap_frequency,
        ns.preview_kaleidomap_speed,
        ns.preview_polargram,
        ns.preview_response_map,
        ns.preview_waveforms,
    ]
    for namespace in figure_namespaces:
        add_generic_figure_parsing_client_side_callback(namespace)

    return layout


def inc_serial(serial) -> int:
    try:
        return int(serial + 1)
    except TypeError:
        return 1
