from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional, Set, Tuple
from uuid import UUID

from pydantic import BaseModel

from vizpike import services
from vizpike.app import AppControl, widgets
from vizpike.app.caching import UnsetField
from vizpike.app.utils import redis_cache
from vizpike.app.widgets import GratingsAnalysis, GratingsResponse, WidgetSize
from vizpike.db import DBSession
from vizpike.orm.schemas.app import UserTask
from vizpike.orm.schemas.app.json import (
    Action,
    ActionSequence,
    ActionType,
    SortingSelectionUserTaskDetails,
)

from . import model as m


@dataclass
class Task1Controller:
    session_uuid: UUID

    @property
    def app(self) -> AppControl:
        return AppControl(self.session_uuid)

    @property
    def data(self) -> m.Task1Data:
        return m.Task1Data(
            session_uuid=self.session_uuid,
            group=self.app.hash_group,
            prefix="task1",
            redis=redis_cache,
        )

    def append_action(self, action: Action):
        """Register a user action (approve/reject/detail multi-unit).
        """
        actions = self.data.actions
        actions.append(action)
        self.data.actions = actions

    def links_state(self, url_path: str) -> "LinksState":
        """Compute the state of navigation links, given the base URL.
        """
        data = self.data
        task_id = data.task_id
        indices = data.item_indices

        def disabled_and_href(index):
            if index is None:
                return True, None
            else:
                return False, f"{url_path}?task_id={task_id}&item_index={index}"

        prev_disabled, prev_href = disabled_and_href(indices.previous)
        next_disabled, next_href = disabled_and_href(indices.next)
        prev_unfinished_disabled, prev_unfinished_href = disabled_and_href(
            indices.previous_unfinished
        )
        next_unfinished_disabled, next_unfinished_href = disabled_and_href(
            indices.next_unfinished
        )

        return LinksState(
            next_disabled=next_disabled,
            next_href=next_href,
            next_unfinished_disabled=next_unfinished_disabled,
            next_unfinished_href=next_unfinished_href,
            prev_disabled=prev_disabled,
            prev_href=prev_href,
            prev_unfinished_disabled=prev_unfinished_disabled,
            prev_unfinished_href=prev_unfinished_href,
        )

    def navigate(self, db: DBSession, *, task_id: int, item_index: int):
        """Must be called when page is first loaded, when navigation between items
        occurs, and when leaving the study. Any user actions queued in the staging
        area are saved before refreshing the session and resetting its variables in
        the staging area are overwritten.
        """
        try:
            self.save_actions(db)
        except UnsetField:
            pass

        self.app.refresh_session(db)

        task = services.tasks(db).get_by_session_uuid_and_task_id(
            self.session_uuid, task_id
        )
        task_details = task.details
        indices = m.ItemIndices.from_task_details_and_index(task_details, item_index)

        data = self.data
        data.task_id = task_id
        data.task_details = task_details
        data.item_indices = indices
        data.arrival_timestamp = datetime.now()
        data.actions = []

    def precache_adjacent_items_widgets(self, db: DBSession):
        """Precaches widgets for adjacent task items. A :class:`vizpike.services.UnboundService`
        is raised if a null ``db`` is passed and at least one required widget is not
        cached. This allows for lazy instantiation of database sessions, which may
        save a few milliseconds.
        """
        data = self.data
        task = data.task_details
        indices = data.item_indices
        task_items = task.items

        for index in [
            indices.next_unfinished,
            indices.next,
            indices.previous_unfinished,
            indices.previous,
        ]:
            if index is not None:
                precache_task_item(db, task_items[index])

    def save_actions(self, db: DBSession):
        """Gather all user actions from the staging area and update the user's task
        state and action sequence. The task state is effectively updated in the
        database and the actions in the staging area are cleared before returning.
        If a user navigates to a different item, does nothing, then returns to the
        previous one, a new action sequence is created for the previous item. However,
        as long as the user stays on the same item, additional actions are appended to
        the last action sequence. The closing timestamp in the last sequence is only
        updated if any items are appended. Therefore, staying iddle in a page and not
        performing any actions does not alter the closing timestamp.
        """
        data = self.data
        actions = data.actions

        if actions:
            session_uuid = self.session_uuid
            current_index = data.item_indices.current

            user_task = services.tasks(db).get_by_session_uuid_and_task_id(
                session_uuid=session_uuid, task_id=data.task_id,
            )

            task_details = _update_task_item_state(
                db,
                user_task,
                actions=actions,
                current_index=current_index,
                task_details=data.task_details,
            )

            _update_action_sequence(
                db,
                user_task,
                actions=actions,
                start_timestamp=data.arrival_timestamp,
                index=current_index,
                session_uuid=session_uuid,
            )

            db.commit()
            data.actions = []
            data.task_details = task_details


class LinksState(BaseModel):
    next_disabled: bool
    next_href: Optional[str]
    next_unfinished_disabled: bool
    next_unfinished_href: Optional[str]
    prev_disabled: bool
    prev_href: Optional[str]
    prev_unfinished_disabled: bool
    prev_unfinished_href: Optional[str]


def precache_task_item(
    db: DBSession, task_item: SortingSelectionUserTaskDetails.TaskItem
):
    """Call widgets' JSON factories to cache all figures' JSON required for a given
    task item. If this function is called multiple times (say, when precaching all
    items for this task), the prevalence of values in L1 will depend on how its size
    was configured at the module level (see :mod:`vizpike.app.widgets.common`,
    :mod:`vizpike.app.widgets.gratings`, and :mod:`vizpike.app.widgets.mb`). Likewise,
    the longevity of these figures in L2 depends on module-level configurations.
    """
    gratings_ids = task_item.gratings_ids
    moving_bars_ids = task_item.moving_bars_ids

    for multi_unit_id in gratings_ids:
        for size in WidgetSize:
            widgets.common.waveforms.__json__(db, multi_unit_id, size=size)

            for analysis in GratingsAnalysis:
                widgets.gratings.kaleidomap.__json__(
                    db,
                    multi_unit_id,
                    analysis,
                    response=GratingsResponse.FIRING_RATE,
                    size=size,
                    colorbar=(analysis is GratingsAnalysis.COLOR_DIRECTION),
                )

    for multi_unit_id in moving_bars_ids:
        for size in WidgetSize:
            for widget in [
                widgets.common.waveforms,
                widgets.mb.polargram,
                widgets.mb.response_map,
            ]:
                widget.__json__(db, multi_unit_id, size=size)


def _update_action_sequence(
    db: DBSession,
    user_task: UserTask,
    *,
    actions: List[Action],
    start_timestamp: datetime,
    index: int,
    session_uuid: UUID,
):
    current_sequence = ActionSequence(
        start_timestamp=start_timestamp,
        finish_timestamp=datetime.now(),
        session_uuid=session_uuid,
        item_index=index,
        actions=actions,
    )

    if actions:
        updated_sequences = []

        if user_task.action_sequences:
            previous_sequences = *previous_sequences_but_last, last_sequence = [
                ActionSequence.parse_raw(action_json)
                for action_json in user_task.action_sequences
            ]
            last_timestamp = last_sequence.start_timestamp
            last_index = last_sequence.item_index
            last_actions = last_sequence.actions

            if start_timestamp > last_timestamp:
                # If the current action sequence was initiated after the last one, then it is considered a separate
                # action sequence, even if it pertains the same task item, becuase it means at least a navigation
                # action or a browser refresh took place.
                updated_sequences = [*previous_sequences, current_sequence]
            elif (start_timestamp == last_timestamp) and (index == last_index):
                # At consecutive auto-save events, if there is anything new, the current and last action sequences
                # will have the same starting timestamp and item index, but the lists of action sequences will have
                # different sizes, so the current sequence should replace the last one.
                current_sequence.actions = [*last_actions, *actions]
                updated_sequences = [*previous_sequences_but_last, current_sequence]
        else:
            updated_sequences = [current_sequence]

        if updated_sequences:
            user_task.action_sequences = [seq.json() for seq in updated_sequences]

        db.add(user_task)


def _update_task_item_state(
    db: DBSession,
    user_task: UserTask,
    *,
    actions: List[Action],
    current_index: int,
    task_details: SortingSelectionUserTaskDetails,
) -> SortingSelectionUserTaskDetails:
    current_item = task_details.items[current_index]
    gratings_ids = current_item.gratings_ids
    moving_bars_ids = current_item.moving_bars_ids

    last_action = actions[-1]
    if last_action.type is ActionType.USER_APPROVE_ALL_MULTI_UNITS:
        approved_gratings_unit_ids = gratings_ids
        rejected_gratings_unit_ids = []

        approved_moving_bars_unit_ids = moving_bars_ids
        rejected_moving_bars_unit_ids = []
    elif last_action.type is ActionType.USER_REJECT_ALL_MULTI_UNITS:
        approved_gratings_unit_ids = []
        rejected_gratings_unit_ids = gratings_ids

        approved_moving_bars_unit_ids = []
        rejected_moving_bars_unit_ids = moving_bars_ids
    else:
        (
            approved_gratings_unit_ids,
            rejected_gratings_unit_ids,
        ) = _updated_approvals_and_rejections(
            actions=actions,
            ids=gratings_ids,
            approvals=current_item.approved_gratings_unit_ids,
            rejections=current_item.rejected_gratings_unit_ids,
        )
        (
            approved_moving_bars_unit_ids,
            rejected_moving_bars_unit_ids,
        ) = _updated_approvals_and_rejections(
            actions=actions,
            ids=moving_bars_ids,
            approvals=current_item.approved_moving_bars_unit_ids,
            rejections=current_item.rejected_moving_bars_unit_ids,
        )

    current_item.approved_gratings_unit_ids = approved_gratings_unit_ids
    current_item.rejected_gratings_unit_ids = rejected_gratings_unit_ids

    current_item.approved_moving_bars_unit_ids = approved_moving_bars_unit_ids
    current_item.rejected_moving_bars_unit_ids = rejected_moving_bars_unit_ids

    current_item.best_gratings_unit_id = _best_if_any(approved_gratings_unit_ids)
    current_item.best_moving_bars_unit_id = _best_if_any(approved_moving_bars_unit_ids)

    user_task.details = task_details
    services.tasks(db).update(user_task)

    return task_details


def _updated_approvals_and_rejections(
    *,
    actions: List[Action],
    ids: Set[int],
    approvals: List[int],
    rejections: List[int],
) -> Tuple[List[int], List[int]]:
    individual_decisions = {
        ActionType.USER_APPROVE_MULTI_UNIT,
        ActionType.USER_REJECT_MULTI_UNIT,
    }

    updated_approvals = set(approvals)
    updated_rejections = set(rejections)

    for multi_unit_id in ids:
        multi_unit_actions = [
            action
            for action in actions
            if (action.multi_unit_id == multi_unit_id)
            and (action.type in individual_decisions)
        ]
        if multi_unit_actions:
            last_action = multi_unit_actions[-1]
            if last_action.type is ActionType.USER_APPROVE_MULTI_UNIT:
                updated_approvals.add(multi_unit_id)
                updated_rejections.discard(multi_unit_id)
            elif last_action.type is ActionType.USER_REJECT_MULTI_UNIT:
                updated_approvals.discard(multi_unit_id)
                updated_rejections.add(multi_unit_id)

    return list(updated_approvals), list(updated_rejections)


def _best_if_any(approved_ids: List[int]) -> Optional[int]:
    if len(approved_ids) == 1:
        return approved_ids[0]
    else:
        return None
