from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel
from redis.client import Redis

from vizpike.app.caching import StagingMeta, field
from vizpike.orm.schemas.app.json import Action, SortingSelectionUserTaskDetails


class ItemIndices(BaseModel):
    """Represent various convenience indices into the array of task items' details.

    This information allows for easy management of several components, like inter-item
    navigation links, widgets that depend on task details, and widgets pre-caching, for
    instance.
    """

    current: int
    previous: Optional[int]
    next: Optional[int]
    previous_unfinished: Optional[int]
    next_unfinished: Optional[int]

    @classmethod
    def from_task_details_and_index(
        cls, task: SortingSelectionUserTaskDetails, current_index: int
    ):
        prev_unfinished_indices = [
            index
            for index, item in enumerate(task.items[:current_index])
            if not item.is_finished
        ]
        next_unfinished_indices = [
            index + current_index + 1
            for index, item in enumerate(task.items[current_index + 1 :])
            if not item.is_finished
        ]

        prev_index = (current_index - 1) if (current_index > 0) else None
        next_index = (
            (current_index + 1) if (current_index < len(task.items) - 1) else None
        )
        prev_unfinished_index = (
            prev_unfinished_indices[-1] if prev_unfinished_indices else None
        )
        next_unfinished_index = (
            next_unfinished_indices[0] if next_unfinished_indices else None
        )

        return ItemIndices(
            current=current_index,
            previous=prev_index,
            next=next_index,
            previous_unfinished=prev_unfinished_index,
            next_unfinished=next_unfinished_index,
        )


class UserAction(Enum):
    APPROVE = "APPROVE"
    REJECT = "REJECT"
    UNDEFINED = "NOT_CHOSEN"
    DETAIL = "DETAIL"


@dataclass
class Task1Data(metaclass=StagingMeta):
    session_uuid: UUID
    prefix: str
    group: str
    redis: Redis

    actions: List[Action] = field()
    arrival_timestamp: datetime = field()
    item_indices: ItemIndices = field()
    task_id: int = field()
    task_details: SortingSelectionUserTaskDetails = field()

    @property
    def current_index(self) -> int:
        return self.item_indices.current

    @property
    def current_item(self) -> SortingSelectionUserTaskDetails.TaskItem:
        return self.task_details.items[self.current_index]
