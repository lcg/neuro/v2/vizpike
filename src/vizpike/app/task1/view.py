from dataclasses import dataclass
from functools import lru_cache
from typing import List, Optional, Tuple, Union
from urllib.parse import parse_qs

import dash
import dash_bootstrap_components as dbc
import more_itertools as mit
from dash import dcc, html
from dash.development.base_component import Component
from dash.exceptions import PreventUpdate
from plotly import graph_objs as go

from vizpike.app import widgets
from vizpike.app.widgets import WidgetSize
from vizpike.orm.schemas.app.json import SortingSelectionUserTaskDetails

from . import model as m


@dataclass(frozen=True)
class ComponentNamespaces:
    id_prefix: str

    @property
    @lru_cache()
    def add_figure_json(self):
        return {"additional": "FIGURE_JSON"}

    @property
    @lru_cache()
    def add_graph(self):
        return {"additional": "GRAPH"}

    @property
    @lru_cache()
    def add_medium(self):
        return {"size": "MEDIUM"}

    @property
    @lru_cache()
    def add_overlay(self):
        return {"additional": "OVERLAY"}

    @property
    @lru_cache()
    def add_small(self):
        return {"size": "SMALL"}

    @property
    @lru_cache()
    def base(self):
        return {"namespace": self.id_prefix}

    @property
    @lru_cache()
    def base_kaleidomap_contrast(self):
        return {**self.base, "type": "GRATINGS_CONTRAST"}

    @property
    @lru_cache()
    def base_kaleidomap_color(self):
        return {**self.base, "type": "GRATINGS_COLOR"}

    @property
    @lru_cache()
    def base_kaleidomap_frequency(self):
        return {**self.base, "type": "GRATINGS_FREQUENCY"}

    @property
    @lru_cache()
    def base_kaleidomap_speed(self):
        return {**self.base, "type": "GRATINGS_SPEED"}

    @property
    @lru_cache()
    def base_polargram(self):
        return {**self.base, "type": "POLARGRAM"}

    @property
    @lru_cache()
    def base_response_map(self):
        return {**self.base, "type": "RESPONSE_MAP"}

    @property
    @lru_cache()
    def base_waveforms(self):
        return {**self.base, "type": "WAVEFORMS"}

    @property
    @lru_cache()
    def choice_button(self):
        return {**self.base, "type": "CHOICE_BUTTON"}

    @property
    @lru_cache()
    def detail_button(self):
        return {**self.base, "type": "DETAIL_BUTTON"}

    @property
    @lru_cache()
    def detailed_kaleidomap_color(self):
        return {**self.base_kaleidomap_color, **self.add_medium}

    @property
    @lru_cache()
    def detailed_kaleidomap_contrast(self):
        return {**self.base_kaleidomap_contrast, **self.add_medium}

    @property
    @lru_cache()
    def detailed_kaleidomap_frequency(self):
        return {**self.base_kaleidomap_frequency, **self.add_medium}

    @property
    @lru_cache()
    def detailed_kaleidomap_speed(self):
        return {**self.base_kaleidomap_speed, **self.add_medium}

    @property
    @lru_cache()
    def detailed_polargram(self):
        return {**self.base_polargram, **self.add_medium}

    @property
    @lru_cache()
    def detailed_response_map(self):
        return {**self.base_response_map, **self.add_medium}

    @property
    @lru_cache()
    def detailed_waveforms(self):
        return {**self.base_waveforms, **self.add_medium}

    @property
    @lru_cache()
    def multi_unit_choice(self):
        return {**self.base, "type": "MULTI_UNIT_CHOICE"}

    @property
    @lru_cache()
    def multi_unit_row(self):
        return {**self.base, "type": "MULTI_UNIT_ROW"}

    @property
    @lru_cache()
    def preview_kaleidomap_color(self):
        return {**self.base_kaleidomap_color, **self.add_small}

    @property
    @lru_cache()
    def preview_kaleidomap_contrast(self):
        return {**self.base_kaleidomap_contrast, **self.add_small}

    @property
    @lru_cache()
    def preview_kaleidomap_frequency(self):
        return {**self.base_kaleidomap_frequency, **self.add_small}

    @property
    @lru_cache()
    def preview_kaleidomap_speed(self):
        return {**self.base_kaleidomap_speed, **self.add_small}

    @property
    @lru_cache()
    def preview_polargram(self):
        return {**self.base_polargram, **self.add_small}

    @property
    @lru_cache()
    def preview_response_map(self):
        return {**self.base_response_map, **self.add_small}

    @property
    @lru_cache()
    def preview_waveforms(self):
        return {**self.base_waveforms, **self.add_small}


def action_bg_color(action: m.UserAction) -> str:
    color_for = {
        m.UserAction.APPROVE: "success",
        m.UserAction.REJECT: "danger",
        m.UserAction.UNDEFINED: "light",
    }
    return color_for[action]


def action_fg_color(action: m.UserAction) -> str:
    color_for = {
        m.UserAction.APPROVE: "text-light",
        m.UserAction.REJECT: "text-light",
        m.UserAction.UNDEFINED: "text-dark",
    }
    return color_for[action]


@lru_cache()
def get_task_id_from_query(query: str) -> int:
    try:
        return int(parse_qs(query.lstrip("?"))["task_id"][0])
    except (KeyError, IndexError):
        raise ValueError(f"Missing query parameter 'task_id' in query: {query}")


@lru_cache()
def get_item_index_from_query(query: str) -> int:
    try:
        return int(parse_qs(query.lstrip("?"))["item_index"][0])
    except (KeyError, IndexError):
        raise ValueError(f"Missing query parameter 'item_index' in query: {query}")


def hidden_decision_inputs(
    current_item: SortingSelectionUserTaskDetails.TaskItem, *, ns: ComponentNamespaces
) -> List[dcc.Input]:
    all_ids = {*current_item.gratings_ids, *current_item.moving_bars_ids}
    approved_ids = {
        *current_item.approved_gratings_unit_ids,
        *current_item.approved_moving_bars_unit_ids,
    }
    rejected_ids = {
        *current_item.rejected_gratings_unit_ids,
        *current_item.rejected_moving_bars_unit_ids,
    }
    undefined_ids = all_ids - approved_ids - rejected_ids

    actions_by_id_sets = [
        (m.UserAction.APPROVE, approved_ids),
        (m.UserAction.REJECT, rejected_ids),
        (m.UserAction.UNDEFINED, undefined_ids),
    ]

    all_inputs = [
        dcc.Input(
            id={**ns.multi_unit_choice, "unit_id": unit_id},
            type="hidden",
            value=action,
        )
        for action, id_set in actions_by_id_sets
        for unit_id in id_set
    ]

    return all_inputs


def loading_table_body(id: str, n_rows: int, n_cols: int) -> html.Tbody:
    return html.Tbody(
        [
            html.Tr(
                [
                    html.Td(),
                    *[
                        html.Td(dbc.Spinner(size="md"), className="text-center")
                        for _ in range(n_cols)
                    ],
                ]
            )
            for _ in range(n_rows)
        ],
        id=id,
    )


def moving_bars_and_gratings_table_rows(
    current_item: SortingSelectionUserTaskDetails.TaskItem,
    *,
    id_prefix: str,
    ns: ComponentNamespaces,
) -> Tuple[List[html.Tr], List[html.Tr]]:
    approved_unit_ids = {
        *current_item.approved_gratings_unit_ids,
        *current_item.approved_moving_bars_unit_ids,
    }
    rejected_unit_ids = {
        *current_item.rejected_gratings_unit_ids,
        *current_item.rejected_moving_bars_unit_ids,
    }
    if len(dash.callback_context.triggered) < 2:
        raise PreventUpdate()

    def approve_button(unit_id: int):
        return dbc.Button(
            html.I(className="bi bi-check-lg text-success"),
            id={
                **ns.choice_button,
                "unit_id": unit_id,
                "approve": True,
                "detail": False,
            },
            color="light",
            size="sm",
        )

    def details_button(unit_id: int):
        return dbc.Button(
            html.I(className="bi bi-zoom-in"),
            id={**ns.detail_button, "unit_id": unit_id},
            color="light",
            size="sm",
        )

    def reject_button(unit_id: int):
        return dbc.Button(
            html.I(className="bi bi-trash text-danger"),
            id={
                **ns.choice_button,
                "unit_id": unit_id,
                "approve": False,
                "detail": False,
            },
            color="light",
            size="sm",
        )

    def graph_overlay_td(
        figure_placeholder: go.Figure, attributes: dict, unit_id: int
    ) -> html.Td:
        base_id = {**attributes, "unit_id": unit_id}

        return html.Td(
            html.Div(
                [
                    dcc.Graph(
                        id={**base_id, **ns.add_graph},
                        figure=figure_placeholder,
                        config={"displayModeBar": False},
                    ),
                    dcc.Input({**base_id, **ns.add_figure_json}, type="hidden"),
                ],
                style={"position": "relative"},
            )
        )

    mb_details_buttons = [
        details_button(unit_id) for unit_id in current_item.moving_bars_ids
    ]
    gr_details_buttons = [
        details_button(unit_id) for unit_id in current_item.gratings_ids
    ]
    mb_approve_buttons = [
        approve_button(unit_id) for unit_id in current_item.moving_bars_ids
    ]
    gr_approve_buttons = [
        approve_button(unit_id) for unit_id in current_item.gratings_ids
    ]
    mb_discard_buttons = [
        reject_button(unit_id) for unit_id in current_item.moving_bars_ids
    ]
    gr_discard_buttons = [
        reject_button(unit_id) for unit_id in current_item.gratings_ids
    ]

    def badge_kwargs(unit_id):
        class_name = None

        if unit_id in approved_unit_ids:
            color = "success"
        elif unit_id in rejected_unit_ids:
            color = "danger"
        else:
            color = "light"
            class_name = "text-dark"

        return {
            "className": class_name,
            "color": color,
            "id": {
                "unit_id": unit_id,
                "where": f"{id_prefix}-preview-rows",
                **ns.add_overlay,
            },
        }

    mb_button_groups = [
        html.Div(
            [
                dbc.Badge(unit_id, **badge_kwargs(unit_id)),
                html.Br(),
                d_button,
                html.Br(),
                v_button,
                html.Br(),
                x_button,
            ]
        )
        for unit_id, d_button, v_button, x_button in zip(
            current_item.moving_bars_ids,
            mb_details_buttons,
            mb_approve_buttons,
            mb_discard_buttons,
        )
    ]
    gr_button_groups = [
        html.Div(
            [
                dbc.Badge(unit_id, **badge_kwargs(unit_id)),
                html.Br(),
                d_button,
                html.Br(),
                v_button,
                html.Br(),
                x_button,
            ]
        )
        for unit_id, d_button, v_button, x_button in zip(
            current_item.gratings_ids,
            gr_details_buttons,
            gr_approve_buttons,
            gr_discard_buttons,
        )
    ]
    mb_layout = [
        html.Tr(
            [
                html.Td(buttons, className="text-center"),
                graph_overlay_td(
                    widgets.mb.response_map.__placeholder__(WidgetSize.SMALL),
                    ns.preview_response_map,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.mb.polargram.__placeholder__(WidgetSize.SMALL),
                    ns.preview_polargram,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.common.waveforms.__placeholder__(WidgetSize.SMALL),
                    ns.preview_waveforms,
                    unit_id,
                ),
            ],
            id={**ns.multi_unit_row, "unit_id": unit_id},
        )
        for unit_id, buttons in zip(current_item.moving_bars_ids, mb_button_groups)
    ]
    gr_layout = [
        html.Tr(
            [
                html.Td(buttons, className="text-center"),
                graph_overlay_td(
                    widgets.gratings.kaleidomap.__placeholder__(WidgetSize.SMALL),
                    ns.preview_kaleidomap_contrast,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.gratings.kaleidomap.__placeholder__(WidgetSize.SMALL),
                    ns.preview_kaleidomap_frequency,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.gratings.kaleidomap.__placeholder__(WidgetSize.SMALL),
                    ns.preview_kaleidomap_speed,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.gratings.kaleidomap.__placeholder__(WidgetSize.SMALL),
                    ns.preview_kaleidomap_color,
                    unit_id,
                ),
                graph_overlay_td(
                    widgets.common.waveforms.__placeholder__(WidgetSize.SMALL),
                    ns.preview_waveforms,
                    unit_id,
                ),
            ],
            id={**ns.multi_unit_row, "unit_id": unit_id},
        )
        for unit_id, buttons in zip(current_item.gratings_ids, gr_button_groups)
    ]

    return mb_layout, gr_layout


def progress_bar(
    *,
    id_prefix: str,
    task_details: SortingSelectionUserTaskDetails,
    indices: m.ItemIndices,
):
    current_index = indices.current
    task_items = task_details.items

    def tooltip(index: int) -> html.Span:
        ordinals = {
            0: ["1", html.Sup("st")],
            1: ["2", html.Sup("nd")],
            2: ["3", html.Sup("rd")],
        }
        item_is_finished = task_items[index].is_finished
        status = "finished" if item_is_finished else "pending"
        message = ordinals.get(index, [str(index + 1), html.Sup("th")])
        message.append(f" site ({status})")
        return html.Span(message, className="text-primary" if item_is_finished else "")

    def inner_div_class(item, i, current_index):
        classes = ["text-center"]
        if i == current_index:
            classes.extend(["text-black", "bg-white"])
        elif item.is_finished:
            classes.extend(["text-white", "bg-primary"])
        else:
            classes.extend(["text-white", "bg-secondary"])
        return " ".join(classes)

    def outer_div_class(item, i, current_index):
        if i == current_index:
            return "bg-primary"
        elif item.is_finished:
            return "bg-primary"
        else:
            return "bg-secondary"

    progress_items = [
        dbc.Col(
            [
                html.Div(
                    [
                        html.Div(
                            html.Span(i + 1, style={"font-size": "10px"}),
                            id=(div_id := f"{id_prefix}-progress-{i}"),
                            className=inner_div_class(item, i, current_index),
                            style={
                                "display": "block",
                                "width": "100%",
                                "height": "100%",
                            },
                        ),
                        dbc.Tooltip(tooltip(i), target=div_id),
                    ],
                    style={
                        "display": "inline-block",
                        "width": 20,
                        "height": 28,
                        "margin": "2px",
                        "padding": "2px",
                    },
                    className=outer_div_class(item, i, current_index),
                )
                for i, item in chunk
            ]
        )
        for chunk in mit.chunked(enumerate(task_items), 8)
    ]
    return progress_items


def tr_highlighted_style() -> dict:
    return {"border-width": "4px", "border-color": "#1FE1FF", "border-radius": "2px"}


def tr_common_style() -> dict:
    return {}


@dataclass
class MultiUnitDetailsPanel:
    buttons: List[dbc.Button]
    content: Optional[dbc.Table]
    title: str

    @classmethod
    def build(
        cls,
        unit_id: int,
        *,
        id_prefix: str,
        current_item: SortingSelectionUserTaskDetails.TaskItem,
        ns: ComponentNamespaces,
    ) -> "MultiUnitDetailsPanel":
        builder = cls.Builder(id_prefix=id_prefix, ns=ns, unit_id=unit_id)
        buttons = builder.buttons()

        if unit_id in current_item.gratings_ids:
            table = builder.gratings_table()
            title = builder.gratings_title()
        elif unit_id in current_item.moving_bars_ids:
            table = builder.moving_bars_table()
            title = builder.moving_bars_title()
        else:
            table = None
            title = builder.illegal_title()

        return cls(buttons=buttons, content=table, title=title)

    @dataclass
    class Builder:
        Title = Union[str, List[Component]]

        id_prefix: str
        ns: ComponentNamespaces
        unit_id: int

        def buttons(self) -> List[dbc.Button]:
            return [
                dbc.Button(
                    html.I(className="bi bi-check-lg text-success"),
                    color="light",
                    id={
                        **self.ns.choice_button,
                        "unit_id": self.unit_id,
                        "approve": True,
                        "detail": True,
                    },
                ),
                dbc.Button(
                    html.I(className="bi bi-trash text-danger"),
                    color="light",
                    id={
                        **self.ns.choice_button,
                        "unit_id": self.unit_id,
                        "approve": False,
                        "detail": True,
                    },
                ),
            ]

        def illegal_title(self) -> Title:
            return f"Selected unit (ID={self.unit_id}) does not exist."

        def gratings_title(self) -> Title:
            return self._make_title("Details of multi-unit", "(gratings stimulus)")

        def gratings_table(self) -> dbc.Table:
            ns = self.ns

            gratings_namespaces = [
                ns.detailed_kaleidomap_contrast,
                ns.detailed_kaleidomap_frequency,
                ns.detailed_kaleidomap_speed,
                ns.detailed_kaleidomap_color,
            ]
            gratings_namespaces_by_widgets = [
                (namespace, widgets.gratings.kaleidomap)
                for namespace in gratings_namespaces
            ]

            namespaces_by_widgets = [
                *gratings_namespaces_by_widgets,
                (ns.detailed_waveforms, widgets.common.waveforms),
            ]

            table = dbc.Table(
                [
                    html.Thead(
                        html.Tr(
                            [
                                html.Th("Contrast x dir."),
                                html.Th("Frequency (1/°) x dir."),
                                html.Th("Speed (Hz.) x dir."),
                                html.Th("Color x dir."),
                                html.Th("Sorted waveforms"),
                            ]
                        )
                    ),
                    html.Tbody(
                        html.Tr(
                            [
                                html.Td(
                                    self._graph_and_figure_components(
                                        namespace=namespace,
                                        placeholder=widget.__placeholder__(
                                            WidgetSize.MEDIUM
                                        ),
                                    )
                                )
                                for namespace, widget in namespaces_by_widgets
                            ]
                        )
                    ),
                ]
            )
            return table

        def moving_bars_title(self) -> Title:
            return self._make_title("Details of multi-unit", "(moving bars stimulus)")

        def moving_bars_table(self) -> dbc.Table:
            ns = self.ns
            namespaces_by_widgets = [
                (ns.detailed_response_map, widgets.mb.response_map),
                (ns.detailed_polargram, widgets.mb.polargram),
                (ns.detailed_waveforms, widgets.common.waveforms),
            ]
            table = dbc.Table(
                [
                    html.Thead(
                        html.Tr(
                            [
                                html.Th("Response map"),
                                html.Th("Polargram"),
                                html.Th("Sorted waveforms"),
                            ]
                        )
                    ),
                    html.Tbody(
                        html.Tr(
                            [
                                html.Td(
                                    self._graph_and_figure_components(
                                        namespace=namespace,
                                        placeholder=widget.__placeholder__(
                                            WidgetSize.MEDIUM
                                        ),
                                    )
                                )
                                for namespace, widget in namespaces_by_widgets
                            ]
                        )
                    ),
                ]
            )
            return table

        def _make_title(self, prefix: str, suffix: str):
            return [
                prefix.rstrip() + " ",
                dbc.Badge(
                    self.unit_id,
                    id={
                        "unit_id": self.unit_id,
                        "where": f"{self.id_prefix}-detail-title",
                        **self.ns.add_overlay,
                    },
                ),
                " " + suffix.lstrip(),
            ]

        def _graph_and_figure_components(
            self, namespace: dict, placeholder: go.Figure
        ) -> Tuple[dcc.Input, dcc.Graph]:
            unit_id = self.unit_id
            ns = self.ns
            return (
                dcc.Input(
                    id={**namespace, **ns.add_figure_json, "unit_id": unit_id},
                    type="hidden",
                ),
                dcc.Graph(
                    id={**namespace, **ns.add_graph, "unit_id": unit_id},
                    figure=placeholder,
                ),
            )
