from flask import Flask

from vizpike.app import DashApp
from vizpike.app.index import index
from vizpike.app.task1 import task1


def make_app() -> DashApp:
    app = DashApp()

    app.add_page("/", index(app, "index"))
    app.add_page("/task1", task1(app, "task1"))

    return app


def wsgi() -> Flask:
    return make_app().dash.server
