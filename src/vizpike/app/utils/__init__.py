import base64
import json
import urllib.parse
from datetime import datetime
from functools import wraps
from logging import getLogger
from typing import Any, List, Tuple
from urllib.error import URLError

import dash
import requests
from dash import callback_context
from pydantic import AnyHttpUrl, BaseSettings
from redis import Redis

# TODO: Refactor Redis settings out of DB module.
from vizpike.db import config as db_config
from vizpike.typing import JSON


class AppSettings(BaseSettings):
    profiler_endpoint: AnyHttpUrl
    profile: bool

    class Config:
        env_prefix = "VIZPIKE_"


class ProfilerInfo(Exception):
    def __init__(self, *, message: JSON, result: Any):
        super().__init__()
        self.message = message
        self.result = result

    def __repr__(self):
        return f"{self.__class__.__name__}(message={self.message!r}, result={self.result!r})"


class Profiler:
    def __init__(self, endpoint: str, active: bool):
        self.endpoint = endpoint
        self.active = active

    def profile(self, function):
        @wraps(function)
        def passthrough_function(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except ProfilerInfo as info:
                return info.result

        @wraps(function)
        def profiled_function(*args, **kwargs):
            start = datetime.now()

            try:
                result = function(*args, **kwargs)
                message = None
            except ProfilerInfo as info:
                message = info.message
                result = info.result
            except Exception:
                raise

            end = datetime.now()

            requests.post(
                self.endpoint,
                json={
                    "start_time": start.isoformat(),
                    "end_time": end.isoformat(),
                    "module": function.__module__,
                    "function": function.__name__,
                    "info": json.dumps(message),
                },
            )

            return result

        if self.active:
            return profiled_function
        else:
            return passthrough_function


def decode_url(url: str) -> Tuple[str, JSON]:
    """Strips the last path part of a URL, decodes it, then loads it as JSON.
    Returns the remaining of the path and the decoded JSON.
    """
    # fmt: off
    *path_parts, encoded_query = url.split("/")
    path = "/".join(path_parts).rstrip("/")
    data = json.loads(
        base64.b64decode(
            urllib.parse.unquote(encoded_query)
        ).decode()
    )
    # fmt: on
    return path, data


def encode_url(path: str, data: JSON) -> str:
    """URL-encodes JSON data and appends it to a given URL path. Returns the
    resulting path as a string.
    """
    # fmt: off
    query = urllib.parse.quote(
        base64.b64encode(
            json.dumps(data).replace(" ", "").encode()
        )
    )
    url = f"{path.rstrip('/')}/{query}"

    if len(url) > _URL_LENGTH_HARD_LIMIT:
        raise URLError(
            f"URL-encoded exceeds {_URL_LENGTH_HARD_LIMIT} characters. "
            f"Path: {path!r}. Data: {data!r}"
        )
    elif len(url) > _URL_LENGTH_SOFT_LIMIT:
        logger.warning(
            f"URL-encoded exceeds {_URL_LENGTH_SOFT_LIMIT} characters. "
            f"The actual limit is {_URL_LENGTH_HARD_LIMIT}. "
            "Beware of the length of your data. "
            f"Path: {path!r}. Data: {data!r}"
        )

    # fmt: on
    return url


def trigger_ids() -> List[str]:
    return [trigger["prop_id"].split(".")[0] for trigger in callback_context.triggered]


def unfold_prop_ids_in_triggers():
    """JSON-decodes rich component IDs and changed property name from a list of
    triggers and update those in the global list of triggers.

    When rich component IDs (that is, dictionaries) are assigned to Dash components,
    they are packed with the name of the attribute that triggered the update in the
    list of triggers. The prop_id key of each trigger therefore corresponds to the
    JSON-encoded dictionary, followed with a dot and the name of the attribute. For
    instance, if

        {"namespace": "action", "type": "approve", "unit_id": 1234}

    is used as a button component's ID, then an Input for this button's n_click
    property will be reflected in the dash.callback_context.triggers list as

        {"prop_id": '{"namespace":"action","type":"approve","unit_id":1234}.n_clicks"}

    This function converts thea above dictionary into

        {
            "prop_id": {"namespace": "action", "type": "approve", "unit_id": 1234},
            "property": "n_clicks"
        }
    """
    for trigger in dash.callback_context.triggered:
        *namespace_parts, attribute = trigger["prop_id"].split(".")
        namespace = ".".join(namespace_parts)

        try:
            namespace_obj = json.loads(namespace)
        except json.JSONDecodeError:
            namespace_obj = namespace

        trigger["prop_id"] = namespace_obj
        trigger["property"] = attribute


_URL_LENGTH_HARD_LIMIT = 2048
_URL_LENGTH_SOFT_LIMIT = _URL_LENGTH_HARD_LIMIT // 2
logger = getLogger(__name__)
profiler = Profiler(
    endpoint=AppSettings().profiler_endpoint, active=AppSettings().profile
)
redis_cache = Redis(host=db_config.fp_redis_host, port=db_config.fp_redis_port)
