import json
from enum import Enum
from functools import wraps
from inspect import Parameter, signature
from pprint import pformat
from textwrap import indent
from typing import Any, Callable, Type

import dash
import makefun
from dash.exceptions import PreventUpdate
from pydantic import BaseModel
from sqlalchemy.exc import SQLAlchemyError

from vizpike.app.config import config as app_config
from vizpike.db import DBSession, get_session
from vizpike.typing import JSON


def abort_on_empty(callback):
    """Decorate a Dash callback to abort execution (raise :class:`dash.exceptions.PreventUpdate`)
    when the triggers are all empty (initial update).
    """

    @wraps(callback)
    def wrapped_callback(*args, **kwargs):
        if not dash.callback_context.triggered:
            raise PreventUpdate()
        else:
            return callback(*args, **kwargs)

    return wrapped_callback


def log(callback):
    """Decorate a Dash callback with rudimentary stdout debugging capabilities. Output
    is turned on/off by an environment variable (see code).
    """

    @wraps(callback)
    def wrapped_callback(*args, **kwargs):
        def format(what):
            return indent(pformat(what, depth=3, compact=True), prefix="    ")

        header = (
            "==========================================================================================\n"
            f"{callback.__name__.upper()}\n"
            "==========================================================================================\n"
        )
        try:
            r = callback(*args, **kwargs)
            if app_config().debug_callbacks:
                print(
                    header
                    + "\n".join(
                        [
                            f"TRIGGERS: \n{format(dash.callback_context.triggered)}",
                            f"ARGS: \n{format(args)}",
                            f"KWARGS: \n{format(kwargs)}",
                            f"RETURN:\n{format(r)}",
                        ]
                    )
                )
            return r
        except PreventUpdate:
            if app_config().debug_callbacks:
                print(header + "PREVENT UPDATE")
            raise

    return wrapped_callback


def inject_db(
    dash_callback_or_none: Callable = None,
    commit=True,
    close=True,
    session_factory: Callable[[], DBSession] = get_session,
) -> Callable:
    """Decorate a Dash callback that accepts a 'db' parameter annotated with type
    :class:`vizpike.db.DBSession` and return a callback in which this parameter is
    removed, but the argument is filled with a database session instantiated at the
    moment of the call. By default, :meth:`vizpike.db.get_session` is used as the
    session factory, and the session is both committed (when no exceptions occur) and
    closed before returning. If the :meth`__sqlalchemy_handler` attribute is set on the
    decorated function and a SQLAlchemy exception occurs, it will be called with
    arguments ``db, exc, *args, **kwargs`` after rolling back the transaction.

    Note: this is similar to FastAPI's dependency injection feature.
    """

    def wrapper(dash_callback):
        base_message = (
            f"Cannot decorate callback {dash_callback} with an auto-closing DB session"
        )

        f_signature = signature(dash_callback)
        f_params = f_signature.parameters

        try:
            db_param = f_params["db"]
            db_param_type = db_param.annotation
            assert (db_param_type is Parameter.empty) or (db_param_type is DBSession)
        except AssertionError:
            raise TypeError(
                f"{base_message}: its 'db' parameter is not annotated with the "
                f"{DBSession.__module__}.{DBSession.__name__} type."
            )
        except KeyError:
            raise TypeError(f"{base_message}: it does not contain a 'db' parameter.")

        @makefun.wraps(dash_callback, remove_args="db")
        def decorated_callback(*args, **kwargs):
            kwargs["db"] = db = session_factory()

            try:
                result = dash_callback(*args, **kwargs)
                if commit:
                    db.commit()
            except SQLAlchemyError as exc:
                db.rollback()
                if decorated_callback.__sqlalchemy_handler:
                    result = decorated_callback.__sqlalchemy_handler(
                        db, exc, *args, **kwargs
                    )
                else:
                    raise
            finally:
                if close:
                    db.close()

            return result

        def register_error_handler(handler: Callable[[SQLAlchemyError, ...], Any]):
            decorated_callback.__sqlalchemy_handler = handler
            return handler

        decorated_callback.__sqlalchemy_handler = None
        decorated_callback.on_sqlalchemy_error = register_error_handler

        return decorated_callback

    if dash_callback_or_none is None:
        return wrapper
    else:
        return wrapper(dash_callback_or_none)


def marshall(
    dash_callback_or_none: Callable = None, intermediate_wrapper=None
) -> Callable:
    """Decorate a Dash callback to provide transparent marshalling of Pydantic model
    or enumeration arguments and return values, based on type annotations. The callback
    signature is left untouched.
    """

    def enum_conversion(param_annotation: Type[Enum]) -> Callable[[JSON], Enum]:
        def conversion(val):
            if val is None:
                raise PreventUpdate()
            else:
                return param_annotation(val)

        return conversion

    def model_conversion(
        param_annotation: Type[BaseModel],
    ) -> Callable[[JSON], BaseModel]:
        def conversion(val):
            if val is None:
                raise PreventUpdate()
            else:
                return param_annotation(**val)

        return conversion

    def wrapper(dash_callback):

        nonlocal intermediate_wrapper

        if intermediate_wrapper is None:
            intermediate_wrapper = wraps(dash_callback)

        f_signature = signature(dash_callback)

        @intermediate_wrapper
        def marshalled_callback(*args, **kwargs):
            f_bind = f_signature.bind(*args, **kwargs)
            f_arguments = f_bind.arguments

            for name, param in f_signature.parameters.items():
                param_kind = param.kind
                param_annotation = param.annotation

                if (param_annotation is not Parameter.empty) and isinstance(
                    param_annotation, type
                ):
                    if issubclass(param_annotation, BaseModel):
                        conversion = model_conversion(param_annotation)
                    elif issubclass(param_annotation, Enum):
                        conversion = enum_conversion(param_annotation)
                    else:
                        continue

                    if param_kind is Parameter.VAR_POSITIONAL:
                        f_arguments[name] = tuple(
                            conversion(arg) for arg in f_arguments[name]
                        )
                    elif param_kind is Parameter.VAR_KEYWORD:
                        f_arguments[name] = {
                            k: conversion(v) for k, v in f_arguments[name].items()
                        }
                    elif param_kind in {
                        Parameter.KEYWORD_ONLY,
                        Parameter.POSITIONAL_ONLY,
                        Parameter.POSITIONAL_OR_KEYWORD,
                    }:
                        f_arguments[name] = conversion(f_arguments[name])
                    else:
                        raise TypeError(
                            f"Marshalling not supported for parameter type: {param_kind}"
                        )

            result = dash_callback(*f_bind.args, **f_bind.kwargs)

            try:
                # TODO: I don't know if that's a bug or intended behavior, but some types like UUID4 are not
                # converted by Pydantic into strings when calling .dict(), which is why calling .json() then
                # deserializing into a dict again is needed here.
                return json.loads(result.json())
            except AttributeError:
                return result

        return marshalled_callback

    if dash_callback_or_none is None:
        return wrapper
    else:
        return wrapper(dash_callback_or_none)
