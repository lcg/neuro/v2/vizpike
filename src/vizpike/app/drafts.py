from functools import lru_cache
from itertools import zip_longest
from pathlib import Path
from typing import Dict, Iterable, List, Optional, TypeVar

import numpy as np
import pandas as pd
from dash import dcc, html
from plotly import express as px
from plotly import graph_objs as go
from pydantic import BaseModel
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sqlalchemy.engine import Engine

from vizpike.app.widgets import printable_unit_name
from vizpike.db import get_engine
from vizpike.orm.schemas.dataset import enums

T = TypeVar("T")


def section_location_overview() -> html.Section:
    num_sortings = pd.read_sql(
        """\
        SELECT mega_gr.site_hemisphere,
               mega_gr.site_area,
               mega_gr.site_depth_in_um,
               mega_gr.spot_channel,
               COUNT(DISTINCT mega_gr.new_sorting_id) AS count_gr_sortings,
               COUNT(DISTINCT mega_mb.new_sorting_id) AS count_mb_sortings
        FROM derived."Megazord" mega_gr,
             derived."Megazord" mega_mb
        WHERE mega_gr.property_type = 'GRATINGS'
          AND mega_mb.property_type = 'MOVING_BARS'
          AND mega_gr.site_id = mega_mb.site_id
          AND mega_gr.spot_channel = mega_mb.spot_channel
        GROUP BY mega_gr.site_hemisphere,
                 mega_gr.site_area,
                 mega_gr.site_depth_in_um,
                 mega_gr.spot_channel
        ORDER BY mega_gr.site_hemisphere,
                 mega_gr.site_area,
                 mega_gr.site_depth_in_um,
                 mega_gr.spot_channel;
        """,
        get_engine(),
    ).sort_values(["site_depth_in_um", "count_mb_sortings"])
    num_sortings["count_sorting_pairs"] = (
        num_sortings["count_gr_sortings"] * num_sortings["count_mb_sortings"]
    )

    num_units = pd.read_sql(
        """
        SELECT
            mega.experiment_subject,
            mega.site_area,
            mega.site_hemisphere,
            mega.site_depth_in_um,
            mega.property_type,
            mega.spot_cytox_band,
            COUNT(DISTINCT mega.unit_id) AS count_units,
            COUNT(DISTINCT mega.sorting_type) AS count_sortings
        FROM derived."Megazord" mega
            INNER JOIN derived."Sorting" srt ON srt.new_sorting_id = mega.new_sorting_id
        WHERE mega.unit_type = 'MULTI_UNIT'
        GROUP BY
            experiment_subject,
            site_area,
            site_hemisphere,
            site_depth_in_um,
            property_type,
            spot_cytox_band;
        """,
        get_engine(),
    ).sort_values(["site_depth_in_um"])

    selection_id = "site-selection"
    selection_figure = (
        px.parallel_categories(
            num_units,
            color="count_units",
            color_continuous_scale=px.colors.sequential.Inferno,
            dimensions=num_units.drop(columns=["count_units"]).columns,
            labels=label_mapping(num_units.columns),
        )
        .update_layout(autosize=False, margin={"l": 10}, width=750)
        .update_traces(line_shape="hspline")
    )

    return html.Section(
        children=[
            dcc.Markdown("## 2. Location overview/selection"),
            dcc.Markdown("### By no. units"),
            dcc.Graph(selection_id, figure=selection_figure),
            dcc.Markdown("### By no. sortings"),
            dcc.Graph(
                figure=px.parallel_categories(
                    num_sortings,
                    color="count_gr_sortings",
                    color_continuous_scale=px.colors.sequential.Inferno,
                    dimensions=num_sortings.drop(
                        columns=["count_gr_sortings", "spot_channel"]
                    ).columns,
                    labels=label_mapping(num_sortings.columns),
                )
                .update_layout(autosize=False, margin={"l": 10}, width=750)
                .update_traces(line_shape="hspline")
            ),
        ],
    )


class Variable(BaseModel):
    db_column_or_name: str
    db_entity: Optional[str]
    db_schema: Optional[str]
    long_label: Optional[str]
    short_label: str

    @classmethod
    def search(cls, common_name: str) -> "Variable":
        searches = [
            (cls._short_name_mapping(), common_name),
            (cls._full_name_mapping(), common_name),
            (cls._full_name_mapping(), f"derived_{common_name}"),
            (cls._full_name_mapping(), f"original_{common_name}"),
        ]
        for mapping, key in searches:
            variable = mapping.get(key, None)
            if variable is not None:
                return variable
        raise KeyError(
            f"Variable with common name {common_name} not found in data dictionary."
        )

    @classmethod
    @lru_cache()
    def _full_name_mapping(cls) -> Dict[str, "Variable"]:
        df = cls._data_dict()
        df["full_name"] = df.apply(
            lambda row: f"{row['db_schema']}_{row['db_entity']}_{row['db_column_or_name']}",
            axis=1,
        )
        mapping = {
            full_name: Variable(**row.to_dict())
            for full_name, row in df.set_index("full_name").iterrows()
        }
        return mapping

    @classmethod
    @lru_cache()
    def _short_name_mapping(cls) -> Dict[str, "Variable"]:
        df = cls._data_dict()
        mapping = {
            short_name: Variable(db_column_or_name=short_name, **row.to_dict())
            for short_name, row in df.set_index("db_column_or_name").iterrows()
        }
        return mapping

    @classmethod
    @lru_cache()
    def _data_dict(cls) -> pd.DataFrame:
        return pd.read_csv(Path(__file__).parent / "variables.csv")


def label_mapping(labels: Iterable[str]) -> Dict[str, str]:
    return {label: Variable.search(label).short_label for label in labels}


def transpose(collection: Iterable[Iterable[T]]) -> List[List[T]]:
    return [list(n_tuple) for n_tuple in zip_longest(*collection)]


def units_pca(
    *, engine: Engine, site_id: int, spot_channel: int, property_type: str
) -> go.Figure:
    # TODO: Refactor to `units` service.
    data = pd.read_sql_query(
        """
        SELECT feat.*
        FROM derived."UnitFeatures" feat
            JOIN derived."Megazord" mega USING(unit_id)
        WHERE mega.site_id = %(site_id)s
            AND mega.spot_channel = %(spot_channel)s
            AND mega.property_type = %(property_type)s
            AND mega.unit_type != 'UNSORTED'
        """,
        engine,
        index_col="unit_id",
        params={
            "site_id": site_id,
            "spot_channel": spot_channel,
            "property_type": property_type,
        },
    ).dropna(axis=1)
    meta = pd.read_sql_query(
        """
        SELECT mega.unit_id, mega.unit_type AS type, mega.unit_label AS label
        FROM derived."Megazord" mega
        WHERE mega.site_id = %(site_id)s
            AND mega.spot_channel = %(spot_channel)s
            AND mega.property_type = %(property_type)s
            AND mega.unit_type != 'UNSORTED'
        """,
        engine,
        index_col="unit_id",
        params={
            "site_id": site_id,
            "spot_channel": spot_channel,
            "property_type": property_type,
        },
    )

    feat = StandardScaler().fit_transform(data)
    pca = PCA(feat.shape[1])
    pca.fit(feat)
    dims = (
        np.argmax(
            np.cumsum(pca.explained_variance_) / np.sum(pca.explained_variance_) > 0.99
        )
        + 1
    )
    data["name"] = meta.apply(
        lambda row: printable_unit_name(
            type=enums.UnitType(row["type"]), label=row["label"]
        ).capitalize(),
        axis=1,
    )
    data[[f"pca_{i}" for i in range(dims)]] = pca.transform(feat)[:, :dims]

    figure = px.scatter(
        data, x="pca_0", y="pca_1", hover_name=data.index, color="name",
    ).update_layout(
        width=500,
        height=350,
        xaxis_title="PCA 1",
        yaxis_title="PCA 2",
        title="Unit similarity",
    )
    return figure
