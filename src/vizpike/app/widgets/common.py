from typing import Optional

import numpy as np
from plotly import graph_objs as go
from scipy.signal import resample

from vizpike import services
from vizpike.app.caching import cache_on, cache_size
from vizpike.db import DBSession

from .lib import (
    WidgetSize,
    _standard_expiry,
    hex_to_rgba,
    printable_unit_name,
    qualitative_unit_color,
    with_cached_json,
    with_placeholder,
)


@with_placeholder(1.5, 1.0)
@with_cached_json(
    key=lambda db, unit_id, size, samples: ";".join(
        [f"{unit_id=}", f"size={size.name}", f"{samples=}"]
    ),
    l1_size=cache_size(
        enums=[WidgetSize],
        samples=1,
        units_per_task_item=12,  # Usually <= 4 MB + 8 gratings per item
        task_items=4,
    ),
    l2_expiry=None,
)
def waveforms(
    db: DBSession, multi_unit_id: int, size: WidgetSize, samples: Optional[int] = None,
) -> go.Figure:
    """Plot mean waveforms as solid lines and the regions within 1 standard deviation of the corresponding means as
    transparent regions, given a collection of units.
    """
    figure = _waveforms(db, multi_unit_id, samples=samples)

    pixels = size.pixels

    layout_updates = {
        WidgetSize.SMALL: dict(
            autosize=False,
            height=pixels,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
            width=int(1.25 * pixels),
            xaxis_showticklabels=False,
            yaxis_showticklabels=False,
        ),
        WidgetSize.MEDIUM: dict(
            autosize=False,
            width=int(1.5 * pixels),
            height=pixels,
            margin={"l": 0, "r": 128, "t": 32, "b": 0},
            xaxis_title="Spike time (µs)",
            yaxis_title="Voltage (mV)",
            xaxis_showticklabels=True,
            yaxis_showticklabels=True,
        ),
    }
    traces_updates = {
        WidgetSize.SMALL: dict(showlegend=False),
        WidgetSize.MEDIUM: dict(showlegend=True),
    }

    figure.update_layout(**layout_updates[size])
    figure.update_traces(**traces_updates[size])

    return figure


@cache_on(
    key=lambda db, unit_id, samples: f"{unit_id=};{samples=}",
    l1_size=cache_size(
        samples=1,
        units_per_task_item=12,  # Usually <= 4 MB + 8 gratings per item
        task_items=4,
    ),
    l2_expiry=_standard_expiry,
)
def _waveforms(
    db: DBSession, multi_unit_id: int, samples: Optional[int] = None,
) -> go.Figure:
    multi_unit = services.units(db).get_by_id(multi_unit_id)
    try:
        single_units = services.units(db).get_single_unit_splits_for(multi_unit)
    except AssertionError:
        # TODO: Should be replaced by a specific error type
        single_units = []

    detection = multi_unit.property.sorting.detection
    wf_duration = (detection.waveform_samples / detection.source.sampling).to("us").m
    samples = samples or detection.waveform_samples

    if single_units:
        rendered_units = single_units
    else:
        rendered_units = [multi_unit]

    figure = go.Figure()
    time_values = np.linspace(0, wf_duration, samples, endpoint=False)
    units_by_name = {
        printable_unit_name(unit=unit, label=i + 1).capitalize(): (
            unit,
            qualitative_unit_color(unit, label=i + 1),
        )
        for i, unit in enumerate(rendered_units)
    }

    for unit_name, (unit, unit_color) in sorted(units_by_name.items()):
        waveform_mean = np.array(unit.waveform_mean_in_mv)
        waveform_deviation = np.array(unit.waveform_deviation_in_mv)

        if (not waveform_mean.shape) or (not waveform_deviation.shape):
            continue

        figure.add_trace(
            go.Scatter(
                x=np.hstack([time_values, time_values[::-1]]),
                y=np.hstack(
                    [
                        resample(waveform_mean + waveform_deviation, samples),
                        resample(waveform_mean - waveform_deviation, samples)[::-1],
                    ]
                ),
                fill="toself",
                fillcolor=hex_to_rgba(unit_color, alpha=0.5),
                hoveron="points",
                mode="none",
                name=unit_name,
            )
        )
        # figure.add_trace(
        #     go.Scatter(
        #         x=time_values,
        #         y=resample(waveform_mean, samples),
        #         line={"color": unit_color},
        #         showlegend=False
        #     )
        # )

    figure.add_vline(
        x=wf_duration * detection.samples_before_alignment / samples,
        line_dash="dash",
        annotation_text="Alignment",
    )

    return figure
