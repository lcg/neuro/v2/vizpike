from typing import Sequence, Tuple

import numpy as np
import pandas as pd
from plotly import express as px
from plotly import graph_objs as go
from scipy.ndimage import zoom
from skimage.morphology import octagon

from vizpike import services
from vizpike.app.caching import cache_on, cache_size
from vizpike.db import DBSession

from .lib import (
    WidgetSize,
    _standard_expiry,
    cycled,
    with_cached_json,
    with_placeholder,
)


@with_placeholder(1.0, 1.0)
@with_cached_json(
    key=lambda db, unit_id, *, size, use_spline: ";".join(
        [f"{unit_id=}", f"size={size.name}", f"{use_spline=}"]
    ),
    l1_size=cache_size(
        enums=[WidgetSize], bools=1, units_per_task_item=4, task_items=4,
    ),
    l2_expiry=None,
)
def polargram(
    db: DBSession, unit_id: int, *, size: WidgetSize, use_spline: bool = False,
) -> go.Figure:
    """Plot a polargram of unit responses in polar axes. Each polargram vertex corresponds to the fraction of the mean
    response along the vertex's direction that falls within the region of peak activity. Therefore it is an indicator of
    direction selectivity.
    """
    figure = _polargram(db, unit_id, use_spline=use_spline)
    pixels = size.pixels

    layout_updates = {
        WidgetSize.SMALL: dict(
            height=pixels,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
            # polar_radialaxis_showticklabels=False,
            # polar_angularaxis_showticklabels=False,
            width=pixels,
        ),
        WidgetSize.MEDIUM: dict(
            autosize=False,
            height=pixels,
            margin={"l": 32, "r": 32, "t": 32, "b": 32},
            width=pixels,
        ),
    }
    figure.update_layout(**layout_updates[size])

    return figure


@with_placeholder(1.0, 1.0)
@with_cached_json(
    key=lambda db, unit_id, *, size: f"{unit_id=};size={size.name}",
    l1_size=cache_size(enums=[WidgetSize], units_per_task_item=4, task_items=4,),
    l2_expiry=None,
)
def response_map(db: DBSession, unit_id: int, *, size: WidgetSize) -> go.Figure:
    """Plots the response map as a heatmap. A response map encodes the estimated response at each point of the visual
    field.
    """
    scale_by_size = {
        WidgetSize.SMALL: 0.2,
        WidgetSize.MEDIUM: 0.5,
    }
    layout_updates = {
        WidgetSize.MEDIUM: dict(
            margin={"l": 0, "r": 32, "t": 32, "b": 0},
            xaxis_title="Visual field X (°)",
            yaxis_title="Visual field Y (°)",
        ),
        WidgetSize.SMALL: dict(
            height=size.pixels,
            width=size.pixels,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
            xaxis_showticklabels=False,
            yaxis_showticklabels=False,
        ),
    }

    figure = _response_map(
        db,
        unit_id,
        zscale=(-3, 3),
        scale=scale_by_size[size],
        showscale=(size is not WidgetSize.SMALL),
    )
    figure.update_layout(**layout_updates[size])

    return figure


@cache_on(
    key=lambda db, unit_id, *, use_spline: f"{unit_id=};{use_spline=}",
    l1_size=cache_size(bools=1, units_per_task_item=4, task_items=4,),
    l2_expiry=_standard_expiry,
)
def _polargram(db: DBSession, unit_id: int, *, use_spline: bool = False) -> go.Figure:
    unit = services.units(db).get_by_id(unit_id)

    data = cycled(
        pd.DataFrame(
            {
                "direction": sorted(
                    [
                        s.direction_in_deg
                        for s in unit.spot.experiment.protocol.stimuli
                        if s.is_stimulus and not s.is_neutral
                    ]
                ),
                "intensity": unit.polargram,
            }
        )
    )
    figure = px.line_polar(
        data,
        r="intensity",
        theta="direction",
        line_shape="spline" if use_spline else "linear",
        start_angle=0,
        direction="counterclockwise",
        markers=True,
    )
    return figure


@cache_on(
    key=lambda db, unit_id, *, scale, zscale, showscale: ";".join(
        [f"{unit_id=}", f"{scale=}", f"{zscale=}", f"{showscale=}"]
    ),
    l1_size=cache_size(scales=2, zscales=1, units_per_task_item=4, task_items=4,),
    l2_expiry=_standard_expiry,
)
def _response_map(
    db: DBSession,
    unit_id: int,
    *,
    scale: float = 0.5,
    zscale: Tuple[float, float] = (-3.0, 3.0),
    showscale=True,
) -> go.Figure:
    unit = services.units(db).get_by_id(unit_id)

    visual_field_amplitude = unit.spot.experiment.protocol.amplitude_in_deg
    response_map = zoom(unit.response_map, scale)
    _mask_response_map(response_map)

    height, width = response_map.shape
    x = visual_field_amplitude * np.linspace(-0.5, 0.5, width)
    y = visual_field_amplitude * np.linspace(-0.5, 0.5, height)

    pixels = WidgetSize.MEDIUM.pixels

    figure = go.Figure(
        data=go.Heatmap(
            x=x,
            y=y,
            z=response_map,
            colorscale=px.colors.sequential.RdBu[::-1],
            zmin=zscale[0],
            zmax=zscale[1],
            zsmooth="fast",
            colorbar={"title": "Z response"},
        ),
    )
    figure.update_traces(showscale=showscale)
    figure.update_layout(
        autosize=False,
        width=pixels + 64,
        height=pixels,
        margin={"l": 0, "r": 0, "t": 0, "b": 0},
    )
    _ensure_axis_range(figure, x, y)

    rf_contour = unit.rf_contour.to("deg").m
    rf_center = unit.rf_center.to("deg").m
    figure.add_scatter(
        x=rf_contour[:, 0],
        y=-rf_contour[:, 1],
        # TODO: Check orientation. It's possibly wrong either in the RF computation or in the response map's.
        line=dict(color="black", width=1.0),
        hoverinfo="skip",
        showlegend=False,
    )
    _ensure_axis_range(figure, x, y)

    figure.add_annotation(
        x=rf_center[0], y=-rf_center[1], text=f"RF (Z ≥ {unit.rf_response_cutoff:.2f})"
    )
    _ensure_axis_range(figure, x, y)

    return figure


def _ensure_axis_range(figure: go.Figure, x: Sequence[float], y: Sequence[float]):
    figure.update_layout(xaxis_range=[x[0], x[-1]], yaxis_range=[y[0], y[-1]])


def _mask_response_map(response_map: np.array):
    """Mask a response map with an octagonal mask. Values outside the square-
    inscribed octagon are set to NaN.
    TODO: This is a temporary fix for release 1.0.0. Not all response maps are octagonal, so this should be fixed during computation of the response maps in the data pipeline.
    """
    width = len(response_map)
    oct_side = int(np.round(width * np.tan(np.radians(22.5))))
    width_minus_side = (width - oct_side) // 2
    response_map[~octagon(oct_side, width_minus_side).astype(bool)] = np.nan
