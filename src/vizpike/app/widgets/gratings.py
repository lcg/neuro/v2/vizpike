import warnings
from enum import Enum
from functools import lru_cache

import numpy as np
import pandas as pd
from nearest_colours import nearest_w3c
from pandas.errors import PerformanceWarning
from plotly import express as px
from plotly import graph_objs as go

from vizpike import services
from vizpike.app.caching import cache_on, cache_size
from vizpike.db import DBSession
from vizpike.orm.schemas.dataset import derived

from .lib import WidgetSize, _standard_expiry, with_cached_json, with_placeholder


class GratingsAnalysis(Enum):
    COLOR_DIRECTION = ("fg_bg_colors", "direction", "color")
    COLOR_ORIENTATION = ("fg_bg_colors", "orientation", "color")
    CONTRAST_DIRECTION = ("contrast", "direction", "contrast")
    CONTRAST_ORIENTATION = ("contrast", "orientation", "contrast")
    FREQUENCY_DIRECTION = ("frequency", "direction", "spatial_freq")
    FREQUENCY_ORIENTATION = ("frequency", "orientation", "spatial_freq")
    SPEED_DIRECTION = ("speed", "direction", "speed")
    SPEED_ORIENTATION = ("speed", "orientation", "speed")


class GratingsResponse(Enum):
    SPIKES = "spikes"
    FIRING_RATE = "fr"


@with_placeholder(1.0, 1.0)
@with_cached_json(
    key=lambda db, unit_id, analysis, *, response, size, colorbar: ";".join(
        [
            f"{unit_id=}",
            f"analysis={analysis.name}",
            f"response={response.name}",
            f"size={size.name}",
            f"colorbar={colorbar}",
        ]
    ),
    l1_size=cache_size(
        enums=[GratingsAnalysis, GratingsResponse, WidgetSize],
        bools=1,
        units_per_task_item=8,
        task_items=4,
    ),
    l2_expiry=None,
)
def kaleidomap(
    db: DBSession,
    unit_id: int,
    analysis: GratingsAnalysis,
    *,
    response: GratingsResponse,
    size: WidgetSize,
    colorbar=False,
) -> go.Figure:
    figure = go.Figure(_kaleidomap_base(db, unit_id, analysis, response))
    pixels = size.pixels - (48 if size == WidgetSize.MEDIUM else 0)

    figure.update_layout(
        width=pixels + (80 if colorbar else 0),
        height=pixels,
        coloraxis_showscale=colorbar,
    )

    layout_by_size = {
        WidgetSize.SMALL: dict(
            hovermode=False,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
            polar_radialaxis_showticklabels=False,
            polar_angularaxis_showticklabels=False,
        ),
        WidgetSize.MEDIUM: dict(margin={"l": 32, "r": 32, "t": 32, "b": 16},),
    }

    figure.update_layout(**layout_by_size[size])

    return figure


@cache_on(
    key=lambda db, unit_id, analysis, response, categorize: ";".join(
        [
            f"{unit_id=}",
            f"analysis={analysis.name}",
            f"response={response.name}",
            f"{categorize=}",
        ]
    ),
    l1_size=cache_size(
        bools=1, enums=[GratingsAnalysis, GratingsResponse], units_per_task_item=8,
    ),
    # Deserializing Plotly figures from pickle/json is not very efficient, so L2 does not pay off here.
    l2_expiry=0,
)
def _kaleidomap_base(
    db: DBSession,
    unit_id: int,
    analysis: GratingsAnalysis,
    response: GratingsResponse,
    categorize=True,
) -> go.Figure:
    unit = services.units(db).get_by_id(unit_id)

    cols_mapping = {
        "spikes": "Avg. spikes",
        "fr": "FR (Hz)",
        "contrast": "Contrast",
        "direction": "Dir. (deg)",
        "orientation": "Orient. (deg)",
        "frequency": "Spat. Freq. (1/deg)",
        "speed": "Speed (Hz)",
        "fg_bg_colors": "Color",
    }
    radial_col, angular_col, group = analysis.value

    df = _gratings_analysis(unit, analysis)
    df_renamed = df.rename(columns=cols_mapping)

    if angular_col == "orientation":
        range_theta = [-22.5, 180]
    else:
        range_theta = [0, 360]

    vals = df[radial_col].unique()

    if (radial_col == "fg_bg_colors") or categorize:
        num_vals = len(vals)
        replacement = [1 + 10 ** (-5 - i) for i in range(num_vals)]
        ticks = np.arange(num_vals) + 0.5
    else:
        replacement = np.hstack((vals[:1], np.diff(vals)))
        ticks = np.hstack((vals[:1] / 2, vals[1:] - 0.5 * np.diff(vals)))

    radialaxis = dict(tickmode="array", tickvals=ticks, ticktext=vals)
    df_renamed["r"] = df[radial_col].replace(vals, replacement)

    def formatting(col):
        if col == "r":
            return False
        elif col == cols_mapping["fg_bg_colors"]:
            return True
        else:
            return ":.1f"

    colorscales = {
        GratingsResponse.FIRING_RATE: px.colors.sequential.RdBu[::-1],
        GratingsResponse.SPIKES: px.colors.sequential.PuRd,
    }
    colorranges = {
        GratingsResponse.FIRING_RATE: {
            "cauto": True,
            "cmid": unit.basal_rate_in_hertz,
        },
        GratingsResponse.SPIKES: {"cmin": 0, "cmax": df[response.value].max()},
    }

    figure = (
        px.bar_polar(
            df_renamed,
            r="r",
            theta=cols_mapping[angular_col],
            color=cols_mapping[response.value],
            direction="counterclockwise",
            color_continuous_scale=colorscales[response],
            hover_data={col: formatting(col) for col in df_renamed.columns},
            start_angle=0,
            range_theta=range_theta,
        )
        .update_polars(hole=0.25, radialaxis=radialaxis)
        .update_coloraxes(**colorranges[response])
    )

    return figure


@cache_on(
    key=lambda unit, analysis: ";".join(
        [f"unit_id={unit.id}", f"analysis={analysis.name}"]
    ),
    l1_size=cache_size(
        enums=[GratingsAnalysis, GratingsResponse], units_per_task_item=8,
    ),
    l2_expiry=_standard_expiry,
)
def _gratings_analysis(unit: derived.Unit, analysis: GratingsAnalysis) -> pd.DataFrame:
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PerformanceWarning)

        base_df = _gratings_df(unit)
        base_df["fg_bg_colors"] = base_df["fg_bg_colors"].apply(
            lambda fgbg: " / ".join(
                [_named_color(color_hex) for color_hex in fgbg.split(" / ")]
            ).capitalize()
        )

        radial_col, angular_col, group = analysis.value
        df = (
            base_df.query(f"groups.str.contains('{group}')")
            .groupby([radial_col, angular_col])
            .mean()
            .reset_index()
        )
        return df


@cache_on(
    key=lambda unit: f"unit_id={unit.id}",
    l1_size=cache_size(units_per_task_item=8),
    l2_expiry=_standard_expiry,
)
def _gratings_df(unit: derived.Unit) -> pd.DataFrame:
    protocol = unit.spot.experiment.protocol
    stimuli = [
        (stim)
        for stim in protocol.stimuli
        if stim.is_stimulus and (not stim.is_neutral)
    ]
    trial_time_in_ms = protocol.trial_time_in_ms

    df = pd.DataFrame(
        [
            {
                "spikes": unit.spike_counts[s][t],
                "fr": 1000 * unit.spike_counts[s][t] / trial_time_in_ms,
                "contrast": stim.contrast,
                "direction": stim.direction_in_deg,
                "orientation": stim.orientation_in_deg,
                "frequency": stim.spatial_freq_in_invdeg,
                "speed": stim.speed_in_hz,
                "fg_color": stim.fg_color_in_hex,
                "bg_color": stim.bg_color_in_hex,
                "fg_bg_colors": f"{stim.fg_color_in_hex} / {stim.bg_color_in_hex}",
                "groups": str(stim.groups),
            }
            for s, stim in enumerate(stimuli)
            for t in range(protocol.trials_per_condition)
        ]
    )

    return df


@lru_cache()
def _named_color(color_hex: str) -> str:
    return nearest_w3c(color_hex)[0].get_web()
