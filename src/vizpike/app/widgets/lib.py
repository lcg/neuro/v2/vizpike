from enum import Enum
from functools import wraps
from typing import Callable, Optional, Protocol

import numpy as np
import pandas as pd
import plotly.io
from colour import Color
from plotly import express as px
from plotly import graph_objs as go
from pydantic import BaseSettings

from vizpike.app.caching import cache_on
from vizpike.db import DBSession
from vizpike.orm.schemas.dataset import derived, enums


class WidgetSize(Enum):
    SMALL = 120
    MEDIUM = 350

    @property
    def pixels(self) -> int:
        return self.value


class Config(BaseSettings):
    widget_expiry_in_seconds: int


def unit_ids_key(*units: derived.Unit):
    return "unit_ids=" + ",".join(map(str, sorted(u.id for u in units)))


def cycled(df: pd.DataFrame) -> pd.DataFrame:
    """Repeats the first row of data frame after the last one."""
    return df.loc[np.hstack((df.index, df.index[:1]))]


def hex_to_rgba(color: str, alpha: float) -> str:
    """Convert an HTML hex-coded RGB color to an HTML RGBA constructor, with the specified alpha."""
    rgb = (255 * np.array(Color(color).rgb)).astype(int)
    return f"rgba({rgb[0]}, {rgb[1]}, {rgb[2]}, {alpha})"


def qualitative_unit_color(unit: derived.Unit, label: int = None) -> str:
    colors = px.colors.qualitative.Plotly

    if unit.type is derived.enums.UnitType.MULTI_UNIT:
        return colors[-1]
    elif unit.type is derived.enums.UnitType.MULTI_UNIT_WITHOUT_UNSORTED:
        return colors[-2]
    elif unit.type is derived.enums.UnitType.UNSORTED:
        return "#888888"
    elif unit.type is derived.enums.UnitType.SINGLE_UNIT:
        return colors[(label or unit.label) - 1]
    else:
        raise ValueError(
            f"Unit type not supported in waveform summary plot: {unit.type}"
        )


def printable_unit_name(
    *, unit: derived.Unit = None, type: enums.UnitType = None, label: int = None
) -> str:
    if unit is not None:
        type = unit.type
        label = unit.label

    name = {
        enums.UnitType.UNSORTED: lambda: "unsorted",
        enums.UnitType.MULTI_UNIT_WITHOUT_UNSORTED: lambda: "multi-unit",
        enums.UnitType.MULTI_UNIT: lambda: "multi-unit + unsorted",
        enums.UnitType.SINGLE_UNIT: lambda: f"single-unit {chr(ord('a') + int(label) - 1)}",
    }[type]()

    return name


class FigureFactory(Protocol):
    def __call__(self, db: DBSession, unit_id: int, *args, **kwargs) -> go.Figure:
        pass


class FigureJSONFactory(Protocol):
    def __call__(self, db: Optional[DBSession], unit_id: int, *args, **kwargs) -> str:
        pass


class FigureFactoryWithCachedJSONFactory(FigureFactory):
    def __call__(self, db: DBSession, unit_id: int, *args, **kwargs) -> go.Figure:
        pass

    @property
    def __json__(self) -> FigureJSONFactory:
        pass


def with_cached_json(
    key: Callable[..., str], **caching_kwargs,
) -> Callable[[FigureFactory], FigureFactoryWithCachedJSONFactory]:
    def decorator(function):
        @wraps(function)
        def json(db: DBSession, unit_id: int, *args, **kwargs) -> str:
            return plotly.io.to_json(function(db, unit_id, *args, **kwargs))

        json.__name__ = f"{function.__name__}.__json__"
        function.__json__ = cache_on(key, **caching_kwargs)(json)

        return function

    return decorator


def with_placeholder(xfactor: float, yfactor: float):
    placeholders_by_size = {
        size: (
            px.scatter(x=[-0.5, 0, 0.5], y=[-0.5, 0, 0.5],)
            .update_layout(
                width=int(xfactor * size.pixels),
                height=int(yfactor * size.pixels),
                margin={"l": 10, "r": 10, "t": 30, "b": 10},
                xaxis={"range": (-1, 1)},
                yaxis={"range": (-1, 1)},
                title={"text": "Loading...", "x": 0.55, "xanchor": "center"},
            )
            .update_xaxes(tickangle=0, title_text=None)
            .update_yaxes(title_text=None)
        )
        for size in WidgetSize
    }

    def decorator(widget):
        def placeholder(size: WidgetSize):
            return placeholders_by_size[size]

        widget.__placeholder__ = placeholder
        return widget

    return decorator


_standard_expiry = 300
