from enum import Enum
from typing import List, Tuple

import pandas as pd

from vizpike.orm.schemas.dataset import derived, original


class GratingsOrdering(Enum):
    DIRECTION_FIRST = (
        "direction_in_deg",
        "contrast",
        "spatial_freq_in_invdeg",
        "speed_in_hz",
        "bg_color_in_hex",
        "fg_color_in_hex",
    )


def gratings_unit_df(
    unit: derived.Unit, ordering: GratingsOrdering = GratingsOrdering.DIRECTION_FIRST,
) -> pd.DataFrame:
    protocol = unit.spot.experiment.protocol
    stimuli = ordered_gratings_stimuli(protocol, ordering)
    df = pd.DataFrame(
        [
            {
                "spikes": unit.spike_counts[s][t],
                "trial": t + 1,
                "stim": s,
                "contrast": stim.contrast,
                "direction": stim.direction_in_deg,
                "orientation": stim.orientation_in_deg,
                "frequency": stim.spatial_freq_in_invdeg,
                "speed": stim.speed_in_hz,
                "fg_color": stim.fg_color_in_hex,
                "bg_color": stim.bg_color_in_hex,
            }
            for s, stim in stimuli
            for t in range(protocol.trials_per_condition)
        ]
    )
    return df


def ordered_gratings_stimuli(
    protocol: original.Protocol, ordering: GratingsOrdering
) -> List[Tuple[int, original.GratingStimulus]]:
    relevant_stimuli = [
        stim for stim in protocol.stimuli if stim.is_stimulus and (not stim.is_neutral)
    ]
    stimuli = sorted(
        enumerate(relevant_stimuli),
        key=lambda s_stim: tuple(getattr(s_stim[1], attr) for attr in ordering.value),
    )
    return stimuli
