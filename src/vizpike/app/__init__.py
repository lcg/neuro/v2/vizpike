from dataclasses import dataclass
from enum import Enum
from typing import Optional
from uuid import UUID

import dash_bootstrap_components as dbc
from dash import Dash, callback_context, dcc, html
from dash.dependencies import Input, Output, State
from dash.development.base_component import Component
from dash.exceptions import PreventUpdate
from pydantic import UUID4, BaseModel, EmailStr

from vizpike import __version__, services
from vizpike.app.utils import AppSettings
from vizpike.app.utils import callbacks as cb
from vizpike.app.utils import profiler, redis_cache
from vizpike.db import DBSession, get_engine
from vizpike.orm.schemas.app import Session
from vizpike.services.sessions import (
    IncorrectLogin,
    InvalidSession,
    SessionCreationFailed,
    SessionExpired,
)


class SessionError(Enum):
    LOGIN_FAILED = "LOGIN_FAILED"
    INVALID_SESSION = "INVALID_SESSION"
    SESSION_EXPIRED = "SESSION_EXPIRED"


class SessionData(BaseModel):
    session_uuid: Optional[UUID4]
    user_email: Optional[EmailStr]
    error: Optional[SessionError]

    @classmethod
    def from_session(cls, session: Session) -> "SessionData":
        return SessionData(session_uuid=session.uuid, user_email=session.user.email,)


@dataclass
class AppControl:
    session_uuid: UUID

    @classmethod
    def login(cls, db: DBSession, *, email: str, password: str) -> SessionData:
        try:
            session = services.sessions(db).login(email=email, password=password)
            db.commit()

            control = AppControl(session.uuid)
            control._stage_session(session)

            return SessionData.from_session(session)
        except IncorrectLogin:
            return SessionData(error=SessionError.LOGIN_FAILED)
        except SessionCreationFailed:
            return SessionData(error=SessionError.INVALID_SESSION)

    @property
    def app_expiry(self) -> int:
        return int(redis_cache.hget(self.hash_group, "app.expiry"))

    @property
    def hash_group(self) -> str:
        return f"session_uuid={self.session_uuid}"

    def logout(self, db: DBSession) -> SessionData:
        try:
            services.sessions(db).logout(self.session_uuid)
            db.commit()
            self._unstage_session()
            return SessionData()
        except InvalidSession:
            return SessionData(error=SessionError.INVALID_SESSION)
        except SessionExpired:
            # For controlling purposes, we can consider an attempt to log out of an
            # expired session as a no-op, which is why the error is supressed below.
            return SessionData()

    def refresh_session(self, db: DBSession):
        try:
            session = services.sessions(db).checkin(self.session_uuid)
            db.commit()
            self._stage_session(session)
        except SessionExpired:
            self._unstage_session()
            raise

    def _stage_session(self, session: Session):
        expiry = session.max_duration_in_seconds

        redis_cache.hset(self.hash_group, "app.expiry", expiry)
        redis_cache.expire(self.hash_group, expiry)

    def _unstage_session(self):
        redis_cache.delete(self.hash_group)


class DashApp:
    def __init__(self):
        self.config = AppSettings()
        self.engine = get_engine()
        self.profile = profiler.profile

        self.id_prefix = "app"
        self.redis = redis_cache
        self.dash = Dash(
            __name__,
            external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP],
            title=f"Vizpike {__version__}",
        )

        self.location = dcc.Location(
            f"{self.id_prefix}-url", pathname="/", refresh=True,
        )
        self.session_data = dcc.Store(
            f"{self.id_prefix}-session", storage_type="session", data={}
        )

        self.routes = {}

        # TODO: Refactor auth and routing
        login_button = dbc.Button(
            "Login", id=f"{self.id_prefix}-login-button", type="submit"
        )
        logout_button = dbc.Button(
            "Logout", id=f"{self.id_prefix}-logout-button", type="submit", size="sm"
        )
        login_email = dbc.Input(
            id=f"{self.id_prefix}-login-email", type="email", placeholder="Email"
        )
        login_password = dbc.Input(
            id=f"{self.id_prefix}-login-password",
            type="password",
            placeholder="Password",
        )

        login_panel = dbc.Container(
            [
                dbc.Row(
                    dbc.Col(
                        [
                            login_message := dbc.Alert(
                                id=f"{self.id_prefix}-login-message",
                                color="danger",
                                dismissable=True,
                            )
                        ],
                        width={"size": 4, "offset": 4},
                    )
                ),
                dbc.Row(dbc.Col([login_email], width={"size": 4, "offset": 4})),
                dbc.Row(dbc.Col([login_password], width={"size": 4, "offset": 4})),
                dbc.Row(dbc.Col([login_button], width={"size": 4, "offset": 4})),
            ],
            id=f"{self.id_prefix}-login-panel",
        )

        logout_panel = dbc.Container(
            [
                dbc.Row(
                    dbc.Col(
                        [
                            logout_message := html.Span(
                                id=f"{self.id_prefix}-logout-message"
                            ),
                            html.Span(" "),
                            logout_button,
                        ],
                        width={"size": 4, "offset": 4},
                    ),
                )
            ],
            id=f"{self.id_prefix}-logout-panel",
        )

        self.dash.layout = html.Div(
            [
                self.location,
                self.session_data,
                session_refresh_timer := dcc.Interval(
                    id=f"{self.id_prefix}-session-refresh-timer", interval=5000
                ),
                session_refresh_dummy := dcc.Store(
                    id=f"{self.id_prefix}-session-refresh-dummy", storage_type="session"
                ),
                login_panel,
                logout_panel,
                html.P(),
                content := html.Div(id=f"{self.id_prefix}-content"),
            ]
        )
        self.dash.validation_layout = html.Div(self.dash.layout.children[:])

        @self.dash.callback(
            Output(content.id, "children"),
            Output(login_panel.id, "style"),
            Output(login_message.id, "children"),
            Output(logout_panel.id, "style"),
            Output(logout_message.id, "children"),
            Input(self.location.id, "pathname"),
            Input(self.session_data.id, "data"),
        )
        @cb.marshall
        @cb.inject_db
        def route(db: DBSession, url: str, session_data: SessionData):
            error_messages = {
                SessionError.INVALID_SESSION: "Invalid session. Review your credentials and try to login again.",
                SessionError.LOGIN_FAILED: "Login failed. Check your username and password.",
                SessionError.SESSION_EXPIRED: "Your user session has expired. Please log in again.",
                None: "You need to authenticate first.",
            }

            def error_outputs(session_error: SessionError):
                return (
                    None,
                    {},
                    error_messages[session_error],
                    {"display": "none"},
                    None,
                )

            def normal_outputs():
                return (
                    self.routes[url.rstrip("/") or "/"],
                    {"display": "none"},
                    None,
                    {},
                    f"Logged in as {session_data.user_email}",
                )

            if session_data.session_uuid is None:
                return error_outputs(session_data.error)
            else:
                try:
                    AppControl(session_data.session_uuid).refresh_session(db)
                    return normal_outputs()
                except SessionExpired:
                    return error_outputs(SessionError.SESSION_EXPIRED)
                except InvalidSession:
                    return error_outputs(SessionError.INVALID_SESSION)

        @self.dash.callback(
            Output(self.session_data.id, "data"),
            Input(login_button.id, "n_clicks"),
            Input(logout_button.id, "n_clicks"),
            State(login_email.id, "value"),
            State(login_password.id, "value"),
            State(self.session_data.id, "data"),
        )
        @cb.marshall
        @cb.inject_db
        def update_session_data_on_login_or_logout(
            db: DBSession,
            login_clicks: int,
            logout_clicks: int,
            login_email: str,
            login_password: str,
            session_data: SessionData,
        ) -> SessionData:
            is_login_action = (
                callback_context.triggered[0]["prop_id"]
                == f"{login_button.id}.n_clicks"
            )

            if (login_clicks is None) and (logout_clicks is None):
                return session_data
            elif is_login_action:
                return AppControl.login(db, email=login_email, password=login_password)
            else:
                return AppControl(session_data.session_uuid).logout(db)

        @self.dash.callback(
            Output(session_refresh_timer.id, "interval"),
            Input(self.session_data.id, "data"),
        )
        @cb.marshall
        def update_refresh_timer_on_session_data_available(
            session_data: SessionData,
        ) -> int:
            if session_data.session_uuid is None:
                raise PreventUpdate()
            else:
                two_min = 120_000
                fifth_of_app_exipiry = int(
                    1000 * 0.2 * AppControl(session_data.session_uuid).app_expiry
                )
                return min(two_min, fifth_of_app_exipiry)

        @self.dash.callback(
            Output(session_refresh_dummy.id, "data"),
            Input(session_refresh_timer.id, "n_intervals"),
            Input(self.session_data.id, "data"),
        )
        @cb.marshall
        @cb.inject_db
        def refresh_session_on_session_refresh_timer(
            db: DBSession, n_intervals: int, session_data: SessionData
        ) -> int:
            if n_intervals is None or session_data.session_uuid is None:
                raise PreventUpdate()

            AppControl(session_data.session_uuid).refresh_session(db)
            return n_intervals

    def add_page(self, route: str, page: Component):
        self.routes[route] = page
        self.dash.validation_layout.children.append(page)
