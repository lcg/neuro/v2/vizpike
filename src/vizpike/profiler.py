import os
from csv import DictWriter
from http import HTTPStatus
from pathlib import Path

from flask import Flask, request
from pydantic import BaseSettings


class Settings(BaseSettings):
    file: Path

    class Config:
        env_prefix = "PROFILER_"


settings = Settings()

log_file = settings.file
log_stream = open(log_file, "a", buffering=1)
log_writer = DictWriter(
    log_stream, ["start_time", "end_time", "module", "function", "info"]
)

if os.stat(settings.file).st_size == 0:
    log_writer.writeheader()

app = Flask(__name__)


@app.route("/flush", methods=["POST"])
def flush_endpoint():
    log_stream.flush()
    return "", HTTPStatus.OK


@app.route("/log", methods=["POST"])
def log_endpoint():
    log_writer.writerow(request.get_json())
    return "", HTTPStatus.NO_CONTENT


@app.route("/reset", methods=["POST"])
def reset_endpoint():
    log_stream.truncate(0)
    log_writer.writeheader()
    return "", HTTPStatus.OK


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
