import re
import sys
import traceback
from collections import OrderedDict
from contextlib import contextmanager
from pathlib import Path
from typing import Callable, List, Tuple, NoReturn

from sqlalchemy.engine import Engine
from tqdm import tqdm

from vizpike.orm import Base
from vizpike.orm.schemas import app, dataset
from vizpike.pipeline.dataset import load_derived_entity, load_original_entity

LoggingFunction = Callable[[str], NoReturn]


@contextmanager
def logging(logging_function: LoggingFunction, message: str, *, end: str = "Done."):
    try:
        logging_function(message)
        yield
    except Exception as exc:
        _, _, exc_traceback = sys.exc_info()
        tb = "\t" + "\n\t".join(traceback.format_tb(exc_traceback))
        logging_function(tb)

        raise exc
    finally:
        logging_function(end)


def _discard_logs(message: str):
    pass
    

_this_dir = Path(__file__).parent
tqdm.pandas(desc="Pandas", leave=False, unit="row")


def initialize(db_engine: Engine, *, fresh_start=False, logging_function: LoggingFunction = _discard_logs):
    """Create mapped tables in the given engine, optionally wiping out any existing data. If not starting out fresh, any
    existing tables are skipped.
    """

    schemas = [
        app.SCHEMA,
        dataset.original.SCHEMA,
        dataset.derived.SCHEMA,
    ]

    if fresh_start:
        with logging(logging_function, "Wiping out existing schemas first..."):
            _drop_schemas(db_engine, schemas)

    with logging(logging_function, "Creating DB schemas..."):
        _create_schemas(db_engine, schemas)

    with logging(logging_function, "Creating DB schemas"):
        _create_tables(db_engine)

    with logging(logging_function, "Creating DB views..."):
        _create_views(db_engine)


def populate(db_engine: Engine, *, fresh_start=False, skip_derived: Tuple[str] = (), logging_function = _discard_logs):
    """Populate tables in given engine with dataset's CSV metadata. All known schemas will be populated with all table
    data found.
    """
    initialize(db_engine, fresh_start=fresh_start)

    with logging(logging_function, "Populating database with original dataset elements..."):
        _populate_original(db_engine)

    with logging(logging_function, "Populating database with derived dataset elements..."):
        _populate_derived(db_engine, skip_derived=skip_derived)


def _drop_schemas(db_engine: Engine, schemas: List[str]):
    for s in tqdm(schemas, desc="Dropping schemas"):
        db_engine.execute(f"DROP SCHEMA IF EXISTS {s} CASCADE;")


def _create_schemas(db_engine: Engine, schemas: List[str]):
    for s in tqdm(schemas, desc="Creating schemas"):
        db_engine.execute(f"CREATE SCHEMA IF NOT EXISTS {s};")


def _create_tables(db_engine):
    Base.metadata.create_all(bind=db_engine)


def _create_views(db_engine: Engine):
    with open(_this_dir / "views.sql") as views_file:
        views_script = views_file.read()

    view_definitions = [
        vd for vd in [vd.strip() for vd in views_script.split(";")] if vd
    ]
    for vdef in tqdm(view_definitions, desc="Creating views"):
        db_engine.execute(vdef)


def _populate_original(db_engine: Engine):
    entity_csv_to_model_map = OrderedDict(
        [
            ("protocol", dataset.original.Protocol),
            ("condition", dataset.original.Stimulus),
            ("condition/gratings", dataset.original.GratingStimulus),
            ("condition/moving_bars", dataset.original.MovingBarsStimulus),
            ("mea", dataset.original.MEA),
            ("plx", dataset.original.PLX),
            ("recording", dataset.original.Experiment),
            ("site", dataset.original.Site),
            ("source", dataset.original.Source),
            ("source/spike", dataset.original.SpikeSource),
            ("sorting", dataset.original.Sorting),
            ("spot", dataset.original.Spot),
            ("trial", dataset.original.Trial),
        ]
    )
    column_mappings = {
        "recording_id": "experiment_id",
        "condition_id": "stimulus_id",
    }
    with db_engine.connect() as connection:
        for entity_csv, entity_model in tqdm(
            entity_csv_to_model_map.items(), desc="Populating original schema"
        ):
            entity_data = load_original_entity(entity_csv, use_units=False).rename(
                columns=column_mappings
            )

            _clean_entity(entity_model, entity_data)
            _persist_entity(connection, entity_model, entity_data)

        recordings_sites = (
            load_original_entity("recording_site", use_units=False)
            .reset_index()
            .rename(columns=column_mappings)
        )
        recordings_sites.to_sql(
            dataset.original.ExperimentSite.__tablename__,
            connection,
            schema=dataset.original.ExperimentSite.__table__.schema,
            if_exists="append",
            index=False,
        )


def _populate_derived(db_engine: Engine, skip_derived: Tuple[str] = ()):
    column_mappings = {}
    with db_engine.connect() as connection:
        for entity_csv, entity_model in tqdm(
            entity_csv_to_model_map.items(), desc="Populating derived schema"
        ):
            if entity_csv in skip_derived:
                continue

            entity_data = load_derived_entity(entity_csv, subtypes=False).rename(
                columns=column_mappings
            )

            tqdm.write(f"Loaded {len(entity_data)} rows for entity {entity_csv}")

            # TODO: Fix value of columns directly in dataset, which contain the hertz unit
            if entity_model is dataset.derived.OmniplexDetection:
                entity_data["waveform_sampling_in_hz"] = entity_data[
                    "waveform_sampling"
                ].apply(lambda s: float(re.match(r"(\d+\.\d+) hertz", s).groups()[0]))

            _clean_entity(entity_model, entity_data)
            _persist_entity(connection, entity_model, entity_data)

entity_csv_to_model_map = OrderedDict(
    [
        ("detection", dataset.derived.Detection),
        ("new_sorting", dataset.derived.Sorting),
        ("property", dataset.derived.Property),
        ("detection/omniplex", dataset.derived.OmniplexDetection),
        ("detection/omniplex_re_spkc", dataset.derived.SpikeContinuousDetection,),
        ("detection/omniplex_re_wb", dataset.derived.WidebandDetection),
        ("new_sorting/omniplex", dataset.derived.OmniplexSorting),
        ("new_sorting/omniplex_re", dataset.derived.PCAKmeansSorting),
        ("property/gratings", dataset.derived.GratingsProperty),
        ("property/moving_bars", dataset.derived.MovingBarsProperty),
        ("unit", dataset.derived.Unit),
    ]
)


def _clean_entity(entity_model, entity_data):
    dropped_columns = set(entity_data.columns) - set(
        c.name for c in entity_model.__table__.columns
    )
    for i in range(3):
        entity_data.replace({r"\[([^\[\]]+)\]": r"{\1}"}, regex=True, inplace=True)
    entity_data.drop(columns=dropped_columns, inplace=True)


def _persist_entity(connection, entity_model, entity_data):
    entity_data.to_sql(
        entity_model.__tablename__,
        connection,
        schema=entity_model.__table__.schema,
        if_exists="append",
        index=True,
        index_label=entity_model.id.name,
    )
