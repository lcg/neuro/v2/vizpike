CREATE OR REPLACE VIEW derived."Megazord" AS
SELECT
    experiment.subject AS experiment_subject,
    site.area AS site_area,
    site.hemisphere AS site_hemisphere,
    site.depth_in_um AS site_depth_in_um,
    spot.cytox_band AS spot_cytox_band,
    spot.channel AS spot_channel,
    (
        CASE exp_site.channel_mapping
        WHEN 'ROW_MAJOR' THEN MOD(spot.channel, mea.columns)
        WHEN 'COL_MAJOR' THEN DIV(spot.channel, mea.rows)
        END
    ) AS spot_x,
    (
        CASE exp_site.channel_mapping
        WHEN 'ROW_MAJOR' THEN DIV(spot.channel, mea.columns)
        WHEN 'COL_MAJOR' THEN MOD(spot.channel, mea.rows)
        END
    ) AS spot_y,

    mea.rows AS mea_rows,
    mea.columns AS mea_columns,

    source.type AS source_type,
    detection.type AS detection_type,
    sorting.type AS sorting_type,
    property.type AS property_type,

    experiment.title AS experiment_title,
    experiment.channels AS experiment_channels,
    experiment.date_time_in_iso8601 AS experiment_date_time_in_iso8601,
    experiment.timestamp_frequency_in_hz AS experiment_timestamp_frequency_in_hz,
    experiment.max_timestamp AS experiment_max_timestamp,

    source.prefix AS source_prefix,
    source.sampling_in_hz AS source_sampling_in_hz,

    detection.waveform_samples AS detection_waveform_samples,
    detection.samples_before_alignment AS detection_samples_before_alignment,
    detection.waveform_alignment AS detection_waveform_alignment,
    detection.max_overlapping_samples AS detection_max_overlapping_samples,

    property.background_estimator AS property_background_estimator,
    property.kernel_window_in_ms AS property_kernel_window_in_ms,
    property.max_latency_in_ms AS property_max_latency_in_ms,
    property.latency_precision_in_ms AS property_latency_precision_in_ms,

    unit.type AS unit_type,
    CASE unit.type
        -- The functional property algorithms that process sorting information in HDF5 and create
        -- the final multi/single-units in the DB may skip a few single-units (for example, due to
        -- failure in finding a receptive field for a subset of spikes), therefore single-unit
        -- labels from the same multi-unit must be corrected.
        WHEN 'SINGLE_UNIT' THEN RANK() OVER (
            PARTITION BY property.property_id, spot.spot_id, unit.type
            ORDER BY unit.label
        )
    END::SMALLINT AS unit_label,

    unit.basal_rate_in_hertz AS unit_basal_rate_in_hertz,
    unit.waveform_deviation_in_mv AS unit_waveform_deviation_in_mv,
    unit.waveform_mean_in_mv AS unit_waveform_mean_in_mv,
    unit.active_direction_cv AS unit_active_direction_cv,
    unit.active_direction_index AS unit_active_direction_index,
    unit.active_orientation_cv AS unit_active_orientation_cv,
    unit.active_orientation_index AS unit_active_orientation_index,
    unit.active_preferred_direction_in_degree AS unit_active_preferred_direction_in_degree,
    unit.active_preferred_orientation_in_degree AS unit_active_preferred_orientation_in_degree,
    unit.direction_cv AS unit_direction_cv,
    unit.direction_index AS unit_direction_index,
    unit.orientation_cv AS unit_orientation_cv,
    unit.orientation_index AS unit_orientation_index,
    unit.preferred_direction_in_degree AS unit_preferred_direction_in_degree,
    unit.preferred_orientation_in_degree AS unit_preferred_orientation_in_degree,
    unit.latency_in_ms AS unit_latency_in_ms,
    unit.polargram AS unit_polargram,
    unit.rf_area_in_squared_degree AS unit_rf_area_in_squared_degree,
    unit.rf_aspect_ratio AS unit_rf_aspect_ratio,
    unit.rf_center_in_degree AS unit_rf_center_in_degree,
    unit.rf_contour_in_degree AS unit_rf_contour_in_degree,
    unit.rf_convex_area_in_squared_degree AS unit_rf_convex_area_in_squared_degree,
    unit.rf_eccentricity_in_degree AS unit_rf_eccentricity_in_degree,
    unit.rf_equivalent_diameter_in_degree AS unit_rf_equivalent_diameter_in_degree,
    unit.rf_euler_number AS unit_rf_euler_number,
    unit.rf_major_axis_in_degree AS unit_rf_major_axis_in_degree,
    unit.rf_minor_axis_in_degree AS unit_rf_minor_axis_in_degree,
    unit.rf_peak_response AS unit_rf_peak_response,
    unit.rf_perimeter_in_degree AS unit_rf_perimeter_in_degree,
    unit.rf_polar_contour_in_degree AS unit_rf_polar_contour_in_degree,
    unit.rf_total_response AS unit_rf_total_response,
    unit.rf_response_cutoff,

    unit.unit_id,
    property.property_id,
    sorting.new_sorting_id,
    detection.detection_id,
    source.source_id,
    experiment.experiment_id,
    spot.spot_id,
    site.site_id,
    mea.mea_id

FROM derived."Unit" unit
    INNER JOIN derived."Property" property ON unit.property_id = property.property_id
    INNER JOIN derived."Sorting" sorting ON property.new_sorting_id = sorting.new_sorting_id
    INNER JOIN derived."Detection" detection ON sorting.detection_id = detection.detection_id
    INNER JOIN original."Source" source ON detection.source_id = source.source_id
    INNER JOIN original."Experiment" experiment ON source.experiment_id = experiment.experiment_id
    INNER JOIN original."ExperimentSite" exp_site ON exp_site.experiment_id = experiment.experiment_id
    INNER JOIN original."Site" site ON site.site_id = exp_site.site_id
    INNER JOIN original."Spot" spot ON unit.spot_id = spot.spot_id AND spot.site_id = site.site_id
    INNER JOIN original."MEA" mea ON mea.mea_id = exp_site.mea_id

ORDER BY
    experiment_subject,
    site_area,
    site_hemisphere,
    site_depth_in_um,
    spot_cytox_band,
    spot_channel,
    source_type,
    detection_type,
    sorting_type,
    property_type,
    unit_type,
    unit_label
;

CREATE OR REPLACE VIEW derived."UnitFeatures" AS
SELECT
    unit_id,
	basal_rate_in_hertz,
	total_spikes,
-- 	waveform_deviation_in_mv double precision[],
-- 	waveform_mean_in_mv double precision[],
	active_direction_cv,
	active_direction_index,
	active_orientation_cv,
	active_orientation_index,
	active_preferred_direction_in_degree,
	active_preferred_orientation_in_degree,
	direction_cv,
	direction_index,
	orientation_cv,
	orientation_index,
	preferred_direction_in_degree,
	preferred_orientation_in_degree,
	latency_in_ms,
-- 	polargram double precision[],
	rf_area_in_squared_degree,
	rf_aspect_ratio,
-- 	rf_center_in_degree double precision[],
-- 	rf_contour_in_degree double precision[],
	rf_convex_area_in_squared_degree,
	rf_eccentricity_in_degree,
	rf_equivalent_diameter_in_degree,
	rf_euler_number,
	rf_major_axis_in_degree,
	rf_minor_axis_in_degree,
	rf_peak_response,
	rf_perimeter_in_degree,
-- 	rf_polar_contour_in_degree double precision[],
	rf_total_response,
	rf_response_cutoff
FROM derived."Unit" unit;

CREATE OR REPLACE VIEW derived."SameSiteExperimentPairings" AS
SELECT
    MB.site_hemisphere,
    MB.site_depth_in_um,
    COUNT(DISTINCT MB.property_id) * COUNT(DISTINCT GR.property_id) AS possible_property_pairings,
    COUNT(DISTINCT MB.experiment_id) * COUNT(DISTINCT GR.experiment_id) AS possible_experiment_pairings,
    ARRAY_AGG(DISTINCT MB.experiment_id) AS moving_bars_experiment_ids,
    ARRAY_AGG(DISTINCT GR.experiment_id) AS gratings_experiment_ids,
    ARRAY_AGG(DISTINCT MB.property_id) AS moving_bars_property_ids,
    ARRAY_AGG(DISTINCT GR.property_id) AS gratings_property_ids,
    COUNT(DISTINCT MB.unit_id) AS moving_bars_units,
    COUNT(DISTINCT GR.unit_id) AS gratings_units,
    MB.site_id
FROM
    derived."Megazord" MB,
    derived."Megazord" GR
WHERE
    MB.experiment_title LIKE 'rf%%'
    AND GR.experiment_title LIKE 'grating%%'
    AND MB.site_id = GR.site_id
    AND MB.spot_channel = GR.spot_channel
    AND MB.mea_id = GR.mea_id
GROUP BY
    MB.site_id,
    MB.site_hemisphere,
    MB.site_depth_in_um
ORDER BY
    MB.site_depth_in_um
;
