from functools import lru_cache

from frozendict import frozendict
from pydantic import BaseSettings
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm.session import Session as DBSession


class Config(BaseSettings):
    class Config:
        frozen = True

    postgres_user: str
    postgres_password: str
    postgres_host: str
    postgres_port: int
    postgres_db: str

    fp_redis_host: str
    fp_redis_port: int
    fp_expiry_in_sec: int

    @property
    @lru_cache()
    def database_url(self) -> str:
        user = self.postgres_user
        pwd = self.postgres_password
        host = self.postgres_host
        port = self.postgres_port
        db = self.postgres_db
        return f"postgresql://{user}:{pwd}@{host}:{port}/{db}"


config = Config()


def get_engine(
    *,
    user: str = config.postgres_user,
    password: str = config.postgres_password,
    host: str = config.postgres_host,
    port: int = config.postgres_port,
    db: str = config.postgres_db,
    **engine_kwargs,
) -> Engine:
    return _engine_factory(user, password, host, port, db, frozendict(engine_kwargs))


def get_session(db_engine: Engine = None, **session_kwargs) -> DBSession:
    session = DBSession(bind=(db_engine or get_engine()), **session_kwargs)
    return session


@lru_cache()
def _engine_factory(user, password, host, port, db, engine_kwargs) -> Engine:
    db_string = f"postgresql://{user}:{password}@{host}:{port}/{db}"
    db_engine = create_engine(db_string, **(engine_kwargs or {}))
    return db_engine
