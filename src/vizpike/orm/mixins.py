import json
from typing import Generic, Type, TypeVar

import caseconverter as cc
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declared_attr


class PrimaryKey:
    @declared_attr
    def id(cls):
        return Column(f"{cls.__name__.lower()}_id", Integer, primary_key=True)


class PolymorphicBase:
    type = Column(String, nullable=False)

    @declared_attr
    def __mapper_args__(cls):
        return {
            "polymorphic_identity": cls.__name__.upper(),
            "polymorphic_on": cls.type,
        }


class PolymorphicBaseWithPrimaryKey(PrimaryKey, PolymorphicBase):
    pass


class HDF:
    hdf_path = Column(String, nullable=False)
    hdf_group = Column(String, nullable=False)
    hdf_prefix = Column(String, nullable=False)


def subtype(
    base_cls: Type[PolymorphicBaseWithPrimaryKey], *, identity: str = None
) -> type:
    class Mixin(base_cls):
        __abstract__ = True

        @declared_attr
        def id(cls):
            return Column(
                base_cls.id.name,
                Integer,
                ForeignKey(base_cls.id, onupdate="CASCADE", ondelete="CASCADE"),
                primary_key=True,
            )

        @declared_attr
        def __mapper_args__(cls):
            return {
                "polymorphic_identity": identity or cc.snakecase(cls.__name__).upper()
            }

    return Mixin


T = TypeVar("T")


class Details(Generic[T]):
    """Mixin for entity types with a JSON-encoded details column.

    Adds a `_details_json` string column and requires subclasses to implement the
    `details_type` method, which should return the type object (Pydantic-compatible)
    for the details. The details object may be retrieved (it is JSON-deserialized) via
    the `details` property, which also behaves as a setter (it raises a `ValueError` if
    an incompatible-type object is provided).
    """

    # TODO: Using abc.ABC or abc.ABCMeta causes conflicts with SQLAlchemy
    # @abc.abstractmethod
    # def details_type(self) -> Type[T]:
    #     pass

    @declared_attr
    def _details_json(cls):
        return Column(String, nullable=False, default="null")

    @property
    def details(self) -> T:
        details = self.details_type()(**json.loads(self._details_json))
        return details

    @details.setter
    def details(self, details: T):
        details_type = details.__class__
        details_json = details.json()

        if not issubclass(details_type, self.details_type()):
            raise ValueError(
                f"Trying to set object's {self} details with an object of type "
                f"{details_type}, but this object requires details of type "
                f"{self.details_type()}."
            )

        self._details_json = details_json
