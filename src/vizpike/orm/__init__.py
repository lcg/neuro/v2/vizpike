from typing import Any, Dict, Tuple

from sqlalchemy import Column, ForeignKey
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base, declared_attr


class _DeclarativeMeta(DeclarativeMeta):
    def __new__(cls, name: str, bases: Tuple[type, ...], attrs: Dict[str, Any]):
        columns_map = {
            key: val for key, val in attrs.items() if isinstance(val, Column)
        }

        def __eq__(self, other):
            comparisons = [
                getattr(self, col_name) == getattr(other, col_name)
                for col_name, col in columns_map.items()
                if col.primary_key is False
            ]
            return all(comparisons)

        def __repr__(self):
            attributes = [f"{col}={getattr(self, col)!r}" for col in columns_map]
            string = f"{self.__class__.__name__}({', '.join(attributes)})"
            return string

        attrs.update({"__eq__": __eq__, "__repr__": __repr__})
        new_type = super().__new__(cls, name, bases, attrs)

        return new_type


class _Base(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__


Base = declarative_base(cls=_Base, metaclass=_DeclarativeMeta)


def StrictForeignKey(*args, **kwargs) -> ForeignKey:
    kwargs.update({"onupdate": "CASCADE", "ondelete": "RESTRICT"})
    return ForeignKey(*args, **kwargs)


def StrictForeignKeyColumn(col: Column, **kwargs) -> Column:
    return Column(col.type, StrictForeignKey(col), **kwargs)
