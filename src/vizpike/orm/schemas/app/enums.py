from enum import Enum

from .json import SortingSelectionTaskDetails, SortingSelectionUserTaskDetails


class TaskState(Enum):
    ASSIGNED = "ASSIGNED"
    STARTED = "STARTED"
    FINISHED = "FINISHED"


class TaskType(Enum):
    PAIRED_MB_GRATINGS_SORTING_SELECTION = "TASK1"

    @property
    def task_details_type(self) -> type:
        return _task_details_types[self]

    @property
    def user_task_details_type(self) -> type:
        return _user_task_details_types[self]


_task_details_types = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: SortingSelectionTaskDetails,
}
_user_task_details_types = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: SortingSelectionUserTaskDetails,
}
