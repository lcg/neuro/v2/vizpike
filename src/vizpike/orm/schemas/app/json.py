import random
from datetime import datetime
from enum import Enum
from typing import Iterable, List, Optional, Tuple
from uuid import UUID

from pydantic import BaseModel, Field


class UnitDetails(BaseModel):
    multi_unit_id: int
    single_unit_ids: List[int]


class SortingSelectionTaskDetails(BaseModel):
    class TaskItem(BaseModel):
        channel: int
        gratings_units: List[UnitDetails]
        moving_bars_units: List[UnitDetails]

        @property
        def gratings_ids(self) -> List[int]:
            return [unit.multi_unit_id for unit in self.gratings_units]

        @property
        def moving_bars_ids(self) -> List[int]:
            return [unit.multi_unit_id for unit in self.moving_bars_units]

    moving_bars_experiment_id: int
    gratings_experiment_id: int
    items: List[TaskItem]


class SortingSelectionUserTaskDetails(SortingSelectionTaskDetails):
    class TaskItem(SortingSelectionTaskDetails.TaskItem):
        # TODO: This is a draft
        approved_moving_bars_unit_ids: List[int] = Field(default_factory=list)
        approved_gratings_unit_ids: List[int] = Field(default_factory=list)
        rejected_moving_bars_unit_ids: List[int] = Field(default_factory=list)
        rejected_gratings_unit_ids: List[int] = Field(default_factory=list)
        best_moving_bars_unit_id: Optional[int]
        best_gratings_unit_id: Optional[int]

        @property
        def is_finished(self):
            approved_ids = {
                *self.approved_gratings_unit_ids,
                *self.approved_moving_bars_unit_ids,
            }
            rejected_ids = {
                *self.rejected_gratings_unit_ids,
                *self.rejected_moving_bars_unit_ids,
            }
            all_ids = {*self.gratings_ids, *self.moving_bars_ids}
            return (approved_ids | rejected_ids) == all_ids

    items: List[TaskItem]

    @classmethod
    def from_task_details(cls, task_details: SortingSelectionTaskDetails):
        items = task_details.items[:]
        for i, item in enumerate(items):
            items[i] = cls.TaskItem(**dict(item))
        random.shuffle(items)

        kwargs = dict(task_details)
        kwargs["items"] = items

        return cls(**kwargs)

    @property
    def finished_items(self) -> Iterable[Tuple[int, TaskItem]]:
        return ((i, item) for i, item in enumerate(self.items) if item.is_finished)

    @property
    def unfinished_items(self) -> Iterable[Tuple[int, TaskItem]]:
        return ((i, item) for i, item in enumerate(self.items) if not item.is_finished)


class ActionType(Enum):
    USER_APPROVE_ALL_MULTI_UNITS = "USER_APPROVE_ALL_MULTI_UNITS"
    USER_APPROVE_MULTI_UNIT = "USER_APPROVE_MULTI_UNIT"
    USER_DETAIL_MULTI_UNIT = "USER_DETAIL_MULTI_UNIT"
    USER_HOVER_MULTI_UNIT = "USER_HOVER_MULTI_UNIT"
    USER_REJECT_ALL_MULTI_UNITS = "USER_REJECT_ALL_MULTI_UNITS"
    USER_REJECT_MULTI_UNIT = "USER_REJECT_MULTI_UNIT"


class HoverTarget(Enum):
    MEDIUM_KALEIDOMAP = "MEDIUM_KALEIDOMAP"
    MEDIUM_POLARGRAM = "MEDIUM_POLARGRAM"
    MEDIUM_RESPONSE_MAP = "MEDIUM_RESPONSE_MAP"
    MEDIUM_WAVEFORMS = "MEDIUM_WAVEFORMS"
    SMALL_KALEIDOMAP = "SMALL_KALEIDOMAP"
    SMALL_POLARGRAM = "SMALL_POLARGRAM"
    SMALL_RESPONSE_MAP = "SMALL_RESPONSE_MAP"
    SMALL_WAVEFORMS = "SMALL_WAVEFORMS"


class Action(BaseModel):
    type: ActionType
    timestamp: datetime
    multi_unit_id: Optional[int]
    hover_target: Optional[HoverTarget]

    @classmethod
    def approve_one(cls, timestamp: datetime, multi_unit_id: int):
        return Action(
            type=ActionType.USER_APPROVE_MULTI_UNIT,
            timestamp=timestamp,
            multi_unit_id=multi_unit_id,
        )

    @classmethod
    def approve_all(cls, timestamp: datetime):
        return Action(type=ActionType.USER_APPROVE_ALL_MULTI_UNITS, timestamp=timestamp)

    @classmethod
    def detail(cls, timestamp: datetime, multi_unit_id: int):
        return Action(
            type=ActionType.USER_DETAIL_MULTI_UNIT,
            timestamp=timestamp,
            multi_unit_id=multi_unit_id,
        )

    @classmethod
    def hover(cls, timestamp: datetime, multi_unit_id: int, target: HoverTarget):
        return Action(
            type=ActionType.USER_HOVER_MULTI_UNIT,
            timestamp=timestamp,
            multi_unit_id=multi_unit_id,
            target=target,
        )

    @classmethod
    def reject_one(cls, timestamp: datetime, multi_unit_id: int):
        return Action(
            type=ActionType.USER_REJECT_MULTI_UNIT,
            timestamp=timestamp,
            multi_unit_id=multi_unit_id,
        )

    @classmethod
    def reject_all(cls, timestamp: datetime):
        return Action(type=ActionType.USER_REJECT_ALL_MULTI_UNITS, timestamp=timestamp)


class ActionSequence(BaseModel):
    start_timestamp: datetime
    finish_timestamp: datetime
    session_uuid: UUID
    item_index: int
    actions: List[Action]
