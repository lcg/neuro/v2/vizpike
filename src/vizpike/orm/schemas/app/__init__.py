from datetime import datetime
from typing import TypeVar
from uuid import uuid4

from sqlalchemy import ARRAY, Column, DateTime, Enum, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import declared_attr, relationship

from vizpike.orm import Base as _Base
from vizpike.orm import StrictForeignKey, StrictForeignKeyColumn, mixins

from .enums import TaskState, TaskType

SCHEMA = "app"


class Base(_Base):
    __abstract__ = True

    @declared_attr
    def __table_args__(cls):
        return {"schema": SCHEMA}


T = TypeVar("T")


class Task(mixins.PrimaryKey, mixins.Details, Base):
    type = Column(Enum(TaskType), nullable=False)
    title = Column(String, unique=True, nullable=False)
    description_in_markdown = Column(String)
    endpoint = Column(String, nullable=False)
    length = Column(Integer, nullable=False)

    def details_type(self):
        return TaskType(self.type).task_details_type


class User(mixins.PrimaryKey, Base):
    email = Column(String, unique=True, index=True, nullable=False)
    name = Column(String)
    hashed_password = Column(String, nullable=False)


class Session(mixins.PrimaryKey, Base):
    user_id = StrictForeignKeyColumn(User.id, nullable=False)
    uuid = Column(UUID(as_uuid=True), index=True, default=uuid4, unique=True)

    login_at = Column(DateTime, default=datetime.now, nullable=False)
    logout_at = Column(DateTime)
    refreshed_at = Column(DateTime)

    max_duration_in_seconds = Column(Integer, nullable=False)

    user = relationship(User)


class UserTask(mixins.Details, Base):
    user_id = Column(
        User.id.type, StrictForeignKey(User.id), primary_key=True, nullable=False
    )
    task_id = Column(
        Task.id.type, StrictForeignKey(Task.id), primary_key=True, nullable=False
    )

    task_state = Column(Enum(TaskState), default=TaskState.ASSIGNED, nullable=False)
    action_sequences = Column(ARRAY(String))

    user = relationship(User, backref="tasks")
    task = relationship(Task, backref="users")

    def details_type(self):
        return TaskType(self.task.type).user_task_details_type
