from typing import Tuple

from colour import Color
from sqlalchemy import (
    ARRAY,
    BigInteger,
    Boolean,
    Column,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    Integer,
    String,
)
from sqlalchemy.orm import column_property, declared_attr, relationship

from vizpike import Quantity, ureg
from vizpike.orm import Base, StrictForeignKey, mixins
from vizpike.pipeline.detection import WaveformAlignment

from .enums import (
    BrainHemisphere,
    BrainRegion,
    CytoxBand,
    MatrixMapping,
    StimulusFamily,
)

SCHEMA = "original"


class OriginalBase(Base):
    __abstract__ = True

    @declared_attr
    def __table_args__(cls):
        return {"schema": SCHEMA}


class Protocol(mixins.PrimaryKey, OriginalBase):
    name = Column(String(), nullable=False)
    type = Column(Enum(StimulusFamily), nullable=False)
    trials_per_condition = Column(Integer, nullable=False)
    trial_time_in_ms = Column("condition_time_in_ms", Float, nullable=False)
    post_trial_time_in_ms = Column(Float, nullable=False)
    pre_trial_time_in_ms = Column(Float, nullable=False)
    amplitude_in_deg = Column(Float, nullable=False)
    comment = Column(String)

    non_trial_time_in_ms = column_property(pre_trial_time_in_ms + post_trial_time_in_ms)
    total_time_in_ms = column_property(non_trial_time_in_ms + trial_time_in_ms)

    @property
    def amplitude(self) -> Quantity:
        return self.amplitude_in_deg * ureg.degree

    @property
    def non_trial_time(self) -> Quantity:
        return self.non_trial_time_in_ms * ureg.ms

    @property
    def post_trial_time(self) -> Quantity:
        return self.post_trial_time_in_ms * ureg.ms

    @property
    def pre_trial_time(self) -> Quantity:
        return self.pre_trial_time_in_ms * ureg.ms

    @property
    def total_time(self) -> Quantity:
        return self.total_time_in_ms * ureg.ms

    @property
    def trial_time(self) -> Quantity:
        return self.trial_time_in_ms * ureg.ms


class Stimulus(mixins.PrimaryKey, mixins.PolymorphicBase, OriginalBase):
    protocol_id = Column(Integer, ForeignKey(Protocol.id), nullable=False)
    code = Column(Integer, nullable=False)
    name = Column(String())
    is_stimulus = Column(Boolean, nullable=False)
    is_neutral = Column(Boolean, nullable=False)
    groups = Column(ARRAY(String()))

    protocol = relationship(Protocol, backref="stimuli")


class GratingStimulus(mixins.subtype(Stimulus, identity="GRATINGS")):
    contrast = Column(Float)
    spatial_freq_in_invdeg = Column(Float)
    speed_in_hz = Column(Float)
    direction_in_deg = Column(Float)
    signed_direction_in_deg = Column(Float)
    orientation_in_deg = Column(Float)
    bg_color_in_hex = Column(String(7))
    fg_color_in_hex = Column(String(7))

    @property
    def spatial_freq(self) -> Quantity:
        return self.spatial_freq_in_invdeg / ureg.deg

    @property
    def speed(self) -> Quantity:
        return self.speed_in_hz * ureg.hertz

    @property
    def direction(self) -> Quantity:
        return self.direction_in_deg * ureg.deg

    @property
    def signed_direction(self) -> Quantity:
        return self.signed_direction_in_deg * ureg.deg

    @property
    def orientation(self) -> Quantity:
        return self.orientation_in_deg * ureg.deg

    @property
    def bg_color(self) -> Color:
        return Color(self.bg_color_in_hex)

    @property
    def fg_color(self) -> Color:
        return Color(self.fg_color_in_hex)


class MovingBarsStimulus(mixins.subtype(Stimulus, identity="MOVING_BARS")):
    direction_in_deg = Column(Float)
    signed_direction_in_deg = Column(Float)
    orientation_in_deg = Column(Float)
    contrast = Column(Float)
    bar_length_in_deg = Column(Float)
    bar_width_in_deg = Column(Float)
    bar_speed_in_deg_per_sec = Column(Float)
    bg_color_in_hex = Column(String(7))
    fg_color_in_hex = Column(String(7))

    @property
    def direction(self) -> Quantity:
        return self.direction_in_deg * ureg.deg

    @property
    def signed_direction(self) -> Quantity:
        return self.signed_direction_in_deg * ureg.deg

    @property
    def orientation(self) -> Quantity:
        return self.orientation_in_deg * ureg.deg

    @property
    def bar_length(self) -> Quantity:
        return self.bar_length_in_deg * ureg.deg

    @property
    def bar_width(self) -> Quantity:
        return self.bar_width_in_deg * ureg.deg

    @property
    def bar_speed(self) -> Quantity:
        return self.bar_speed_in_deg_per_sec * ureg.deg / ureg.second


class MEA(mixins.PrimaryKey, OriginalBase):
    rows = Column(Integer, nullable=False)
    columns = Column(Integer, nullable=False)


class PLX(mixins.PrimaryKey, OriginalBase):
    size_in_bytes = Column(BigInteger, nullable=False)
    version = Column(Integer, nullable=False)
    md5 = Column(String(32), nullable=False)
    corrupted = Column(Boolean, nullable=False)
    path = Column(String, nullable=False)


class Experiment(mixins.PrimaryKey, OriginalBase):
    plx_id = Column(Integer, ForeignKey(PLX.id), nullable=False)
    protocol_id = Column(Integer, ForeignKey(Protocol.id), nullable=False)
    subject = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    channels = Column(Integer, nullable=False)
    date_time = Column("date_time_in_iso8601", DateTime, nullable=False)
    timestamp_frequency_in_hz = Column(Integer, nullable=False)
    max_timestamp = Column(Integer, nullable=False)
    hdf_path = Column(String(), nullable=False)
    metadata_hdf_path = Column(String(), nullable=False)

    plx = relationship(PLX, backref="experiments")
    protocol = relationship(Protocol, backref="experiments")

    @property
    def timestamp_frequency(self) -> Quantity:
        return self.timestamp_frequency_in_hz * ureg.hertz


class Site(mixins.PrimaryKey, OriginalBase):
    area = Column(Enum(BrainRegion), nullable=False)
    hemisphere = Column(Enum(BrainHemisphere), nullable=False)
    depth_in_um = Column(Float, nullable=False)

    @property
    def depth(self) -> Quantity:
        return self.depth_in_um * ureg.um


class Source(mixins.PrimaryKey, mixins.PolymorphicBase, OriginalBase):
    experiment_id = Column(Integer, StrictForeignKey(Experiment.id), nullable=False)
    prefix = Column(String(), nullable=False)
    sampling_in_hz = Column(Integer, nullable=False)
    hdf_path = Column(String, nullable=False)

    experiment = relationship(Experiment, backref="sources")

    @property
    def sampling(self) -> Quantity:
        return self.sampling_in_hz * ureg.hertz


Source.dsp_source_id = Column(Integer, StrictForeignKey(Source.id))
Source.dsp_source = relationship(
    "Source", remote_side=[Source.id], backref="dsp_derivatives"
)


class SpikeSource(mixins.subtype(Source, identity="SPIKE")):
    waveform_samples = Column(Integer, nullable=False)
    samples_before_threshold = Column(Integer, nullable=False)
    waveform_alignment = Column(Enum(WaveformAlignment), nullable=False)
    waveform_sampling_in_hz = Column(Float, nullable=False)

    @property
    def waveform_sampling(self) -> Quantity:
        return self.waveform_sampling_in_hz * ureg.hertz


class FieldPotentialSource(mixins.subtype(Source, identity="FIELD_POTENTIAL")):
    pass


class SpikeContinuousSource(mixins.subtype(Source, identity="SPIKE_CONTINUOUS")):
    pass


class WidebandSource(mixins.subtype(Source, identity="WIDEBAND")):
    pass


class Spot(mixins.PrimaryKey, OriginalBase):
    experiment_id = Column(Integer, StrictForeignKey(Experiment.id), nullable=False)
    site_id = Column(Integer, StrictForeignKey(Site.id), nullable=False)
    channel = Column(Integer, nullable=False)
    cytox_band = Column(Enum(CytoxBand), nullable=False)

    experiment = relationship(Experiment, backref="spots")
    site = relationship(Site, backref="spots")

    @property
    def mea_xy(self) -> Tuple[int, int]:
        site_mea_recording = [
            smr
            for smr in self.site.experiments
            if smr.experiment_id == self.experiment_id
        ][0]
        channel_mapping = site_mea_recording.channel_mapping
        channel_0_based = self.channel - 1
        mea = site_mea_recording.mea

        if channel_mapping ==  MatrixMapping.ROW_MAJOR:
            x = channel_0_based % mea.columns
            y = channel_0_based // mea.columns
        elif channel_mapping == MatrixMapping.COL_MAJOR:
            x = channel_0_based // mea.rows
            y = channel_0_based % mea.rows
        else:
            raise ValueError(f"Unknown matrix mapping: {channel_mapping!r}")
            
        return x, y


class Trial(mixins.PrimaryKey, OriginalBase):
    experiment_id = Column(Integer, StrictForeignKey(Experiment.id), nullable=False)
    stimulus_id = Column(Integer, StrictForeignKey(Stimulus.id), nullable=False)
    start_in_ts = Column(Integer, nullable=False)
    stop_in_ts = Column(Integer, nullable=False)
    duration_in_ts = Column(Integer, nullable=False)

    experiment = relationship(Experiment, backref="trials")
    stimulus = relationship(Stimulus, backref="trials")


class Sorting(mixins.PrimaryKey, OriginalBase):
    spike_source_id = Column(Integer, StrictForeignKey(SpikeSource.id))
    plx_id = Column(Integer, StrictForeignKey(PLX.id), nullable=False)
    sorted_channels = Column(Integer, nullable=False)
    max_units = Column(Integer, nullable=False)
    author = Column(String())
    hdf_path = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    comment = Column(String())

    spike_source = relationship(SpikeSource, backref="sortings")
    plx = relationship(PLX, backref="sortings")


class ExperimentSite(OriginalBase):
    experiment_id = Column(Integer, StrictForeignKey(Experiment.id), primary_key=True)
    site_id = Column(Integer, StrictForeignKey(Site.id), primary_key=True)
    mea_id = Column(Integer, StrictForeignKey(MEA.id))
    base_channel = Column(Integer, nullable=False)
    channel_mapping = Column(Enum(MatrixMapping))

    experiment = relationship(Experiment, backref="sites")
    site = relationship(Site, backref="experiments")
    mea = relationship(MEA)


# Experiment.sites = relationship(Site, secondary=ExperimentSite.__table__, backref="experiments")
# Experiment.meas = relationship(MEA, secondary=ExperimentSite.__table__, backref="experiments")
