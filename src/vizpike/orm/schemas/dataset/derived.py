import pickle
from functools import wraps
from io import BytesIO
from typing import Any, Callable, List, TypeVar, Union

import h5py as h5
import numpy as np
import pandas as pd
import redis
from nptyping import Float64, NDArray
from sqlalchemy import ARRAY, Column, Enum, Float, Integer, SmallInteger
from sqlalchemy.orm import column_property, declared_attr, relationship
from sqlalchemy.orm.session import object_session

from vizpike import Quantity, ureg
from vizpike.db import config
from vizpike.orm import Base, StrictForeignKey, mixins
from vizpike.orm.schemas.dataset import enums
from vizpike.pipeline.dataset import DATASET_ROOT
from vizpike.pipeline.detection import (
    DspFilteringMode,
    DspFilterType,
    WaveformAlignment,
)
from vizpike.pipeline.properties.moving_bars import BackProjector
from vizpike.pipeline.sigproc import BackgroundActivityEstimator

from . import original

SCHEMA = "derived"


class DerivedBase(Base):
    __abstract__ = True

    @declared_attr
    def __table_args__(cls):
        return {"schema": SCHEMA}


class Detection(mixins.PrimaryKey, mixins.HDF, mixins.PolymorphicBase, DerivedBase):
    source_id = Column(Integer, StrictForeignKey(original.Source.id), nullable=False)
    waveform_samples = Column(Integer, nullable=False)
    samples_before_alignment = Column(Integer, nullable=False)
    waveform_alignment = Column(Enum(WaveformAlignment), nullable=False)
    max_overlapping_samples = Column(Integer, nullable=False)
    samples_after_alignment = column_property(
        waveform_samples - samples_before_alignment
    )

    source = relationship(original.Source, backref="detections")


class OmniplexDetection(mixins.subtype(Detection, identity="OMNIPLEX")):
    waveform_sampling_in_hertz = Column(
        "waveform_sampling_in_hz", Float, nullable=False
    )

    @property
    def waveform_sampling(self) -> Quantity:
        return self.waveform_sampling_in_hertz * ureg.hertz


class ReverseEngineeredDetection:
    sampling_in_hertz = Column("sampling_in_hz", Float, nullable=False)
    threshold_in_stdev = Column(Float, nullable=False)

    @property
    def sampling(self) -> Quantity:
        return self.sampling_in_hertz * ureg.hertz


class SpikeContinuousDetection(
    ReverseEngineeredDetection, mixins.subtype(Detection, identity="OMNIPLEX_RE_SPKC"),
):
    pass


class WidebandDetection(
    ReverseEngineeredDetection, mixins.subtype(Detection, identity="OMNIPLEX_RE_WB"),
):
    dsp_filter_type = Column(Enum(DspFilterType), nullable=False)
    dsp_filter_highpass_cutoff_in_hertz = Column(
        "dsp_filter_highpass_cutoff_in_hz", Float, nullable=False
    )
    dsp_filter_order = Column(SmallInteger, nullable=False)
    dsp_filtering_mode = Column(Enum(DspFilteringMode), nullable=False)

    @property
    def dsp_filter_highpass_cutoff(self) -> Quantity:
        return self.dsp_filter_highpass_cutoff_in_hertz * ureg.hertz


class Sorting(mixins.HDF, mixins.PolymorphicBase, DerivedBase):
    id = Column("new_sorting_id", Integer, primary_key=True)
    detection_id = Column(Integer, StrictForeignKey(Detection.id), nullable=False)

    detection = relationship(Detection, backref="sortings")


class OmniplexSorting(mixins.subtype(Sorting, identity="OMNIPLEX")):
    legacy_sorting_id = Column(
        "sorting_id", Integer, StrictForeignKey(original.Sorting.id), nullable=False,
    )

    legacy_sorting = relationship(original.Sorting)


class PCAKmeansSorting(mixins.subtype(Sorting, identity="OMNIPLEX_RE")):
    clustering = Column(Enum(enums.WaveformClusteringMethod), nullable=False)
    max_clusters = Column(SmallInteger, nullable=False)
    upsampling_in_doubles = Column(SmallInteger, nullable=False)
    projection = Column(Enum(enums.WaveformProjectionMethod), nullable=False)
    projection_dims = Column(SmallInteger, nullable=False)
    feature_space = Column(Enum(enums.WaveformProjectionFeatures), nullable=False)


class Property(mixins.PrimaryKey, mixins.HDF, mixins.PolymorphicBase, DerivedBase):
    sorting_id = Column(
        "new_sorting_id", Integer, StrictForeignKey(Sorting.id), nullable=False
    )
    background_estimator = Column(Enum(BackgroundActivityEstimator), nullable=False)
    kernel_window_in_ms = Column(Float, nullable=False)
    max_latency_in_ms = Column(Float, nullable=False)
    latency_precision_in_ms = Column(Float, nullable=False)

    sorting = relationship(Sorting)

    @property
    def kernel_window(self) -> Quantity:
        return self.kernel_window_in_ms * ureg.ms

    @property
    def max_latency(self) -> Quantity:
        return self.max_latency_in_ms * ureg.ms

    @property
    def latency_precision(self) -> Quantity:
        return self.latency_precision_in_ms * ureg.ms


class GratingsProperty(mixins.subtype(Property, identity="GRATINGS")):
    pass


class MovingBarsProperty(mixins.subtype(Property, identity="MOVING_BARS")):
    back_projector = Column(Enum(BackProjector), nullable=False)
    resolution = Column(Integer, nullable=False)
    response_threshold = Column(Float, nullable=False)
    min_peak_disparity = Column(Float, nullable=False)


_not_exported_unit_variables = set(
    pd.read_csv(DATASET_ROOT / "variable-exports.csv")
    .query("~exported")["name"]
    .drop_duplicates()
)
_unit_cache = redis.Redis(host=config.fp_redis_host, port=config.fp_redis_port)
_unit_variables = pd.read_csv(DATASET_ROOT / "variable-exports.csv", index_col=(0, 1))
R = TypeVar("R")


def _cached_property(
    item_key: str,
    save: Callable[[R], bytes] = pickle.dumps,
    load: Callable[[bytes], R] = pickle.loads,
) -> Callable[[Callable[["Unit"], R]], Callable[["Unit"], R]]:
    def decorator(f):
        @wraps(f)
        def wrapper(unit: "Unit") -> R:
            key = unit.__attr_cache_key__(item_key)

            cached_value = _unit_cache.get(key)
            if cached_value is None:
                property_value = f(unit)
                _unit_cache.set(key, save(property_value))
            else:
                property_value = load(cached_value)

            return property_value

        return wrapper

    return decorator


def _deserialize_array(buffer: bytes) -> np.ndarray:
    array = np.load(BytesIO(buffer))
    return array


def _serialize_array(array: np.ndarray) -> bytes:
    buffer = BytesIO()
    np.save(buffer, array)
    buffer.seek(0)
    return buffer.read()


class Unit(mixins.PrimaryKey, DerivedBase):
    property_id = Column(Integer, StrictForeignKey(Property.id), nullable=False)
    spot_id = Column(Integer, StrictForeignKey(original.Spot.id), nullable=False)
    type = Column(Enum(enums.UnitType), nullable=False)
    label = Column(SmallInteger)

    # Common functional properties
    basal_rate_in_hertz = Column(Float)
    waveform_deviation_in_mv = Column(ARRAY(Float))
    waveform_mean_in_mv = Column(ARRAY(Float))
    total_spikes = Column(Integer)

    # Gratings functional properties
    active_direction_cv = Column(Float)
    active_direction_index = Column(Float)
    active_orientation_cv = Column(Float)
    active_orientation_index = Column(Float)
    active_preferred_direction_in_degree = Column(Float)
    active_preferred_orientation_in_degree = Column(Float)
    direction_cv = Column(Float)
    direction_index = Column(Float)
    orientation_cv = Column(Float)
    orientation_index = Column(Float)
    preferred_direction_in_degree = Column(Float)
    preferred_orientation_in_degree = Column(Float)

    # Moving bars functional properties
    latency_in_ms = Column(Float)
    polargram = Column(ARRAY(Float))
    rf_area_in_squared_degree = Column(Float)
    rf_aspect_ratio = Column(Float)
    rf_center_in_degree = Column(ARRAY(Float))
    rf_contour_in_degree = Column(ARRAY(Float))
    rf_convex_area_in_squared_degree = Column(Float)
    rf_eccentricity_in_degree = Column(Float)
    rf_equivalent_diameter_in_degree = Column(Float)
    rf_euler_number = Column(Integer)
    rf_major_axis_in_degree = Column(Float)
    rf_minor_axis_in_degree = Column(Float)
    rf_peak_response = Column(Float)
    rf_perimeter_in_degree = Column(Float)
    rf_polar_contour_in_degree = Column(ARRAY(Float))
    rf_total_response = Column(Float)
    rf_response_cutoff = Column(Float)

    def __getattr__(self, item):
        """Fallback for un-exported functional properties with computation formulas defined in the
        dataset/variable-exports.csv file.
        """
        if item not in _not_exported_unit_variables:
            return getattr(super(), item)

        item_key = self.__attr_cache_key__(item)

        property_value = _unit_cache.get(item_key)
        if property_value is not None:
            return pickle.loads(property_value)

        property_obj = self.property
        spot_obj = self.spot

        hdf_expression = _unit_variables.loc[
            (item, property_obj.type), "hdf_expression"
        ]

        with h5.File(
            DATASET_ROOT / "derived/data" / property_obj.hdf_path, "r"
        ) as hdf_file:
            properties_url = "/".join(
                [
                    property_obj.hdf_group,
                    f"{property_obj.hdf_prefix}{spot_obj.channel:02d}",
                    (
                        f"{self.type.value}_{chr(ord('A') + self.label - 1)}"
                        if (self.type == enums.UnitType.SINGLE_UNIT)
                        else self.type.value
                    ),
                ]
            )
            properties_group = hdf_file[properties_url]

            property_value = eval(
                str(hdf_expression),
                {"np": np, "unit": ureg.Unit, "props": properties_group},
            )
            _unit_cache.set(item_key, pickle.dumps(property_value))

            return property_value

    def __attr_cache_key__(self, item: str) -> str:
        return f"{self.__class__.__module__}.{self.__class__.__name__}:id={self.id}:item={item}"

    @property
    @_cached_property("active_firing_rate_deviations")
    def active_firing_rate_deviations(
        self,
    ) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return self.active_firing_rate_deviations_in_hertz * ureg.Unit("hertz")

    @property
    @_cached_property("active_firing_rate_functions")
    def active_firing_rate_functions(
        self,
    ) -> Union[Quantity, NDArray[(Any, Any), Float64]]:
        return self.active_firing_rate_functions_in_hertz * ureg.Unit("hertz")

    @property
    @_cached_property("active_firing_rate_means")
    def active_firing_rate_means(self,) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return self.active_firing_rate_means_in_hertz * ureg.Unit("hertz")

    @property
    def active_preferred_direction(self) -> Quantity:
        return self.active_preferred_direction_in_degree * ureg.Unit("degree")

    @property
    def active_preferred_orientation(self) -> Quantity:
        return self.active_preferred_orientation_in_degree * ureg.Unit("degree")

    @property
    def basal_rate(self) -> Quantity:
        return self.basal_rate_in_hertz * ureg.Unit("hertz")

    @property
    @_cached_property("firing_rate_deviations")
    def firing_rate_deviations(self,) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return np.array(self.firing_rate_deviations_in_hertz) * ureg.Unit("hertz")

    @property
    def firing_rate_functions(self,) -> Union[Quantity, NDArray[(Any, Any), Float64]]:
        return self.firing_rate_functions_in_hertz * ureg.Unit("hertz")

    @property
    @_cached_property("firing_rate_means")
    def firing_rate_means(self) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return np.array(self.firing_rate_means_in_hertz) * ureg.Unit("hertz")

    @property
    def latency(self) -> Quantity:
        return self.latency_in_ms * ureg.Unit("millisecond")

    @property
    def preferred_direction(self) -> Quantity:
        return self.preferred_direction_in_degree * ureg.Unit("degree")

    @property
    def preferred_orientation(self) -> Quantity:
        return self.preferred_orientation_in_degree * ureg.Unit("degree")

    @property
    @_cached_property("response_map", save=_serialize_array, load=_deserialize_array)
    def response_map(self):
        return self.__getattr__("response_map")

    @property
    def rf_area(self) -> Quantity:
        return self.rf_area_in_squared_degree * ureg.Unit("degree**2")

    @property
    def rf_center(self) -> Union[Quantity, NDArray[(2,), Float64]]:
        return np.array(self.rf_center_in_degree) * ureg.Unit("degree")

    @property
    def rf_contour(self) -> Union[Quantity, NDArray[(Any, 2), Float64]]:
        return np.array(self.rf_contour_in_degree) * ureg.Unit("degree")

    @property
    def rf_convex_area(self) -> Quantity:
        return self.rf_convex_area_in_squared_degree * ureg.Unit("degree")

    @property
    def rf_equivalent_diameter(self) -> Quantity:
        return self.rf_equivalent_diameter_in_degree * ureg.Unit("degree")

    @property
    def rf_major_axis(self) -> Quantity:
        return self.rf_major_axis_in_degree * ureg.Unit("degree")

    @property
    def rf_minor_axis(self) -> Quantity:
        return self.rf_minor_axis_in_degree * ureg.Unit("degree")

    @property
    def rf_perimeter(self) -> Quantity:
        return self.rf_perimeter_in_degree * ureg.Unit("degree")

    @property
    def rf_polar_contour(self) -> Union[Quantity, NDArray[(Any, 2), Float64]]:
        return np.array(self.rf_polar_contour_in_degree) * ureg.Unit("degree")

    @property
    def siblings(self) -> List["Unit"]:
        siblings = (
            object_session(self)
            .query(Unit)
            .filter(
                Unit.spot == self.spot,
                Unit.property == self.property,
                Unit.type != enums.UnitType.MULTI_UNIT_WITHOUT_UNSORTED,
                Unit.type != enums.UnitType.UNSORTED,
            )
            .all()
        )
        return siblings

    @property
    def waveform_deviation(self) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return self.waveform_deviation_in_mv_squared * ureg.Unit("millivolt")

    @property
    def waveform_mean(self) -> Union[Quantity, NDArray[(Any,), Float64]]:
        return self.waveform_mean_in_mv * ureg.Unit("millivolt")

    property = relationship(Property, backref="units")
    spot = relationship(original.Spot, backref="units")
