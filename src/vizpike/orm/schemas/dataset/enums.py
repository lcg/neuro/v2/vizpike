from enum import Enum


class BrainRegion(Enum):
    V2 = "V2"


class BrainHemisphere(Enum):
    LEFT = "LEFT"
    RIGHT = "RIGHT"


class CytoxBand(Enum):
    THIN = "THIN"
    THICK = "THICK"
    INTERBAND_1 = "INTERBAND_1"
    INTERBAND_2 = "INTERBAND_2"


class MatrixMapping(Enum):
    COL_MAJOR = "COL_MAJOR"
    ROW_MAJOR = "ROW_MAJOR"


class StimulusFamily(Enum):
    GRATINGS = "GRATINGS"
    MOVING_BARS = "MOVING_BARS"


class UnitType(Enum):
    MULTI_UNIT = "MULTI_UNIT"
    MULTI_UNIT_WITHOUT_UNSORTED = "MULTI_UNIT_WITHOUT_UNSORTED"
    SINGLE_UNIT = "SINGLE_UNIT"
    UNSORTED = "UNSORTED"


# TODO: None of the three classes below are currently used in the metadata derivation step. Instead, their values are hard-coded.
class WaveformClusteringMethod(Enum):
    KMEANS_SCAN = "KMEANS_SCAN"


class WaveformProjectionMethod(Enum):
    PCA = "PCA"


class WaveformProjectionFeatures(Enum):
    WAVEFORM_FEATURES = "WAVEFORM_FEATURES"
    WAVEFORM_SAMPLES = "WAVEFORM_SAMPLES"
