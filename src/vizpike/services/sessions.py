from datetime import datetime, timedelta
from typing import Optional
from uuid import UUID

import bcrypt
from sqlalchemy.exc import IntegrityError

from vizpike.orm.schemas.app import Session

from . import base
from .users import service as users

UUID_COLLISION_MAX_TRIES = 3


class SessionExpired(Exception):
    pass


class IncorrectLogin(Exception):
    pass


class InvalidSession(Exception):
    pass


class SessionCreationFailed(Exception):
    pass


class SessionService(base.CRUD[Session]):
    @base.requires_db
    def checkin(self, uuid: UUID) -> Session:
        session = self.get_by_uuid(uuid)

        if session:
            if self.is_valid(session):
                session.refreshed_at = datetime.now()
                return self.update(session)
            else:
                raise SessionExpired()
        else:
            raise InvalidSession()

    @base.requires_db
    def get_by_uuid(self, uuid: UUID) -> Optional[Session]:
        return self.get_by(Session.uuid == uuid).one_or_none()

    def is_valid(self, session: Session) -> bool:
        now = datetime.now()
        session_duration = timedelta(seconds=session.max_duration_in_seconds)

        if session.logout_at:
            return False
        else:
            return now < (session.refreshed_at or session.login_at) + session_duration

    @base.requires_db
    def login(self, *, email: str, password: str) -> Session:
        user = users(self.session).get_by_email(email)

        if user and bcrypt.checkpw(password.encode(), user.hashed_password.encode()):
            for i in range(UUID_COLLISION_MAX_TRIES):
                try:
                    session = Session(user=user, max_duration_in_seconds=48 * 60 * 60)
                    return self.create(session)
                except IntegrityError:
                    # This may happen because of duplicate UUIDs, which is an extremely
                    # unlikely event, so a couple of retries should eliminate this
                    # problem completely.
                    pass
            else:
                raise SessionCreationFailed()
        else:
            raise IncorrectLogin()

    def logout(self, uuid: UUID):
        session = self.get_by_uuid(uuid)

        if session:
            if self.is_valid(session):
                session.logout_at = datetime.now()
                self.update(session)
            else:
                raise SessionExpired()
        else:
            raise InvalidSession()


service = SessionService()
