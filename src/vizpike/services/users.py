import os
from base64 import b64encode
from typing import Optional

import bcrypt

from vizpike.orm.schemas.app import User

from . import base


class UsersCRUD(base.CRUD[User]):
    @base.requires_db
    def create(self, *, name: str, email: str, password: str) -> User:
        user = User(
            name=name, email=email, hashed_password=self.hashed_password(password)
        )
        return super().create(user)

    @base.requires_db
    def get_by_email(self, email: str) -> Optional[User]:
        return self.get_by(User.email == email).one_or_none()

    @base.requires_db
    def reset_password(self, user: User, password: str):
        # TODO: Check whether password was the same.
        user.hashed_password = self.hashed_password(password)
        return super().update(user)

    def hashed_password(self, password: str) -> str:
        return bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

    def random_password(self, length=8) -> str:
        return b64encode(os.urandom(length)).decode()


service = UsersCRUD()
