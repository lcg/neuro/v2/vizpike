from typing import Any, Callable, Iterable, Optional, Tuple
from uuid import UUID

import numpy as np

from vizpike.db import DBSession
from vizpike.orm.schemas.app import Session, Task, User, UserTask
from vizpike.orm.schemas.app.enums import TaskType
from vizpike.orm.schemas.app.json import (
    SortingSelectionTaskDetails,
    SortingSelectionUserTaskDetails,
    UnitDetails,
)

from . import base
from .experiments import service as experiments
from .units import service as units


class UnsupportedTaskType(Exception):
    def __init__(
        self, task_type: TaskType, operation: Callable[[UserTask], Any],
    ):
        super().__init__(
            f"Operation {operation} is not supported for task type {task_type}."
        )
        self.task_type = task_type
        self.operation = operation


class TasksCRUD(base.CRUD[Task]):
    @base.requires_db
    def create(self, task_type: TaskType, task_kwargs: Optional[dict] = None,) -> Task:
        task = _create_for_type[task_type](self.session, **(task_kwargs or {}))
        return super().create(task)

    def first_unfinished_item_if_any(
        self, user_task: UserTask
    ) -> Optional[Tuple[int, Any]]:
        task_type = user_task.task.type
        try:
            strategy = _first_unfinished_item_if_any_for_type[task_type]
        except KeyError:
            raise UnsupportedTaskType(task_type, self.first_unfinished_item_if_any)
        return strategy(user_task)

    @base.requires_db
    def get_by_type(self, task_type: TaskType) -> Iterable[Task]:
        return self.session.query(Task).filter(Task.type == task_type).all()

    @base.requires_db
    def get_by_session_uuid(self, session_uuid: UUID) -> Iterable[UserTask]:
        return (
            self.session.query(UserTask)
            .join(User, Session)
            .filter(Session.uuid == session_uuid)
        )

    @base.requires_db
    def get_by_session_uuid_and_task_id(
        self, session_uuid: UUID, task_id: int
    ) -> Optional[UserTask]:
        return (
            self.session.query(UserTask)
            .join(User, Session, Task)
            .filter(Session.uuid == session_uuid, Task.id == task_id)
            .one_or_none()
        )

    @base.requires_db
    def assign_to(self, task: Task, users: Iterable[User]):
        """Assign a task to given users, creating the appropriate association object
        and task execution details for each user.
        """
        for user in users:
            user_task = UserTask(user=user, task=task)
            _assign_to_for_type[task.type](task, user_task)
            self.session.add(user_task)

    @base.requires_db
    def reset_for(self, task: Task, users: Iterable[User]):
        """Assign a task to given users, creating the appropriate association object
        and task execution details for each user.
        """
        recreate_for = []
        for user in users:
            user_task = (
                self.session.query(UserTask)
                .filter(UserTask.user == user, UserTask.task == task)
                .one_or_none()
            )
            if user_task:
                self.session.delete(user_task)
                recreate_for.append(user)

        self.session.commit()
        self.assign_to(task, recreate_for)

    def completion(self, user_task: UserTask) -> Tuple[int, int]:
        """User's progress on a task, represented as the number of completed items followed
        by the total number of items in that task.
        """
        task_type = user_task.task.type
        try:
            strategy = _completion_for_type[task_type]
        except KeyError:
            raise UnsupportedTaskType(task_type, self.completion)
        return strategy(user_task)


service = TasksCRUD()


def _assign_to_for_task1(task: Task, user_task: UserTask):
    user_task.details = SortingSelectionUserTaskDetails.from_task_details(task.details)


def _create_for_task1(db: DBSession, *, mb_id: int, gr_id: int, demo: bool = False):
    task: Task[SortingSelectionTaskDetails] = Task(
        title=("Demo: " if demo else "") + "MB x Gratings sorting selection at V2",
        description_in_markdown=(
            "Select valid (i.e. feasible, useful) sortings in pairs of moving bars and"
            " gratings experiments recorded at the same site. Multiple alternative"
            " sortings for each channel will be presented. Explicitly mark alternatives"
            " as discarded (bad data, unfeasible, useless) or approved (interesting,"
            " feasible, worth analyzing any further)."
        ),
        endpoint="/task1",
        type=TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION,
    )

    mb_exp = experiments(db).get_by_id(mb_id)
    gr_exp = experiments(db).get_by_id(gr_id)
    mb_channels = mb_exp.channels
    gr_channels = gr_exp.channels

    if mb_channels != gr_channels:
        raise ValueError(
            f"Trying to create a {TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION} "
            f"task for experiments with IDs {mb_id} and {gr_id}, which have "
            f"different channel counts ({mb_channels} and {gr_channels}, "
            "respectively)."
        )

    def has_spikes(unit_id: int) -> bool:
        total_spikes = units(db).get_by_id(unit_id).total_spikes
        return (total_spikes is not None) and (total_spikes > 0)

    task_items = [
        SortingSelectionTaskDetails.TaskItem(
            channel=mb_spot.channel,
            gratings_units=[
                UnitDetails(
                    multi_unit_id=multi_unit_id,
                    single_unit_ids=[
                        single_unit.id
                        for single_unit in units(db).get_single_unit_splits_for(
                            units(db).get_by_id(multi_unit_id)
                        )
                    ],
                )
                for multi_unit_id in units(db).get_gratings_multi_unit_ids_at(
                    site_id=gr_spot.site_id, spot_channel=gr_spot.channel
                )
                if has_spikes(multi_unit_id)
            ],
            moving_bars_units=[
                UnitDetails(
                    multi_unit_id=multi_unit_id,
                    single_unit_ids=[
                        single_unit.id
                        for single_unit in units(db).get_single_unit_splits_for(
                            units(db).get_by_id(multi_unit_id)
                        )
                    ],
                )
                for multi_unit_id in units(db).get_moving_bar_multi_unit_ids_at(
                    site_id=mb_spot.site_id, spot_channel=mb_spot.channel
                )
                if has_spikes(multi_unit_id)
            ],
        )
        for gr_spot, mb_spot, _ in zip(
            gr_exp.spots, mb_exp.spots, range(4 if demo else 4096)
        )
    ]

    task.details = SortingSelectionTaskDetails(
        moving_bars_experiment_id=mb_id, gratings_experiment_id=gr_id, items=task_items
    )
    task.length = len(task_items)

    return task


def _completion_for_task1(user_task: UserTask):
    user_details = user_task.details
    task_details = user_task.task.details

    completed = len(list(user_details.finished_items))
    total = len(task_details.items)

    return completed, total


def _first_unfinished_item_if_any_for_task_1(
    user_task: UserTask,
) -> Optional[Tuple[int, SortingSelectionUserTaskDetails.TaskItem]]:
    details: SortingSelectionUserTaskDetails = user_task.details
    unfinished_items = list(details.unfinished_items)
    if unfinished_items:
        return unfinished_items[0]


_assign_to_for_type = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: _assign_to_for_task1,
}
_create_for_type = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: _create_for_task1,
}
_completion_for_type = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: _completion_for_task1,
}
_first_unfinished_item_if_any_for_type = {
    TaskType.PAIRED_MB_GRATINGS_SORTING_SELECTION: _first_unfinished_item_if_any_for_task_1
}
