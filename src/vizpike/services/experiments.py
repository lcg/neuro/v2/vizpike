from vizpike.orm.schemas.dataset.original import Experiment

from . import base


class ExperimentsCRUD(base.CRUD[Experiment]):
    pass


service = ExperimentsCRUD()
