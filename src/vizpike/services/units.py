from typing import Iterable, List, Union

from vizpike.orm.schemas.dataset.derived import Unit
from vizpike.orm.schemas.dataset.enums import UnitType
from vizpike.utils import shuffled

from . import base


class UnitService(base.CRUD[Unit]):
    def get_by_id(
        self, *unit_ids: int, shuffle: bool = False
    ) -> Union[List[Unit], Iterable[Unit]]:
        result = super().get_by_id(*unit_ids)
        if shuffle:
            return shuffled(result)
        else:
            return result

    @base.requires_db
    def get_gratings_multi_unit_ids_at(
        self, *, site_id: int, spot_channel: int
    ) -> List[int]:
        result = self.session.execute(
            f"""
            WITH DetectionDetails AS (
                (
                    SELECT detection_id, NULL AS threshold_in_stdev
                    FROM derived."OmniplexDetection"
                ) UNION ALL (
                    SELECT detection_id, threshold_in_stdev
                    FROM derived."SpikeContinuousDetection"
                ) UNION ALL (
                    SELECT detection_id, threshold_in_stdev
                    FROM derived."WidebandDetection"
                )
            ), SortingDetails AS (
                (
                    SELECT new_sorting_id, NULL AS feature_space
                    FROM derived."OmniplexSorting"
                ) UNION ALL (
                    SELECT new_sorting_id, feature_space
                    FROM derived."PCAKmeansSorting"
                )
            ) SELECT mega.unit_id
            FROM derived."Megazord" mega
                JOIN DetectionDetails det USING (detection_id)
                JOIN SortingDetails srt USING (new_sorting_id)
            WHERE mega.site_id = {site_id}
                AND mega.spot_channel = {spot_channel}
                AND mega.property_type = 'GRATINGS'
                AND mega.unit_type = 'MULTI_UNIT_WITHOUT_UNSORTED'
                AND (
                    det.threshold_in_stdev IN (3, 5)
                    OR det.threshold_in_stdev IS NULL
                ) AND (
                    srt.feature_space = 'WAVEFORM_SAMPLES'
                    OR srt.feature_space IS NULL
                );
            """
        ).fetchall()
        return [r[0] for r in result]

    @base.requires_db
    def get_moving_bar_multi_unit_ids_at(
        self, *, site_id: int, spot_channel: int
    ) -> List[int]:
        result = self.session.execute(
            f"""
            SELECT mega.unit_id
            FROM derived."Megazord" mega
            WHERE mega.site_id = {site_id}
                AND mega.spot_channel = {spot_channel}
                AND mega.property_type = 'MOVING_BARS'
                AND mega.unit_type = 'MULTI_UNIT_WITHOUT_UNSORTED';
            """,
        ).fetchall()
        return [r[0] for r in result]

    @base.requires_db
    def get_single_unit_splits_for(self, unit: Unit, include_self=False) -> List[Unit]:
        assert unit.type in {
            UnitType.MULTI_UNIT,
            UnitType.MULTI_UNIT_WITHOUT_UNSORTED,
        }, "Cannot obtain single-unit splits for something which is not a multi-unit"

        splits = (
            self.session.query(Unit)
            .filter(
                Unit.spot_id == unit.spot_id,
                Unit.property_id == unit.property_id,
                Unit.type.notin_(
                    {
                        UnitType.MULTI_UNIT,
                        UnitType.MULTI_UNIT_WITHOUT_UNSORTED,
                        UnitType.UNSORTED,
                    }
                ),
            )
            .all()
        )

        if include_self:
            splits.append(unit)

        return splits


service = UnitService()
