from functools import wraps
from typing import Any, Callable, Generic, Iterable, TypeVar, Union, get_args

from vizpike.db import DBSession

T = TypeVar("T")


def requires_db(operation: Callable[["CRUD", ...], Any]):
    @wraps(operation)
    def wrapper(self: "CRUD", *args, **kwargs):
        if self.session is None:
            raise UnboundService(operation)

        return operation(self, *args, **kwargs)

    return wrapper


class UnboundService(Exception):
    def __init__(self, operation):
        self.operation = operation
        super().__init__(
            f"Trying to execute operation {operation.__qualname__} on unbound service."
        )


class CRUD(Generic[T]):
    def __init__(self, session: DBSession = None):
        (self._cls,) = get_args(self.__class__.__orig_bases__[0])
        self.session = session

    def __call__(self, session) -> "CRUD[T]":
        return self.__class__(session)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            raise exc_val

    @requires_db
    def create(self, obj: T) -> T:
        self.session.add(obj)
        return obj

    @requires_db
    def get_all(self) -> Iterable[T]:
        return self.session.query(self._cls)

    @requires_db
    def get_by(self, *expr) -> Iterable[T]:
        return self.session.query(self._cls).filter(*expr)

    @requires_db
    def get_by_id(self, *ids: int, as_iterable=False) -> Union[T, Iterable[T]]:
        if len(ids) == 0:
            raise ValueError(
                f"At least one ID must be provided for {self.get_by_id.__qualname__}()"
            )
        elif len(ids) > 1 or as_iterable:
            return self.session.query(self._cls).filter(self._cls.id.in_(ids))
        else:
            return self.session.query(self._cls).filter(self._cls.id == ids[0]).one()

    @requires_db
    def update(self, obj: T) -> T:
        self.session.add(obj)
        return obj
