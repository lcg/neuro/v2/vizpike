from typing import Dict, List, Union

JSON = Dict[str, Union[bool, float, int, str, List["JSON"], "JSON", None]]
