from pint import UnitRegistry, set_application_registry
from pint_pandas import PintType

__version__ = "1.0.1"

PintType.ureg = ureg = UnitRegistry()
set_application_registry(ureg)

Quantity = ureg.Quantity
Unit = ureg.Unit
