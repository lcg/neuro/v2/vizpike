from pathlib import Path
from typing import Iterable, Tuple, Union

import click
import h5py as h5
import numpy as np
import pandas as pd
from tqdm import tqdm

from vizpike import db, ureg
from vizpike.orm.schemas.dataset import derived, original
from vizpike.pipeline.dataset import (
    DATASET_ROOT,
    UnitType,
    fk_join,
    load_original_entity,
)
from vizpike.pipeline.properties.moving_bars import BackProjector
from vizpike.pipeline.sigproc import BackgroundActivityEstimator
from vizpike.pipeline.utils import regex_extract


@click.group()
def main():
    pass


@main.command(name="initial")
def main_initial():
    InitialDerivation().run()


@main.command(name="unit")
def main_unit():
    engine = db.get_engine()

    print("Creating DB instances...")
    UnitDerivation(engine).run()


@main.command(name="unit-export")
def main_unit_export():
    engine = db.get_engine()

    print("Loading dataframes...")
    with engine.connect() as connection:
        units = pd.read_sql_table(
            derived.Unit.__tablename__,
            connection,
            schema=derived.Unit.__table__.schema,
            index_col="unit_id",
        )

    print("Saving dataframes...")
    save_derived_table("unit", units, compress=True)


def fix_index(dataframe: pd.DataFrame, entity_name: str):
    dataframe.index = pd.Int64Index(np.arange(len(dataframe)) + 1)
    dataframe.index.name = f"{entity_name}_id"


def concatenated_table(name: str, *tables: pd.DataFrame) -> pd.DataFrame:
    table = pd.concat(tables, ignore_index=True)
    fix_index(table, name)
    return table


def save_derived_table(name: str, table: pd.DataFrame, compress=False):
    if compress:
        options = {"compression": "gzip"}
        path = DATASET_ROOT / f"derived/metadata/{name}.csv.gz"
    else:
        options = {}
        path = DATASET_ROOT / f"derived/metadata/{name}.csv"

    path.parent.mkdir(parents=True, exist_ok=True)

    if len(table.columns) > 0:
        table_to_save = table.convert_dtypes()
    else:
        table_to_save = table

    table_to_save.to_csv(path, **options)


def save_variations(table):
    common_name = regex_extract(r"(\w+)_id", table.index.name)[0]

    table_common = table.dropna(axis=1)
    save_derived_table(common_name, table_common)

    for subtype in table["type"].unique():
        type_specifics = (
            table.query(f"type == '{subtype}'")
            .drop(columns=table_common.columns)
            .dropna(axis=1)
        )
        save_derived_table(f"{common_name}/{subtype.lower()}", type_specifics)


class InitialDerivation:
    def __init__(self):
        self.recordings = load_original_entity("recording", use_units=False)
        self.plx = load_original_entity("plx", use_units=False)
        self.meas = load_original_entity("mea", use_units=False)
        self.sites = load_original_entity("site", use_units=False)
        self.sources = load_original_entity("source", use_units=False)
        self.spike_sources = pd.merge(
            self.sources,
            load_original_entity("source/spike"),
            left_index=True,
            right_index=True,
        )
        self.sortings = load_original_entity("sorting", use_units=False)

    def run(self):
        self.detections = self.derive_and_save_detections()
        self.new_sortings = self.derive_and_save_sortings()
        self.properties = self.derive_and_save_properties()

    def derive_and_save_detections(self):
        detections_from_spk = pd.DataFrame(
            [
                {
                    **row,
                    **{
                        "type": "OMNIPLEX",
                        "hdf_group": "/phase=DETECTION/type=OMNIPLEX",
                        "max_overlapping_samples": 0,
                    },
                }
                for _, row in (
                    self.spike_sources.reset_index()
                    .rename(
                        columns={
                            "prefix": "hdf_prefix",
                            "samples_before_threshold": "samples_before_alignment",
                        }
                    )
                    .drop(
                        columns=[
                            "type",
                            "recording_id",
                            "dsp_source_id",
                            "sampling_in_hz",
                            "hdf_path",
                        ]
                    )
                ).iterrows()
            ]
        )

        detections_from_spkc = pd.DataFrame(
            [
                {
                    **row.to_dict(),
                    **{
                        "type": "OMNIPLEX_RE_SPKC",
                        "threshold_in_stdev": k,
                        "waveform_alignment": "FIRST_VALLEY",
                        "samples_before_alignment": 8,
                        "max_overlapping_samples": 0,
                        "hdf_group": f"/phase=DETECTION/type=OMNIPLEX_RE source=SPKC threshold={k}dev",
                        "hdf_prefix": "SPK",
                        "waveform_samples": 32,
                    },
                }
                for _, row in (
                    self.sources.query("type == 'SPIKE_CONTINUOUS'")
                    .reset_index()
                    .drop(
                        columns=[
                            "type",
                            "recording_id",
                            "dsp_source_id",
                            "prefix",
                            "hdf_path",
                        ]
                    )
                ).iterrows()
                for k in range(3, 7)
            ]
        )

        detections_from_wb = pd.DataFrame(
            [
                {
                    **row.to_dict(),
                    **{
                        "type": "OMNIPLEX_RE_WB",
                        "threshold_in_stdev": k,
                        "waveform_alignment": "FIRST_VALLEY",
                        "samples_before_alignment": 8,
                        "max_overlapping_samples": 0,
                        "hdf_group": f"/phase=DETECTION/type=OMNIPLEX_RE source=WB threshold={k}dev",
                        "hdf_prefix": "SPK",
                        "waveform_samples": 32,
                        "dsp_filter_type": "BESSEL",
                        "dsp_filter_highpass_cutoff_in_hz": 300,
                        "dsp_filter_order": 2,
                        "dsp_filtering_mode": "BIDIRECTIONAL",
                    },
                }
                for _, row in (
                    self.sources.query("type == 'WIDEBAND'")
                    .reset_index()
                    .drop(
                        columns=[
                            "type",
                            "recording_id",
                            "dsp_source_id",
                            "prefix",
                            "hdf_path",
                        ]
                    )
                ).iterrows()
                for k in range(3, 7)
            ]
        )

        detections = concatenated_table(
            "detection", detections_from_spk, detections_from_spkc, detections_from_wb,
        ).sort_values(
            [
                "source_id",
                "type",
                "waveform_alignment",
                "waveform_samples",
                "samples_before_alignment",
                "max_overlapping_samples",
                "hdf_group",
            ]
        )

        detections["hdf_path"] = fk_join(detections, self.sources, self.recordings)[
            "recording_hdf_path"
        ].apply(lambda hdf_path: Path(hdf_path).with_suffix("") / "detections.h5")

        save_variations(detections)

        return detections

    def derive_and_save_sortings(self) -> pd.DataFrame:
        sortings_derived_from_existing_sources = pd.DataFrame(
            [
                {
                    "sorting_id": row["sorting_id"],
                    "detection_id": row["detection_id"],
                    "type": "OMNIPLEX",
                    "hdf_group": f"{row['hdf_group']}/phase=SORTING/type=OMNIPLEX source={Path(row['hdf_path_x']).stem.upper()}",
                    "hdf_prefix": "UNITS",
                }
                for _, row in pd.merge(
                    pd.merge(
                        fk_join(
                            self.sortings[["plx_id", "spike_source_id"]], self.plx,
                        ),
                        fk_join(
                            self.sortings.rename(
                                columns={"spike_source_id": "source_id"}
                            ),
                            self.spike_sources,
                            self.recordings,
                        ),
                        left_index=True,
                        right_index=True,
                    ).reset_index(),
                    self.detections.query("type == 'OMNIPLEX'").reset_index(),
                    left_on="spike_source_id",
                    right_on="source_id",
                )
                .query("~plx_corrupted and (recording_channels == sorted_channels)")
                .iterrows()
            ]
        )

        sortings_derived_from_new_detections = pd.DataFrame(
            [
                {
                    "detection_id": i,
                    "type": "OMNIPLEX_RE",
                    "clustering": "KMEANS_SCAN",
                    "max_clusters": 5,
                    "upsampling_in_doubles": 3,
                    "projection": "PCA",
                    "projection_dims": 3,
                    "feature_space": feature_space,
                    "hdf_group": f"{row['hdf_group']}/phase=SORTING/type=OMNIPLEX_RE projection=PCA(3) clustering=KMEANS(SCAN) features={feature_space}",
                    "hdf_prefix": "UNITS",
                }
                for i, row in self.detections.iterrows()
                for feature_space in {"WAVEFORM_SAMPLES", "WAVEFORM_FEATURES"}
            ]
        )

        new_sortings = concatenated_table(
            "new_sorting",
            sortings_derived_from_existing_sources,
            sortings_derived_from_new_detections,
        ).sort_values(["detection_id", "type", "hdf_group"])
        new_sortings["hdf_path"] = fk_join(
            new_sortings, self.detections, self.sources, self.recordings
        )["recording_hdf_path"].apply(
            lambda hdf_path: Path(hdf_path).with_suffix("") / "sortings.h5"
        )

        save_variations(new_sortings)

        return new_sortings

    def derive_and_save_properties(self) -> pd.DataFrame:
        properties = pd.concat(
            [self.derive_gratings_properties(), self.derive_rf_properties()],
            ignore_index=True,
        ).sort_values(
            [
                "new_sorting_id",
                "type",
                "background_estimator",
                "kernel_window_in_ms",
                "max_latency_in_ms",
                "latency_precision_in_ms",
            ]
        )
        fix_index(properties, "property")

        save_variations(properties)

        return properties

    def derive_gratings_properties(self) -> pd.DataFrame:
        properties = pd.DataFrame(
            index=pd.MultiIndex.from_product(
                [
                    fk_join(
                        self.new_sortings,
                        self.detections,
                        self.sources,
                        self.recordings,
                    )
                    .query("recording_protocol_key.str.contains('GRATINGS')")
                    .index,
                    [BackgroundActivityEstimator.WEIGHTED_NON_TRIAL.value],
                ],
                names=["new_sorting_id", "background_estimator"],
            )
        ).reset_index()

        properties["type"] = "GRATINGS"
        properties["kernel_window_in_ms"] = 150
        properties["max_latency_in_ms"] = 100
        properties["latency_precision_in_ms"] = 5

        properties_joined = fk_join(
            properties,
            self.new_sortings,
            self.detections,
            self.sources,
            self.recordings,
        )

        # TODO: These should be common to all properties rows.
        properties["hdf_path"] = properties_joined["new_sorting_hdf_path"].apply(
            lambda hdf_path: Path(hdf_path).parent.with_suffix("") / "properties.h5"
        )
        properties["hdf_group"] = properties_joined[
            ["new_sorting_hdf_group", "type"]
        ].apply(
            lambda row: f"{row['new_sorting_hdf_group']}/phase=PROPERTIES/type={row['type']}",
            axis=1,
        )
        properties["hdf_prefix"] = "PROPS"

        return properties

    def derive_rf_properties(self) -> pd.DataFrame:
        properties = pd.DataFrame(
            index=pd.MultiIndex.from_product(
                [
                    fk_join(
                        self.new_sortings,
                        self.detections,
                        self.sources,
                        self.recordings,
                    )
                    .query("recording_protocol_key.str.contains('MOVING BARS')")
                    .index,
                    [BackgroundActivityEstimator.WEIGHTED_NON_TRIAL.value],
                    [BackProjector.PROFILE_SUM.value],
                ],
                names=["new_sorting_id", "background_estimator", "back_projector",],
            )
        ).reset_index()

        properties["type"] = "MOVING_BARS"
        properties["kernel_window_in_ms"] = 150
        properties["resolution"] = 512
        properties["response_threshold"] = 0.75
        properties["max_latency_in_ms"] = 100
        properties["latency_precision_in_ms"] = 5
        properties["min_peak_disparity"] = 0.15

        properties_joined = fk_join(
            properties,
            self.new_sortings,
            self.detections,
            self.sources,
            self.recordings,
        )

        properties["hdf_path"] = properties_joined["new_sorting_hdf_path"].apply(
            lambda hdf_path: Path(hdf_path).parent.with_suffix("") / "properties.h5"
        )
        properties["hdf_group"] = properties_joined["new_sorting_hdf_group"].apply(
            lambda hdf_group: f"{hdf_group}/phase=ESTIMATION"
        )
        properties["hdf_prefix"] = "RF"

        return properties


class UnitDerivation:
    def __init__(self, engine: db.Engine):
        self.engine = engine
        self.saver = BulkSavingSession(engine)

    def run(self):
        with db.get_session(self.engine) as session:
            model_tuples: Iterable[
                Tuple[
                    derived.Property, derived.Sorting, derived.Detection, original.Spot,
                ]
            ] = session.query(
                derived.Property, derived.Sorting, derived.Detection, original.Spot,
            ).join(
                derived.Property.sorting,
                derived.Sorting.detection,
                derived.Detection.source,
                original.Source.experiment,
                original.Experiment.spots,
            )
        exportable_variables = pd.read_csv(
            DATASET_ROOT / "variable-exports.csv", index_col=0
        )

        for property_model, sorting_model, detection_model, spot_model in tqdm(
            model_tuples, total=model_tuples.count(), desc="Creating units",
        ):

            class H5Resource:
                def __init__(self, model: original.mixins.HDF):
                    self.file = h5.File(DATASET_ROOT / "derived/data" / model.hdf_path)

                    self.model = model

                def __enter__(self) -> Union[h5.Dataset, h5.Group]:
                    return self.file[
                        f"{self.model.hdf_group}/{self.model.hdf_prefix}{spot_model.channel:02d}"
                    ]

                def __exit__(self, exc_type, exc_val, exc_tb):
                    self.file.close()
                    if exc_type is not None:
                        raise exc_val

            with H5Resource(property_model) as properties_supergroup, H5Resource(
                sorting_model
            ) as sorting_group, H5Resource(detection_model) as detection_dataset:

                matching_variables = exportable_variables.query(
                    " & ".join(
                        [
                            f"(stimulus_type == {property_model.type!r})",
                            "exported",
                            "(hdf_expression.str.len() > 0)",
                        ]
                    )
                )

                spike_waveforms = detection_dataset["waveforms"]
                labels = sorting_group["labels"][:]

                for unit_name, properties_group in tqdm(
                    properties_supergroup.items(),
                    desc="Single/multi-units",
                    leave=False,
                ):
                    if (property_model.type == "MOVING_BARS") and (
                        properties_group.attrs.get("is_ambiguous", True)
                    ):
                        continue

                    unit_attrs = {
                        str(var_name): eval(
                            str(var_row["hdf_expression"]),
                            {"np": np, "unit": ureg.Unit, "props": properties_group,},
                        ).tolist()
                        for var_name, var_row in tqdm(
                            matching_variables.iterrows(),
                            total=len(matching_variables),
                            desc="Gathering variables",
                            leave=False,
                        )
                    }

                    if unit_name == "MULTI_UNIT":
                        unit_label = None
                        unit_type = UnitType.MULTI_UNIT
                        unit_waveforms = spike_waveforms[:]
                    elif unit_name == "MULTI_UNIT_WITHOUT_UNSORTED":
                        unit_label = None
                        unit_type = UnitType.MULTI_UNIT_WITHOUT_UNSORTED
                        unit_waveforms = spike_waveforms[labels[:] > 0]
                    elif unit_name == "UNSORTED":
                        unit_label = 0
                        unit_type = UnitType.UNSORTED
                        unit_waveforms = spike_waveforms[labels[:] == 0]
                    else:
                        unit_label = ord(unit_name[-1]) - ord("A") + 1
                        unit_type = UnitType.SINGLE_UNIT
                        unit_waveforms = spike_waveforms[labels[:] == unit_label]

                    unit_attrs.update(
                        {
                            "label": unit_label,
                            "spot_id": spot_model.id,
                            "property_id": property_model.id,
                            "type": unit_type.value,
                        }
                    )

                    if len(unit_waveforms):
                        unit_attrs.update(
                            {
                                "waveform_mean_in_mv": np.mean(
                                    unit_waveforms, axis=0
                                ).tolist(),
                                "waveform_deviation_in_mv": np.std(
                                    unit_waveforms, axis=0
                                ).tolist(),
                            }
                        )

                    unit_model = derived.Unit(**unit_attrs)
                    self.saver.add(unit_model)

        self.saver.close()


class BulkSavingSession:
    def __init__(self, engine: db.Engine, max_items=64):
        self.engine = engine
        self.objects = []
        self.max_items = max_items

    def add(self, object: original.Base):
        self.objects.append(object)
        if len(self.objects) > self.max_items:
            self._save()

    def _save(self):
        with db.get_session(self.engine) as session:
            session.bulk_save_objects(self.objects)
            session.commit()
        self.objects = []

    def close(self):
        self._save()


if __name__ == "__main__":
    main()
