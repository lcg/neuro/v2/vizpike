import json
import re
from enum import Enum
from pathlib import Path
from typing import Any, Callable, List, Sequence, Tuple, Union

import iso8601
import pandas as pd
from colour import Color
from pint_pandas import PintArray
from pydantic import BaseModel, BaseSettings

from vizpike import ureg as units
from vizpike.pipeline.utils import regex_extract


class Config(BaseSettings):
    dataset_root: Path = Path(__file__).parent.parent


config = Config()
DATASET_ROOT = config.dataset_root

units.setup_matplotlib()


def __del__delattr__(self, key):
    raise TypeError(
        f"Cannot delete {key!r} attribute in {self.__class__.__name__} object (it is immutable)"
    )


def __del__setattr__(self, key, value):
    raise TypeError(
        f"Cannot set {key!r} attribute to {value!r} in {self.__class__.__name__} object (it is immutable)"
    )


class ImmutableColor(Color):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__["__delattr__"] = __del__delattr__
        self.__dict__["__setattr__"] = __del__setattr__

    def __hash__(self):
        return hash(self.__dict__.values())

    def __lt__(self, other):
        return self.get_hex() < other.get_hex()


class HdfResource(BaseModel):
    hdf_group: str
    hdf_path: Path
    hdf_prefix: str

    def dataset_name(self, channel: int) -> str:
        return f"{self.hdf_group}/{self.hdf_prefix}{channel:02d}"


class UnitType(Enum):
    MULTI_UNIT = "MULTI_UNIT"
    UNSORTED = "UNSORTED"
    MULTI_UNIT_WITHOUT_UNSORTED = "MULTI_UNIT_WITHOUT_UNSORTED"
    SINGLE_UNIT = "SINGLE_UNIT"


def derived_data_path(rel_path: Union[Path, str]) -> Path:
    return DATASET_ROOT / "derived/data" / rel_path


def original_data_path(rel_path: Union[Path, str]) -> Path:
    return DATASET_ROOT / "original/data" / rel_path


def fk_join(
    source_table: pd.DataFrame,
    *target_tables: pd.DataFrame,
    drop_fk=True,
    keep_fks: List[str] = None,
) -> pd.DataFrame:
    partial_merge = source_table.copy()
    last_prefix = ""
    dropped_fks = []
    renamed_fks = {}

    for table in target_tables:
        table_index_name = table.index.name
        table_prefix = regex_extract(r"(\w+)_id", table_index_name)[0] + "_"
        fk_name = f"{last_prefix}{table_index_name}"

        partial_merge = pd.merge(
            partial_merge,
            table.add_prefix(table_prefix),
            left_on=fk_name,
            right_index=True,
        )
        last_prefix = table_prefix

        if drop_fk:
            if table_index_name in (keep_fks or {}):
                renamed_fks[fk_name] = table_index_name
            else:
                dropped_fks.append(fk_name)

    merge = partial_merge.drop(columns=dropped_fks).rename(columns=renamed_fks)

    return merge


def load_derived_entity(entity_name: str, subtypes=False) -> pd.DataFrame:
    base_entity_name, *_ = entity_name.split("/")
    index_col = f"{base_entity_name}_id"
    
    main_table = load_table(entity_name, EntityType.DERIVED, index_col=index_col)

    if subtypes and "type" in main_table.columns:
        sub_tables = [
            load_table(f"{entity_name}/{sub_type.lower()}", EntityType.DERIVED, index_col=index_col)
            for sub_type in main_table["type"].unique()
        ]
        table = pd.merge(
            main_table, pd.concat(sub_tables), left_index=True, right_index=True,
        )
    else:
        table = main_table

    return table


def load_derived_table(entity_name: str) -> pd.DataFrame:
    path = DATASET_ROOT / f"derived/metadata/{entity_name}.csv"
    table = pd.read_csv(check_gz_suffix(path), index_col=0)
    return table


def load_original_entity(name: str, use_units=True) -> pd.DataFrame:
    table_path = DATASET_ROOT / f"original/metadata/{name}.csv"

    base_entity, specific_entity = regex_extract(r"^(?:(\w+?)/)?(\w+?)$", name)
    index_name = f"{base_entity or specific_entity}_id"

    if base_entity is None and re.match(r"[^_]+_[^_]+", specific_entity):
        index_col = (0, 1)
    else:
        index_col = 0

    table = pd.read_csv(check_gz_suffix(table_path), index_col=index_col)
    table.index.name = index_name

    if use_units:
        return apply_units_to_cols(table)
    else:
        return table


class EntityType(Enum):
    ORIGINAL = "original"
    DERIVED = "derived"


def load_table(
    entity_name: str,
    entity_type: EntityType,
    index_col: Union[int, str, Sequence[Union[int, str]]] = 0,
) -> pd.DataFrame:
    path = check_gz_suffix(
        DATASET_ROOT / f"{entity_type.value}/metadata/{entity_name}.csv"
    )
    table = pd.read_csv(path, index_col=index_col)
    return table


def load_protocols(use_units=True) -> Tuple[pd.DataFrame, pd.DataFrame]:
    with open(DATASET_ROOT / "original/metadata/protocol.json") as protocol_file:
        protocol_metadata = json.load(protocol_file)

    unitless_protocols = pd.DataFrame(
        [
            {
                **{"protocol_key": protocol_key},
                **{
                    attr_name: attr_value
                    for attr_name, attr_value in protocol_attrs.items()
                    if attr_name not in ["conditions", "extra_parameters"]
                },
                **protocol_attrs["extra_parameters"],
            }
            for protocol_key, protocol_attrs in protocol_metadata.items()
        ]
    ).set_index("protocol_key")
    unitless_conditions = pd.DataFrame(
        [
            {**{"protocol_key": protocol_key}, **condition_dict}
            for protocol_key, protocol_attrs in protocol_metadata.items()
            for condition_dict in protocol_attrs["conditions"]["array"]
        ]
    ).set_index(["protocol_key", "code"])

    if use_units:
        protocols = apply_units_to_cols(unitless_protocols)
        conditions = apply_units_to_cols(unitless_conditions)
    else:
        protocols = unitless_protocols
        conditions = unitless_conditions

    return protocols.convert_dtypes(), conditions.convert_dtypes()


def check_gz_suffix(path: Path) -> Path:
    gz_path = path.with_suffix(f"{path.suffix}.gz")
    if gz_path.exists():
        return gz_path
    else:
        return path


def apply_units_to_cols(table: pd.DataFrame) -> pd.DataFrame:
    new_table = pd.DataFrame(index=table.index)

    for column in table.columns:
        physical_unit_column = re.match(r"(\w+)_in_(\w+)", column)

        if physical_unit_column:
            property_name, unit_name = physical_unit_column.groups()
            conversion = _try_get_conversion(unit_name)
            new_table[property_name] = conversion(table[column])
        else:
            new_table[column] = table[column]

    return new_table


def _try_get_conversion(unit_name: str) -> Callable[[pd.Series], pd.Series]:
    if unit_name in apply_units_to_cols.conversions:
        return apply_units_to_cols.conversions[unit_name]
    else:
        raise UnhandledPhysicalUnit(f"No known conversion for {unit_name}")


class UnhandledPhysicalUnit(Exception):
    pass


class PintConverter:
    def __init__(self, unit: str):
        self.unit = unit

    def __call__(self, series: pd.Series) -> pd.Series:
        return pd.Series(
            index=series.index,
            data=PintArray(series.values, dtype=getattr(units, self.unit)),
        )


class ValueConverter:
    def __init__(self, func: Callable[[Union[str, int, float]], Any]):
        self.func = func

    def __call__(self, series: pd.Series) -> pd.Series:
        null_values = series.isna()

        null_series = series[null_values]
        non_null_series = series[~null_values]

        converted_series = pd.Series(
            index=non_null_series.index, data=[self.func(x) for x in non_null_series],
        )

        final_series = pd.concat([null_series, converted_series]).sort_index()
        return final_series


apply_units_to_cols.conversions = {
    "bytes": PintConverter("byte"),
    "um": PintConverter("micrometer"),
    "deg": PintConverter("deg"),
    "deg_per_sec": PintConverter("deg / second"),
    "hz": PintConverter("hertz"),
    "invdeg": PintConverter("1/deg"),
    "ms": PintConverter("ms"),
    "hex": ValueConverter(lambda x: ImmutableColor(x)),
    "iso8601": ValueConverter(iso8601.parse_date),
}


def dequantify(df: pd.DataFrame) -> pd.DataFrame:
    """Convert physical units-aware columns in a dataframe to ordinary scalar columns, renaming such columns after their
    respective units.
    """
    new_df = pd.DataFrame(index=df.index[:])
    for column in df:
        try:
            values = df[column].values.quantity
            m = values.magnitude
            u = values.units
            if len(m.shape) > 1:
                new_values = m.tolist()
            else:
                new_values = m
            if u == units.UnitDetails("dimensionless"):
                new_column = column
            else:
                new_column = f"{column}_in_{u}"
        except AttributeError:
            new_values = df[column].values
            new_column = column
        new_df[new_column] = new_values
    return new_df
