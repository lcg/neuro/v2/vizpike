from enum import Enum
from typing import List, Tuple, Union

import numpy as np
import pandas as pd
from nptyping import NDArray
from pint import Quantity
from scipy import integrate
from scipy.ndimage import convolve1d
from scipy.signal.windows import gaussian
from tqdm import tqdm

from vizpike.pipeline.dataset import fk_join
from vizpike.pipeline.typeann import (
    FiringRate,
    SpikeMatrix,
    SpikeTrain,
    Tick,
    Time,
    TimeFrequency,
    Timestamps,
    TimeSupport,
)
from vizpike.pipeline.utils import dict_like_enum, strategy


@dict_like_enum
@strategy
class BackgroundActivityEstimator(Enum):
    PRE_EXPERIMENT = "PRE_EXPERIMENT"
    INTER_TRIAL = "INTER_TRIAL"
    WEIGHTED_NON_TRIAL = "WEIGHTED_NON_TRIAL"


def firing_rate(
    spikes: SpikeTrain, window: Time, time_sampling: TimeFrequency
) -> FiringRate:
    kernel = gaussian_kernel(window, time_sampling).magnitude
    activity = convolve1d(spikes, kernel, axis=-1) * time_sampling.units
    return activity


def gaussian_kernel(
    window: Quantity, sampling_rate: Quantity, latency: Quantity = None
) -> TimeFrequency:
    latency = latency or 0 * (1 / sampling_rate).units
    padding = int(latency * sampling_rate)
    apperture = int(window * sampling_rate)
    std = apperture / 6
    full_kernel = gaussian(apperture + 2 * padding, std)
    padded_kernel = full_kernel[padding:]
    normalized_kernel = padded_kernel / integrate.trapz(
        padded_kernel, dx=(1 / sampling_rate)
    )
    return normalized_kernel


def spike_matrix(
    spike_times: Time,
    trials: pd.DataFrame,
    conditions: pd.DataFrame,
    *,
    group_by: str,
    where: str = None,
    order_by: Union[str, List[str]] = None,
    points: int = None,
    sampling_rate: TimeFrequency = None,
) -> Tuple[NDArray, SpikeMatrix]:
    trial_matrix = fk_join(trials, conditions, drop_fk=False)

    if where is None:
        filtered_trial_matrix = trial_matrix
    else:
        filtered_trial_matrix = trial_matrix.query(where)

    if order_by is None:
        ordered_trial_matrix = filtered_trial_matrix
    else:
        ordered_trial_matrix = filtered_trial_matrix.sort_values(order_by)

    grouped_trial_matrix = ordered_trial_matrix.groupby(group_by)

    trial_groups = np.hstack([group_key for group_key, _ in grouped_trial_matrix])
    trial_spikes = np.array(
        [
            [
                spike_train(
                    spike_times,
                    row["start"],
                    row["stop"],
                    points=points,
                    sampling_rate=sampling_rate,
                )
                for _, row in group.iterrows()
            ]
            for _, group in tqdm(
                grouped_trial_matrix,
                desc="Computing spike trains",
                total=len(grouped_trial_matrix),
                leave=False,
            )
        ]
    )
    return trial_groups, trial_spikes


def spike_train(
    timestamps: Timestamps,
    start: Tick,
    end: Tick,
    *,
    points: int = None,
    sampling_rate: TimeFrequency = None,
    return_support=False,
) -> Union[SpikeTrain, Tuple[SpikeTrain, TimeSupport]]:
    if points is None:
        points = int(np.round((end - start) * sampling_rate))

    support = np.linspace(start, end, points + 1)
    train, _ = np.histogram(
        timestamps.to(support.units).magnitude, bins=support.magnitude
    )

    if return_support:
        return train, support
    else:
        return train


def concatenated_spike_trains(
    timestamps: Timestamps,
    trials_conditions: pd.DataFrame,
    *,
    points: int = None,
    time_sampling: TimeFrequency = None,
) -> SpikeTrain:
    return np.hstack(
        [
            spike_train(
                timestamps,
                t["start"],
                t["stop"],
                points=points,
                sampling_rate=time_sampling,
            )
            for _, t in trials_conditions.iterrows()
        ]
    )


@BackgroundActivityEstimator.PRE_EXPERIMENT.handler
def _pre_experiment_base_rate(
    timestamps: Timestamps,
    trials: pd.DataFrame,
    conditions: pd.DataFrame,
    kernel_window: Quantity,
    time_sampling: Quantity,
) -> Quantity:
    trials_conditions = fk_join(trials, conditions, drop_fk=False).query(
        "~condition_is_stimulus"
    )
    pre_trial_spikes = concatenated_spike_trains(
        timestamps, trials_conditions, time_sampling=time_sampling
    )
    pre_trial_baseline = np.mean(
        firing_rate(pre_trial_spikes, window=kernel_window, time_sampling=time_sampling)
    )
    return pre_trial_baseline


@BackgroundActivityEstimator.INTER_TRIAL.handler
def _inter_trial_base_rate(
    timestamps: Timestamps,
    trials: pd.DataFrame,
    conditions: pd.DataFrame,
    kernel_window: Quantity,
    time_sampling: Quantity,
) -> Quantity:
    trials_conditions = fk_join(trials, conditions, drop_fk=False).query(
        "condition_id == 0"
    )
    inter_trial_spikes = concatenated_spike_trains(
        timestamps, trials_conditions, time_sampling=time_sampling
    )
    inter_trial_baseline = np.mean(
        firing_rate(
            inter_trial_spikes, window=kernel_window, time_sampling=time_sampling,
        )
    )
    return inter_trial_baseline


@BackgroundActivityEstimator.WEIGHTED_NON_TRIAL.handler
def _non_trial_base_rate(
    timestamps: Timestamps,
    trials: pd.DataFrame,
    conditions: pd.DataFrame,
    kernel_window: Quantity,
    time_sampling: Quantity,
) -> Quantity:
    trial_matrix = fk_join(trials, conditions, drop_fk=False)

    pre_experiment_weight = np.sum(
        trial_matrix.query("~condition_is_stimulus")["duration"].values.quantity
    )
    inter_trial_weight = np.sum(
        trial_matrix.query("condition_id == 0")["duration"].values.quantity
    )
    weights = np.hstack([pre_experiment_weight, inter_trial_weight])

    base_rate_kwargs = dict(
        timestamps=timestamps,
        trials=trials,
        conditions=conditions,
        kernel_window=kernel_window,
        time_sampling=time_sampling,
    )
    base_rates = np.hstack(
        [
            _pre_experiment_base_rate(**base_rate_kwargs),
            _inter_trial_base_rate(**base_rate_kwargs),
        ]
    )

    base_rate = np.sum(base_rates * weights) / np.sum(weights)

    return base_rate
