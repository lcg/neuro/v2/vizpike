from typing import Any, Tuple

import attr
import numpy as np
from nptyping import NDArray, StructuredType, UInt64
from scipy.signal import bessel, filtfilt, lfilter

from vizpike.pipeline.params.detection import (
    DspFilteringMode,
    DspFilterType,
    WaveformAlignment,
)
from vizpike.pipeline.utils import apply_strategy

TimestampDtype = UInt64
SlowSignal = NDArray[(Any,), float]
SpikeContinuousSignal = SlowSignal
SpikeSignal = NDArray[(Any,), StructuredType[TimestampDtype, NDArray[(Any,), float]]]
WideBandSignal = SlowSignal


@attr.s
class WaveformProperties:
    alignment: WaveformAlignment = attr.ib()
    shape: Tuple[int, int] = attr.ib()
    overlap_in_samples: int = attr.ib()

    @property
    def waveform_samples(self) -> int:
        return sum(self.shape)

    @property
    def size(self) -> int:
        return sum(self.shape)


@attr.s
class FilteringProperties:
    type: DspFilterType = attr.ib()
    mode: DspFilteringMode = attr.ib()
    order: int = attr.ib()
    cutoff_in_hz: float = attr.ib()


def detect_spikes_from_spkc(
    spkc_signal: SpikeContinuousSignal,
    threshold_in_stdev: float,
    *,
    waveform_properties: WaveformProperties,
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
    spkc_mean, spkc_stdev = np.mean(spkc_signal), np.std(spkc_signal)
    spkc_threshold = spkc_mean - threshold_in_stdev * spkc_stdev

    wf_shape = waveform_properties.shape
    wf_size = waveform_properties.size
    wf_overlap = waveform_properties.overlap_in_samples

    initial_timestamps, initial_waveforms = threshold_crossing_spikes(
        spkc_signal, spkc_threshold, wf_shape
    )
    aligned_timestamps, aligned_waveforms = filtered_and_aligned_spikes(
        initial_timestamps,
        initial_waveforms,
        spkc_signal,
        waveform_properties.alignment,
        wf_shape,
    )
    final_timestamps, final_waveforms = non_overlapping_spikes(
        aligned_timestamps, aligned_waveforms, wf_overlap, wf_size
    )

    return final_timestamps, final_waveforms


def detect_spikes_from_wb(
    wb_signal: WideBandSignal,
    threshold_in_stdev: float,
    sampling_in_hz: float,
    *,
    waveform_properties: WaveformProperties,
    filtering_properties: FilteringProperties,
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:

    spkc = spkc_from_wb(wb_signal, sampling_in_hz, filtering_properties)
    spikes = detect_spikes_from_spkc(
        spkc, threshold_in_stdev, waveform_properties=waveform_properties
    )

    return spikes


def spkc_from_wb(
    wb_signal: WideBandSignal, sampling_rate_in_hz: float, props: FilteringProperties,
) -> SpikeContinuousSignal:
    filters = {
        DspFilterType.BESSEL: lambda: bessel(
            props.order,
            2 * props.cutoff_in_hz,
            btype="highpass",
            fs=sampling_rate_in_hz,
            output="ba",
        ),
    }
    appliers = {
        DspFilteringMode.LEFT2RIGHT: lambda: lfilter(
            filter_num, filter_denom, wb_signal
        ),
        DspFilteringMode.BIDIRECTIONAL: lambda: filtfilt(
            filter_num, filter_denom, wb_signal
        ),
    }

    filter_num, filter_denom = apply_strategy(filters, props.type)
    signal = apply_strategy(appliers, props.mode)

    return signal


def threshold_crossing_spikes(
    spkc_in_mv: SpikeContinuousSignal,
    threshold_in_mv: float,
    wf_shape: Tuple[int, int],
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
    threshold_crossing = (spkc_in_mv[:-1] >= threshold_in_mv) & (
        spkc_in_mv[1:] < threshold_in_mv
    )

    initial_timestamps = np.flatnonzero(threshold_crossing)
    initial_waveforms = clip_waveforms(spkc_in_mv, wf_shape, initial_timestamps)

    return initial_timestamps, initial_waveforms


def filtered_and_aligned_spikes(
    timestamps: NDArray[(Any,), TimestampDtype],
    waveforms: NDArray[(Any, Any), float],
    spkc: SpikeContinuousSignal,
    alignment: WaveformAlignment,
    wf_shape: Tuple[int, int],
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
    samples_pre_align = wf_shape[0]

    local_extrema_search = LocalExtremaSearch(waveforms)

    do_repolarize = np.any((waveforms[:, samples_pre_align:] > 0), axis=1)
    are_positive_before_threshold = np.any(
        (waveforms[:, :samples_pre_align] > 0), axis=1
    )
    have_valley_and_positive_peak = local_extrema_search.check_valley_and_peak(
        samples_pre_align
    )

    alignment_strategies = {
        WaveformAlignment.THRESHOLD_CROSSING: lambda: 0,
        WaveformAlignment.FIRST_VALLEY: lambda: np.argmax(
            local_extrema_search.local_minima[:, samples_pre_align - 1 :], axis=1,
        ),
    }

    align_offset = apply_strategy(alignment_strategies, alignment)

    pre_align_filter = (
        do_repolarize & are_positive_before_threshold & have_valley_and_positive_peak
    )
    aligned_timestamps = (timestamps + align_offset)[pre_align_filter]
    aligned_waveforms = clip_waveforms(spkc, wf_shape, aligned_timestamps)

    return aligned_timestamps, aligned_waveforms


def non_overlapping_spikes(
    timestamps: NDArray[(Any,), TimestampDtype],
    waveforms: NDArray[(Any, Any), float],
    max_overlap: int,
    wf_size: int,
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
    max_timestamp = np.iinfo(np.uint64).max
    interspike_intervals = np.diff(timestamps, append=max_timestamp)
    non_overlapping = interspike_intervals > wf_size - max_overlap

    final_timestamps = timestamps[non_overlapping]
    final_waveforms = waveforms[non_overlapping]

    return final_timestamps, final_waveforms


class LocalExtremaSearch:
    def __init__(self, waveforms: NDArray[(Any, Any), float]):
        self.waveforms = waveforms
        self.local_minima: NDArray[(Any, Any), bool] = (
            waveforms[:, :-2] > waveforms[:, 1:-1]
        ) & (waveforms[:, 1:-1] < waveforms[:, 2:])
        self.local_maxima: NDArray[(Any, Any), bool] = (
            waveforms[:, :-2] < waveforms[:, 1:-1]
        ) & (waveforms[:, 1:-1] > waveforms[:, 2:])

    def check_valley_and_peak(self, samples_pre_align: int) -> NDArray[(Any,), bool]:
        have_valley = np.any(self.local_minima[:, samples_pre_align - 1 :], axis=1)
        have_positive_peak = np.any(
            self.local_maxima[:, samples_pre_align:]
            & (self.waveforms[:, samples_pre_align + 1 : -1] > 0),
            axis=1,
        )
        return have_valley & have_positive_peak


def clip_waveforms(
    signal: SpikeContinuousSignal,
    shape: Tuple[int, int],
    timestamps: NDArray[(Any,), TimestampDtype],
) -> NDArray[(Any, Any), float]:
    return np.take(
        signal,
        timestamps[:, np.newaxis] + np.arange(-shape[0], shape[1])[np.newaxis, :],
    )
