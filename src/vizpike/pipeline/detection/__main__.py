from itertools import groupby
from multiprocessing import Lock, Manager, Pool, cpu_count
from multiprocessing.dummy import Pool as DummyPool
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Tuple, Union

import attr
import click
import h5py as h5
import numpy as np
from nptyping import NDArray
from tqdm import tqdm

from vizpike.pipeline.dataset import DATASET_ROOT, HdfResource
from vizpike.pipeline.detection import (
    FilteringProperties,
    SlowSignal,
    SpikeSignal,
    TimestampDtype,
    WaveformProperties,
    detect_spikes_from_spkc,
    detect_spikes_from_wb,
)
from vizpike.pipeline.params import Recording, load_dvc_params
from vizpike.pipeline.params.detection import (
    DetectionTask,
    DetectionType,
    OmniplexParams,
    OmniplexReSpkcParams,
    OmniplexReWbParams,
)
from vizpike.pipeline.utils import apply_strategy

DEFAULT_WORKERS = max(1, cpu_count() // 2)


@click.command()
@click.argument("task_keys", type=str, nargs=-1)
@click.option(
    "--workers",
    "-w",
    type=int,
    default=DEFAULT_WORKERS,
    help="Number of jobs. Defaults to CPU count -1.",
)
def main(task_keys, workers):
    dvc_params = load_dvc_params()
    pool_factory = DummyPool if workers == 1 else Pool

    with Manager() as manager, pool_factory(workers) as worker_pool:
        jobs = Job.generate_from(dvc_params, manager, task_keys)
        job_results = worker_pool.imap_unordered(dispatch_job, jobs)
        for _ in tqdm(job_results, desc=f"Spike detection jobs ({workers} worker(s))",):
            pass


@attr.s
class TaskSpecifics:
    output: HdfResource = attr.ib()
    params: Union[OmniplexReWbParams, OmniplexReSpkcParams, OmniplexParams] = attr.ib()

    @classmethod
    def from_task(cls, task: DetectionTask) -> "TaskSpecifics":
        return TaskSpecifics(output=task.output, params=task.params)


@attr.s
class Job:
    input: HdfResource = attr.ib()
    channel: int = attr.ib()
    specifics: List[TaskSpecifics] = attr.ib()
    type: DetectionType = attr.ib()
    write_lock: Lock = attr.ib()

    @staticmethod
    def wf_properties_from_params(params):
        samples_left = params.samples_before_alignment
        samples_right = params.waveform_samples - samples_left

        waveform_properties = WaveformProperties(
            alignment=params.waveform_alignment,
            shape=(samples_left, samples_right),
            overlap_in_samples=params.max_overlapping_samples,
        )
        return waveform_properties

    @classmethod
    def generate_from(
        cls,
        dvc_params: Dict[str, Recording],
        manager: Manager,
        recording_keys: Optional[List[str]] = None,
    ) -> "Iterable[Job]":
        if recording_keys:
            selected_keys = set(dvc_params).intersection(recording_keys)
        else:
            selected_keys = dvc_params.keys()

        write_locks = {
            (key, task.output.hdf_path): manager.Lock()
            for key in selected_keys
            for task in dvc_params[key].detections
        }

        for key in selected_keys:
            recording = dvc_params[key]
            same_resource_task_groups = groupby(
                recording.detections,
                key=lambda task: (task.input, task.output.hdf_path, task.type),
            )

            for (
                (hdf_input, hdf_output_path, task_type),
                task_group,
            ) in same_resource_task_groups:
                group_specifics = list(map(TaskSpecifics.from_task, task_group))

                for channel in range(1, recording.channels + 1):
                    job = Job(
                        input=hdf_input,
                        channel=channel,
                        specifics=group_specifics,
                        type=task_type,
                        write_lock=write_locks[key, hdf_output_path],
                    )
                    yield job

    def __call__(self):
        load_by_type = {
            DetectionType.OMNIPLEX: load_spike_dataset,
            DetectionType.OMNIPLEX_RE_SPKC: load_slow_dataset,
            DetectionType.OMNIPLEX_RE_WB: load_slow_dataset,
        }
        detect_by_type = {
            DetectionType.OMNIPLEX: self._detect_omniplex_spk,
            DetectionType.OMNIPLEX_RE_SPKC: self._detect_omniplex_re_spkc,
            DetectionType.OMNIPLEX_RE_WB: self._detect_omniplex_re_wb,
        }

        input_signal, _ = apply_strategy(
            load_by_type, self.type, self.input, self.channel
        )

        for specifics in self.specifics:
            params = specifics.params
            timestamps, waveforms = apply_strategy(
                detect_by_type, self.type, input_signal, params
            )

            with self.write_lock:
                save_spikes_dataset(
                    specifics.output, self.channel, timestamps, waveforms
                )

    def _detect_omniplex_spk(
        self, input_signal: SpikeSignal, params: OmniplexParams
    ) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
        timestamps = input_signal["timestamps"]
        waveforms = input_signal["waveforms"]
        return timestamps, waveforms

    def _detect_omniplex_re_spkc(
        self, input_signal: SlowSignal, params: OmniplexReSpkcParams
    ) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
        timestamps, waveforms = detect_spikes_from_spkc(
            input_signal,
            params.threshold_in_stdev,
            waveform_properties=self.wf_properties_from_params(params),
        )
        return timestamps, waveforms

    def _detect_omniplex_re_wb(
        self, input_signal: SlowSignal, params: OmniplexReWbParams
    ) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
        waveform_properties = self.wf_properties_from_params(params)

        timestamps, waveforms = detect_spikes_from_wb(
            input_signal,
            params.threshold_in_stdev,
            sampling_in_hz=params.sampling_in_hz,
            waveform_properties=waveform_properties,
            filtering_properties=FilteringProperties(
                type=params.dsp_filter_type,
                mode=params.dsp_filtering_mode,
                order=params.dsp_filter_order,
                cutoff_in_hz=params.dsp_filter_highpass_cutoff_in_hz,
            ),
        )
        return timestamps, waveforms


def dispatch_job(job: Job):
    job()


def load_slow_dataset(resource: HdfResource, channel: int) -> Tuple[SlowSignal, float]:
    _, data, attrs, _ = load_dataset(resource, channel)

    slow_signal = data * attrs["ad_to_mv"]
    sampling = attrs["plx_ADFreq"]

    return slow_signal, sampling


def load_spike_dataset(
    resource: HdfResource, channel: int
) -> Tuple[SpikeSignal, float]:
    _, data, attrs, file_attrs = load_dataset(resource, channel)

    spike_signal = np.empty(
        len(data),
        dtype=[
            ("timestamps", "u8"),
            ("waveforms", f"({data['waveform'].shape[1]},)f4"),
        ],
    )

    spike_signal["timestamps"] = data["timestamp"]
    spike_signal["waveforms"] = data["waveform"] * attrs["ad_to_mv"]
    sampling = file_attrs["plx_ADFrequency"]

    return spike_signal, sampling


def load_dataset(
    resource: HdfResource, channel: int
) -> Tuple[str, np.ndarray, dict, dict]:
    dataset_name = f"{resource.hdf_group}/{resource.hdf_prefix}{channel:02d}"
    h5_path = hdf_path(resource.hdf_path)

    with h5.File(h5_path) as h5_file:
        dataset = h5_file[dataset_name]
        file_attrs = dict(h5_file.attrs)
        attrs = dict(dataset.attrs)
        data = np.array(dataset)

    return dataset_name, data, attrs, file_attrs


def save_spikes_dataset(
    resource: HdfResource,
    channel: int,
    timestamps: NDArray[(Any,), TimestampDtype],
    waveforms: NDArray[(Any, Any), TimestampDtype],
):
    dataset_name = f"{resource.hdf_group}/{resource.hdf_prefix}{channel:02d}"
    h5_path = hdf_path(resource.hdf_path)

    h5_path.parent.mkdir(parents=True, exist_ok=True)
    with h5.File(h5_path, "a") as h5file:
        dataset = h5file.create_dataset(
            dataset_name,
            dtype=[("timestamps", "u8"), ("waveforms", f"({waveforms.shape[1]},)f4"),],
            shape=(len(timestamps),),
            chunks=True,
            compression="gzip",
        )
        dataset["timestamps"] = timestamps
        dataset["waveforms"] = waveforms


def hdf_path(relative_path: Path) -> Path:
    return DATASET_ROOT / relative_path


if __name__ == "__main__":
    main()
