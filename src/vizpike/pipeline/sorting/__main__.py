from abc import ABCMeta, abstractmethod
from itertools import chain, groupby
from multiprocessing import Lock, Manager, Pool, cpu_count
from typing import List, Tuple, Union

import attr
import click
import h5py as h5
import numpy as np
import pandas as pd
from tqdm import tqdm

from vizpike.pipeline.dataset import (
    DATASET_ROOT,
    HdfResource,
    derived_data_path,
    fk_join,
    load_derived_entity,
    load_original_entity,
    original_data_path,
)
from vizpike.pipeline.sorting import (
    KMeansScanner,
    OmniplexFeatures,
    OmniplexReSorting,
    PCAProjector,
    Sorting,
    WaveformSamples,
    load_waveforms_dataset,
)
from vizpike.pipeline.utils import apply_strategy


@attr.s(auto_attribs=True)
class Job(metaclass=ABCMeta):
    title: str
    input: HdfResource
    output: HdfResource
    channel: int

    @abstractmethod
    def __call__(self, lock: Lock):
        pass


@click.group()
def main():
    pass


@main.command(name="run")
@click.argument("recording_title", type=str)
@click.option("--workers", "-w", type=int, default=max(cpu_count() - 2, 1))
def main_run(recording_title, workers: int):
    jobs = select_jobs(recording_title)

    with Manager() as manager, Pool(workers) as pool:
        hdf_lock = manager.Lock()
        job_results = pool.imap_unordered(run_job, [(j, hdf_lock) for j in jobs])

        for _ in tqdm(job_results, total=len(jobs), desc="Jobs"):
            pass


@main.command(name="dvc")
@click.argument("recording_titles", type=str, nargs=-1)
def main_dvc(recording_titles: Tuple[str]):
    def job_key(j):
        return j.title, j.output.hdf_path

    jobs = select_jobs(*recording_titles)
    grouped_jobs = groupby(sorted(jobs, key=job_key), job_key)

    for (title, output_path), job_group in grouped_jobs:
        constant_args = [
            f"-n {title}_sorting",
            "-d derived/metadata/new_sorting.csv",
            "-d src/sorting",
            f"-o {output_path.relative_to(DATASET_ROOT)}",
        ]
        dependency_args = list(
            set(
                [
                    f"-d {job.input.hdf_path.relative_to(DATASET_ROOT)}"
                    for job in job_group
                ]
            )
        )
        command = " ".join(
            [
                "dvc run",
                " ".join(chain(constant_args, dependency_args)),
                f"PYTHONPATH=src python -m sorting run {title}",
            ]
        )
        print(command)


def run_job(args: Tuple[Job, Lock]):
    job, lock = args
    return job(lock)


def select_jobs(*recording_titles: str) -> List[Job]:
    metadata = select_metadata(*recording_titles)
    jobs = [
        job_for(row, channel)
        for _, row in metadata.iterrows()
        for channel in range(1, row["recording_channels"] + 1)
    ]
    return jobs


def select_metadata(*recording_titles: str) -> pd.DataFrame:
    detections = load_derived_entity("detection")
    new_sortings = load_derived_entity("new_sorting", subtypes=True)

    old_sortings = load_original_entity("sorting").rename(
        columns={"spike_source_id": "source_id"}
    )
    sources = load_original_entity("source", use_units=False)
    recordings = load_original_entity("recording")

    full_df = pd.concat(
        [
            fk_join(new_sortings, old_sortings, sources, recordings),
            fk_join(new_sortings, detections, sources, recordings).query(
                "sorting_id.isnull()"
            ),
        ]
    ).convert_dtypes()

    if recording_titles:
        final_df = full_df[full_df["recording_title"].isin(recording_titles)]
    else:
        final_df = full_df

    return final_df


def job_for(row: pd.Series, channel: int) -> Job:
    def omniplex_copy_job():
        return OmniplexCopyJob(
            title=row["recording_title"],
            input=HdfResource(
                hdf_path=original_data_path(row["sorting_hdf_path"]),
                hdf_group="spike",
                hdf_prefix="SPK",
            ),
            output=output_resource,
            channel=channel,
        )

    def omniplex_re_job():
        feature_strategies = {
            "WAVEFORM_FEATURES": lambda: OmniplexFeatures(
                upsampling_doubles=row["upsampling_in_doubles"],
                sampling_rate_in_hz=row["source_sampling_in_hz"],
                samples_before_align=row["detection_samples_before_alignment"],
            ),
            "WAVEFORM_SAMPLES": lambda: WaveformSamples(),
        }

        return OmniplexReSortingJob(
            title=row["recording_title"],
            input=HdfResource(
                hdf_path=derived_data_path(row["detection_hdf_path"]),
                hdf_group=row["detection_hdf_group"],
                hdf_prefix=row["detection_hdf_prefix"],
            ),
            output=output_resource,
            channel=channel,
            sorting=OmniplexReSorting(
                featurizer=apply_strategy(feature_strategies, row["feature_space"]),
                projector=PCAProjector(dims=row["projection_dims"]),
                clusterer=KMeansScanner(max_clusters=row["max_clusters"]),
            ),
        )

    job_strategies = {
        "OMNIPLEX": omniplex_copy_job,
        "OMNIPLEX_RE": omniplex_re_job,
    }

    output_resource = HdfResource(
        hdf_path=derived_data_path(row["hdf_path"]),
        hdf_group=row["hdf_group"],
        hdf_prefix=row["hdf_prefix"],
    )
    job = apply_strategy(job_strategies, row["type"])

    return job


class OmniplexCopyJob(Job):
    def __call__(self, lock):
        input_dataset_name = self.input.dataset_name(self.channel)

        with h5.File(self.input.hdf_path, "r") as input_hdf_file:
            sorted_spikes = np.array(input_hdf_file[input_dataset_name])

        labels = sorted_spikes["unit"]
        sorting = Sorting(
            n_clusters=int(np.max(labels)) if len(labels) else 0,
            labels=labels,
            scores={},
        )
        save_sorting_result(sorting, self.output, self.channel, lock)


@attr.s(auto_attribs=True)
class OmniplexReSortingJob(Job):
    sorting: OmniplexReSorting

    def __call__(self, lock):
        _, waveforms = load_waveforms_dataset(self.input, self.channel)
        result = self.sorting(waveforms)

        save_sorting_result(result, self.output, self.channel, lock)


def save_sorting_result(result: Sorting, output: HdfResource, channel: int, lock: Lock):
    output_hdf_path = output.hdf_path
    output_hdf_path.parent.mkdir(parents=True, exist_ok=True)
    dataset_group = output.dataset_name(channel)

    attrs = {f"score_{key}": value for key, value in result.scores.items()}

    with lock:
        with h5.File(output_hdf_path, "a") as hdf_file:
            save_hdf_dataset(hdf_file, f"{dataset_group}/labels", result.labels)

            if result.features is not None:
                save_hdf_dataset(hdf_file, f"{dataset_group}/features", result.features)

            if result.feature_projections is not None:
                save_hdf_dataset(
                    hdf_file,
                    f"{dataset_group}/feature_projections",
                    result.feature_projections,
                )

            hdf_file[dataset_group].attrs["n_clusters"] = result.n_clusters
            hdf_file[dataset_group].attrs.update(attrs)


def save_hdf_dataset(
    group: h5.Group, name: str, data: Union[np.ndarray, pd.DataFrame]
) -> h5.Dataset:
    dataset = group.create_dataset(name, chunks=True, compression="gzip", data=data)
    return dataset


if __name__ == "__main__":
    main()
