from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Tuple, Union

import attr
import h5py as h5
import numpy as np
import pandas as pd
from nptyping import NDArray, UInt8
from scipy.interpolate import make_interp_spline
from scipy.signal import argrelmax, argrelmin
from sklearn.cluster import MiniBatchKMeans as KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import davies_bouldin_score

from vizpike.pipeline.dataset import HdfResource, derived_data_path
from vizpike.pipeline.detection import TimestampDtype


@attr.s(auto_attribs=True)
class Clustering:
    n_clusters: int
    labels: NDArray[(Any,), UInt8]
    scores: Dict[str, float]


@attr.s(auto_attribs=True)
class Sorting(Clustering):
    features: pd.DataFrame = None
    feature_projections: NDArray[(Any, Any), float] = None


class Featurizer(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, waveforms: NDArray[(Any, Any), float]) -> pd.DataFrame:
        pass


class Projector(metaclass=ABCMeta):
    @abstractmethod
    def __call__(
        self, features: NDArray[(Any, Any), float]
    ) -> NDArray[(Any, Any), float]:
        pass


class Clusterer(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, projections: NDArray[(Any, Any), float]) -> Clustering:
        pass


class SpikeSorting(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, waveforms: NDArray[(Any, Any), float]) -> Sorting:
        pass


@attr.s(auto_attribs=True)
class OmniplexReSorting(SpikeSorting):
    featurizer: Featurizer
    projector: Projector
    clusterer: Clusterer

    def __call__(self, waveforms):
        try:
            features = self.featurizer(waveforms)

            good_features = features.dropna()
            good_index = good_features.index

            projections = self.projector(good_features.values)
            clustering = self.clusterer(projections)

            sorting = Sorting(
                n_clusters=clustering.n_clusters,
                labels=self.include_unsorted(
                    clustering.labels, good_index, fill=0, rows=len(features)
                ),
                scores=clustering.scores,
                features=features,
                feature_projections=self.include_unsorted(
                    projections, good_index, fill=np.nan, rows=len(features)
                ),
            )
        except ValueError:
            sorting = Sorting(
                n_clusters=0,
                labels=np.zeros(len(waveforms), dtype=np.uint8),
                scores={},
            )
        finally:
            return sorting

    @staticmethod
    def include_unsorted(
        data: NDArray, good_index: pd.Index, *, fill: Union[int, float], rows: int,
    ) -> NDArray:
        full_shape = (rows,) + data.shape[1:]
        full_data = np.full(full_shape, fill, dtype=data.dtype)
        full_data[good_index] = data
        return full_data


class WaveformSamples(Featurizer):
    def __call__(self, waveforms):
        m, n = waveforms.shape
        features = pd.DataFrame(index=np.arange(m))
        features[[f"wf_{i}" for i in np.arange(n)]] = waveforms
        return features


@attr.s(auto_attribs=True)
class OmniplexFeatures(Featurizer):
    upsampling_doubles: int
    sampling_rate_in_hz: float
    samples_before_align: int

    def __call__(self, waveforms_in_mv):
        no_waveforms = len(waveforms_in_mv)

        up_ticks, up_waveforms_in_mv = self.upsampled_waveforms(waveforms_in_mv)
        align_skip = np.argmin(np.abs(up_ticks - self.samples_before_align))
        all_wfs = np.arange(no_waveforms)

        is_local_min, is_local_max = self.local_extrema_masks(up_waveforms_in_mv)
        is_valley, is_peak = self.valley_peak_masks(
            up_waveforms_in_mv, is_local_min, is_local_max
        )
        valley_ticks, peak_ticks = self.valley_peak_ticks(
            align_skip, is_peak, is_valley
        )

        valley_in_mv = up_waveforms_in_mv[all_wfs, valley_ticks]
        peak_in_mv = up_waveforms_in_mv[all_wfs, peak_ticks]
        valley_fwhm, peak_fwhm = self.valley_peak_fwhm(
            up_waveforms_in_mv, up_ticks, valley_in_mv, peak_in_mv
        )

        features = pd.DataFrame(index=np.arange(no_waveforms))

        features["valley_count"] = np.sum(is_valley, axis=1)
        features["peak_count"] = np.sum(is_peak, axis=1)

        has_valley, has_peak = (
            np.any(is_valley, axis=1),
            np.any(is_peak, axis=1),
        )

        features.loc[has_valley, "valley_tick"] = up_ticks[valley_ticks[has_valley]]
        features.loc[has_peak, "peak_tick"] = up_ticks[peak_ticks[has_peak]]

        features.loc[has_valley, "valley_in_mv"] = valley_in_mv[has_valley]
        features.loc[has_peak, "peak_in_mv"] = peak_in_mv[has_peak]

        features.loc[has_valley, "valley_fwhm"] = valley_fwhm[has_valley]
        features.loc[has_peak, "peak_fwhm"] = peak_fwhm[has_peak]

        features["area"] = (
            np.sum(np.abs(up_waveforms_in_mv), axis=1) / self.sampling_rate_in_hz
        )
        features["energy"] = (
            np.sum(up_waveforms_in_mv ** 2, axis=1) / self.sampling_rate_in_hz
        )
        features["nl_energy"] = (
            features["energy"]
            - np.sum(up_waveforms_in_mv[:, :-2] * up_waveforms_in_mv[:, 2:], axis=1)
            / self.sampling_rate_in_hz
        )

        features["amplitude_in_mv"] = features["peak_in_mv"] - features["valley_in_mv"]
        features["extrema_delay_in_ticks"] = (
            features["peak_tick"] - features["valley_tick"]
        )

        return features

    @staticmethod
    def valley_peak_fwhm(
        up_waveforms_in_mv: NDArray[(Any, Any), float],
        up_ticks: NDArray[(Any,), float],
        valley_in_mv: NDArray[(Any,), float],
        peak_in_mv: NDArray[(Any,), float],
    ) -> Tuple[float, float]:
        below_half_valley = up_waveforms_in_mv <= 0.5 * valley_in_mv[:, np.newaxis]
        above_half_peak = up_waveforms_in_mv >= 0.5 * peak_in_mv[:, np.newaxis]

        valley_fwhm = np.take(up_ticks, np.sum(below_half_valley, axis=1), mode="clip")
        peak_fwhm = np.take(up_ticks, np.sum(above_half_peak, axis=1), mode="clip")

        return valley_fwhm, peak_fwhm

    @staticmethod
    def valley_peak_ticks(
        align_skip: int,
        is_peak: NDArray[(Any,), bool],
        is_valley: NDArray[(Any,), bool],
    ) -> Tuple[NDArray[(Any,), float], NDArray[(Any,), float]]:
        valley_ticks = np.argmax(is_valley[:, align_skip:], axis=1) + align_skip
        peak_ticks = np.argmax(is_peak[:, align_skip:], axis=1) + align_skip
        return valley_ticks, peak_ticks

    @staticmethod
    def valley_peak_masks(
        waveforms: NDArray[(Any, Any), float],
        is_local_min: NDArray[(Any,), bool],
        is_local_max: NDArray[(Any,), bool],
    ) -> Tuple[NDArray[(Any,), bool], NDArray[(Any,), bool]]:
        is_valley = is_local_min & (waveforms < 0)
        is_peak = is_local_max & (waveforms > 0)
        return is_valley, is_peak

    @staticmethod
    def local_extrema_masks(
        waveforms: NDArray[(Any, Any), float]
    ) -> Tuple[NDArray[(Any,), bool], NDArray[(Any,), bool]]:
        is_local_min = np.zeros(waveforms.shape, dtype=bool)
        is_local_max = np.zeros(waveforms.shape, dtype=bool)

        is_local_min[argrelmin(waveforms, axis=1)] = True
        is_local_max[argrelmax(waveforms, axis=1)] = True

        return is_local_min, is_local_max

    def upsampled_waveforms(
        self, waveforms_in_mv: NDArray[(Any, Any), float]
    ) -> Tuple[NDArray[(Any,), float], NDArray[(Any, Any), float]]:
        _, wf_samples = waveforms_in_mv.shape
        wf_samples_up = 2 ** self.upsampling_doubles * (wf_samples - 1) + 1

        ticks = np.arange(wf_samples)
        ticks_up = np.linspace(0, ticks[-1], wf_samples_up)

        wfs_up = make_interp_spline(ticks, waveforms_in_mv, axis=1, k=3)(ticks_up)

        return ticks_up, wfs_up


@attr.s(auto_attribs=True)
class PCAProjector(Projector):
    dims: int

    def __call__(self, waveforms):
        return PCA(self.dims, whiten=True).fit_transform(waveforms)


@attr.s(auto_attribs=True)
class KMeansScanner(Clusterer):
    max_clusters: int

    def __call__(self, projections):

        k_values = range(2, self.max_clusters + 1)

        kmeans = [KMeans(k) for k in k_values]
        clusterings = [km.fit_predict(projections) + 1 for km in kmeans]
        scores = [
            {
                "inertia": km.inertia_,
                "Davies-Bouldin": davies_bouldin_score(projections, clust),
                # "silhouette": silhouette_score(projections, clust),
                # "Calinski-Harabasz": calinski_harabasz_score(projections, clust),
            }
            for km, clust in zip(kmeans, clusterings)
        ]
        ranking_scores = np.array([s["Davies-Bouldin"] for s in scores])

        if np.all(np.diff(ranking_scores, axis=0) > 0):
            result = Clustering(
                n_clusters=1,
                labels=np.ones(len(projections), dtype=np.uint8),
                scores={},
            )
        else:
            best_i = int(np.argmin(ranking_scores))
            result = Clustering(
                n_clusters=k_values[best_i],
                labels=clusterings[best_i],
                scores=scores[best_i],
            )

        return result


def load_waveforms_dataset(
    input_resource: HdfResource, channel: int
) -> Tuple[NDArray[(Any,), TimestampDtype], NDArray[(Any, Any), float]]:
    with h5.File(derived_data_path(input_resource.hdf_path)) as h5_file:
        dataset = h5_file[input_resource.dataset_name(channel)]
        spikes = np.array(dataset)

    timestamps = spikes["timestamps"]
    waveforms = spikes["waveforms"]

    return timestamps, waveforms
