import re
from enum import Enum
from typing import Any, Callable, Dict, Iterable, Sequence, Tuple, Type, TypeVar

from pint import Quantity
from pint_pandas import PintArray

K = TypeVar("K")
P = TypeVar("P")
R = TypeVar("R")


def apply_strategy(strategies: Dict[K, Callable[..., R]], key: K, *args, **kwargs) -> R:
    try:
        strategy = strategies[key]
    except KeyError:
        raise ValueError(f"Unsupported strategy: {key}")

    return strategy(*args, **kwargs)


def regex_extract(pattern: str, text: str) -> Sequence[str]:
    return re.match(pattern, text).groups()


def strategy(cls: Type[Enum]) -> Type[Enum]:
    """Class decorator for Enum classes that allows binding a callable to each enumerated value. Use this to implement
    the strategy pattern on enumerations without using if-elif chains or explicit dictionaries.

    Examples
    --------
    >>> from enum import Enum
    >>> from utils import strategy
    >>> @strategy
    ... class TokenStrategy(Enum):
    ...     LETTER = "LETTER"
    ...     DIGIT = "DIGIT"
    ...     UNHANDLED = "UNHANDLED"
    ...
    >>> @TokenStrategy.DIGIT.handler
    ... def digit_token(x):
    ...     return f"{x} is a digit"
    ...
    >>> @TokenStrategy.LETTER.handler
    ... def letter_token(x):
    ...     return f"{x} is a letter"
    ...
    >>> TokenStrategy.LETTER("a")
    "a is letter"
    >>> TokenStrategy.DIGIT("a")
    "a is digit"
    >>> TokenStrategy.UNHANDLED("a")
    utils.UnhandledStrategy: No handler registered for strategy 'UNHANDLED'
    """

    cls.__callables = {}

    @property
    def handler(self) -> Callable[[Callable[..., R]], Callable[..., R]]:
        def decorator(method):
            cls.__callables[self] = method
            return method

        return decorator

    def __call__(self, *args, **kwargs):
        function = cls.__callables.get(self, None)
        if function is None:
            raise UnhandledStrategy(f"No handler registered for strategy {self!r}.")
        else:
            return function(*args, **kwargs)

    cls.handler = handler
    cls.__call__ = __call__

    return cls


def dict_like_enum(cls: Type[Enum]) -> Type[Enum]:
    """Decorates an enum class, adding ``keys()``, ``values()``, and ``items()`` methods to it.

    Examples
    --------
    >>> from utils import dict_like_enum
    >>> @dict_like_enum
    ... class Token(Enum):
    ...     LETTER = "A"
    ...     DIGIT = "1"
    ...     UNHANDLED = "?"
    ...
    >>> Token.keys()
    [<Token.LETTER: 'A'>, <Token.DIGIT: '1'>, <Token.UNKNOWN: '?'>]
    >>> Token.keys()
    ["A", "1", "?"]
    >>> list(Token.items())
    [(<Token.LETTER: 'A', "A">), (<Token.DIGIT: '1'>, "1"), (<Token.UNKNOWN: '?'>, "?")]
    """

    def keys(klass) -> Iterable[Type[Enum]]:
        return list(klass)

    def values(klass) -> Iterable[Any]:
        return [x.value for x in klass]

    def items(klass) -> Iterable[Tuple[Type[Enum], Any]]:
        return zip(klass.keys(), klass.values())

    cls.keys = classmethod(keys)
    cls.values = classmethod(values)
    cls.items = classmethod(items)

    return cls


class UnhandledStrategy(Exception):
    pass


def quantity_to_pint_array(quantity: Quantity) -> PintArray:
    return PintArray(quantity.magnitude, dtype=quantity.units)
