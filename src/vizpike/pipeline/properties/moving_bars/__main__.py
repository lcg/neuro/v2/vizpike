import atexit
import re
from itertools import chain, groupby
from multiprocessing import Lock, Pool, cpu_count
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, List, Optional, Tuple

import attr
import click
import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from nptyping import NDArray, UInt8
from pint import Quantity
from tqdm import tqdm

from vizpike.pipeline.dataset import (
    DATASET_ROOT,
    HdfResource,
    derived_data_path,
    fk_join,
    load_derived_entity,
    load_original_entity,
    load_protocols,
    original_data_path,
    units,
)
from vizpike.pipeline.properties import Hdf5DataSaver
from vizpike.pipeline.properties.moving_bars import (
    BackProjector,
    MovingBarStimulus,
    ReceptiveFieldEstimate,
    ReceptiveFieldLocator,
)
from vizpike.pipeline.sigproc import BackgroundActivityEstimator, spike_matrix
from vizpike.pipeline.typeann import Time
from vizpike.pipeline.utils import quantity_to_pint_array

_RF_RECORDING_TITLES = load_original_entity("recording").query(
    "protocol_key.str.contains('MOVING BARS')"
)["title"]
_hdf_lock = Lock()


@click.group()
def main():
    """Moving bars functional properties."""
    pass


@main.command(name="dvc")
@click.argument("recording_titles", type=str, nargs=-1)
def main_dvc(recording_titles: Tuple[str]):
    """Print DVC CLI commands for creating stages for all of the available metadata."""

    def job_key(j):
        return j.recording_title, j.output_hdf.hdf_path

    jobs = select_jobs(*(recording_titles or tuple(_RF_RECORDING_TITLES)))
    grouped_jobs = groupby(sorted(jobs, key=job_key), job_key)

    for (recording_title, output_path), job_group in grouped_jobs:
        constant_args = [
            f"-n {recording_title}_properties",
            "-d derived/metadata/property.csv",
            "-d derived/metadata/property/moving_bars.csv",
            "-d src/properties/moving_bars",
            f"-o {output_path.relative_to(DATASET_ROOT)}",
        ]
        dependency_args = list(
            set(
                [
                    dependency
                    for job in job_group
                    for dependency in [
                        f"-d {job.labels_hdf.hdf_path.relative_to(DATASET_ROOT)}",
                        f"-d {job.timestamps_hdf.hdf_path.relative_to(DATASET_ROOT)}",
                        f"-d {job.trials_hdf_path.relative_to(DATASET_ROOT)}",
                    ]
                ]
            )
        )
        command = " ".join(
            [
                "dvc run",
                " ".join(chain(constant_args, dependency_args)),
                f"PYTHONPATH=src python -m {__package__} run {recording_title}",
            ]
        )
        print(command)


@main.command(name="run")
@click.argument("recording_title", type=str)
@click.option("--workers", "-w", type=int, default=max(cpu_count() - 2, 1))
def main_run(recording_title, workers: int):
    """Estimate functional properties for all spike sorting instances of the indicated moving-bars-stimulus recording."""
    jobs = select_jobs(recording_title)

    with Pool(workers) as pool:
        job_results = pool.imap_unordered(run_job, jobs)

        for _ in tqdm(job_results, total=len(jobs), desc="Jobs"):
            pass


@main.command(name="test")
@click.option(
    "--back_projector",
    type=click.Choice(BackProjector.values()),
    default=BackProjector.PROFILE_SUM.value,
)
@click.option(
    "--background_estimator",
    type=click.Choice(BackgroundActivityEstimator.values()),
    default=BackgroundActivityEstimator.WEIGHTED_NON_TRIAL.value,
)
@click.option("--channel", type=int, default=47)
@click.option("--detection_type", type=str, default="type=OMNIPLEX")
@click.option("--kernel_window_in_ms", type=float, default=150)
@click.option("--max_latency_in_ms", type=float, default=200)
@click.option("--min_peak_disparity", type=float, default=0.15)
@click.option("--output", type=click.Path(dir_okay=False, writable=True), default=None)
@click.option("--resolution", type=int, default=512)
@click.option(
    "--recording_title",
    type=click.Choice(_RF_RECORDING_TITLES),
    default="rf-v206-he1800-hd2600-repeat",
)
@click.option("--response_threshold", type=float, default=0.75)
@click.option(
    "--sorting_type",
    type=str,
    default="type=OMNIPLEX_RE projection=PCA(3) clustering=KMEANS(SCAN) features=WAVEFORM_FEATURES",
)
def main_test(
    back_projector: str,
    background_estimator: str,
    channel: int,
    detection_type: str,
    kernel_window_in_ms: float,
    max_latency_in_ms: float,
    min_peak_disparity: float,
    output: Optional[Path],
    resolution: int,
    recording_title: str,
    response_threshold: float,
    sorting_type: str,
):
    """Estimate and plot receptive fields in the indicated recording's channel.

    Examples
    --------
    According to https://lcg.gitlab.io/pasad/dsc/journal/f8d10c1c21/lfc-paper/corner-cases.run.html, these are
    interesting cases (inner lists contain channel numbers):

        corner_cases = [
            (
                "rf-v206-he1800-hd2600-repeat",
                {
                    "sharp receptive fields, orientation-tuned multi-units": [29, 47],
                    "sharp receptive fields, no tuning detected with gratings (what lurks behind the salt-and-pepper?)": [
                        1,
                        3,
                        18,
                        26,
                        46,
                        57,
                        56,
                    ],
                    "sharp receptive fields, bad grating data": [21, 54, 61],
                    "visible receptive fields with untuned responses for gratings": [
                        2,
                        38,
                        39,
                        62,
                    ],
                    "sparse receptive field evidence, orientation-tuned": [5],
                    "sparse receptive field evidence, untuned gratings": [
                        6,
                        12,
                        20,
                        27,
                        45,
                        59,
                        60,
                    ],
                    "sparse receptive field evidence, bad grating data": [40, 52, 53],
                    "no receptive field (take a closer look), tuned gratings": [8, 10, 35],
                    "no receptive field (take a closer look), untuned gratings (possibly bad sorting)": [
                        11,
                        13,
                        16,
                        37,
                        48,
                        58,
                    ],
                    "no receptive field (take a closer look), untuned gratings": [
                        7,
                        9,
                        14,
                        15,
                        17,
                        19,
                        28,
                        30,
                        31,
                        32,
                        33,
                        34,
                        36,
                        41,
                        42,
                        43,
                        44,
                        49,
                        50,
                    ],
                    "no receptive field (take a closer look), bad grating data": [
                        4,
                        22,
                        23,
                        24,
                        25,
                        51,
                        55,
                        63,
                        64,
                    ],
                },
            )
        ]
    """

    back_projector = BackProjector(back_projector)
    background_estimator = BackgroundActivityEstimator(background_estimator)

    output_type = f"type=FIORANI resolution={resolution}"
    detection_group = f"phase=DETECTION/{detection_type}"
    sorting_group = f"{detection_group}/phase=SORTING/{sorting_type}/"
    output_group = f"{detection_group}/phase=ESTIMATION/{output_type}"

    if output is None:
        temp_file = NamedTemporaryFile(prefix=recording_title, suffix=".h5")
        atexit.register(lambda: temp_file.close())
        output_path = Path(temp_file.name)
    else:
        output_path = output

    Estimator(
        EstimationJob(
            recording_title=recording_title,
            channel=channel,
            resolution=resolution,
            timestamps_hdf=HdfResource(
                hdf_path=Path(
                    f"derived/data/V206/moving-bars-8/{recording_title}/detections.h5"
                ),
                hdf_group=detection_group,
                hdf_prefix="SPK",
            ),
            labels_hdf=HdfResource(
                hdf_path=Path(
                    f"derived/data/V206/moving-bars-8/{recording_title}/sortings.h5"
                ),
                hdf_group=sorting_group,
                hdf_prefix="UNITS",
            ),
            trials_hdf_path=Path(
                f"original/data/V206/moving-bars-8/{recording_title}.hdf/metadata.h5"
            ),
            output_hdf=HdfResource(
                hdf_path=output_path, hdf_group=output_group, hdf_prefix="RF",
            ),
            response_threshold=response_threshold,
            kernel_window_in_ms=kernel_window_in_ms,
            max_latency_in_ms=max_latency_in_ms,
            min_peak_disparity=min_peak_disparity,
            background_estimator=background_estimator,
            back_projector=back_projector,
        )
    ).run_and_plot()


@attr.s(auto_attribs=True)
class EstimationJob:
    recording_title: str
    channel: int
    timestamps_hdf: HdfResource
    labels_hdf: HdfResource
    trials_hdf_path: Path
    output_hdf: HdfResource
    background_estimator: BackgroundActivityEstimator
    back_projector: BackProjector.PROFILE_SUM
    resolution: int = 512
    response_threshold: float = 0.5
    min_peak_disparity: float = 0.5
    kernel_window_in_ms: float = 150
    max_latency_in_ms: float = 200
    latency_precision_in_ms: float = 5


class Estimator:
    def __init__(self, job: EstimationJob):
        self.recording_title = job.recording_title
        self.resolution = job.resolution
        self.background_estimator = job.background_estimator
        self.output_path = job.output_hdf.hdf_path
        self.output_group = (
            f"{job.output_hdf.hdf_group}/{job.output_hdf.hdf_prefix}{job.channel:02d}"
        )

        self.kernel_window = job.kernel_window_in_ms * units.ms

        self.recording = (
            load_original_entity("recording")
            .query(f"title == {self.recording_title!r}")
            .iloc[0]
        )
        self.timestamp_sampling = self.recording["timestamp_frequency"]

        protocols, conditions = load_protocols(units)
        self.protocol = protocols.loc[self.recording["protocol_key"]].to_dict()
        self.conditions = (
            conditions.loc[self.recording["protocol_key"]]
            .reset_index()
            .rename(columns={"code": "condition_id"})
            .set_index("condition_id")
        )
        stimulus = MovingBarStimulus.from_dataframes(
            self.protocol, self.conditions, units
        )
        self.time_sampling = (job.resolution / stimulus.duration).to("hertz")

        self.rf_estimator = ReceptiveFieldLocator(
            stimulus,
            units=units,
            resolution=job.resolution,
            gauss_window=self.kernel_window,
            response_threshold=job.response_threshold,
            back_projector=job.back_projector,
            max_latency=job.max_latency_in_ms * units.ms,
            min_peak_disparity=job.min_peak_disparity,
            latency_precision=job.latency_precision_in_ms * units.ms,
        )

        self.spike_times = self._load_spikes_dataset(job.timestamps_hdf, job.channel)
        self.labels = self._load_sorting_dataset(job.labels_hdf, channel=job.channel)
        self.trials = self._load_trials_dataset(job.trials_hdf_path)

    @property
    def sampling_rate(self) -> Quantity:
        return self.recording["timestamp_frequency"]

    def run(self) -> List[Tuple[str, ReceptiveFieldEstimate]]:
        unit_labels_and_masks = [
            ("Multi-unit", Ellipsis),
            ("Multi-unit without unsorted", self.labels > 0),
        ] + [
            (
                f"Single-unit {chr(ord('a') + l - 1)}" if l > 0 else "Unsorted",
                self.labels == l,
            )
            for l in np.unique(self.labels)
        ]

        unit_estimates = [
            (unit_label, self._unit_estimate(mask))
            for unit_label, mask in tqdm(
                unit_labels_and_masks,
                desc=f"Estimating RFs for {self.recording_title}",
            )
        ]
        self._save_all_estimates(unit_estimates)

        return unit_estimates

    def run_and_plot(self) -> List[Tuple[str, ReceptiveFieldEstimate]]:
        results = self.run()

        # plt.switch_backend("TKAgg")
        plt.style.use("ggplot")
        units.setup_matplotlib()

        for unit_label, estimate in results:
            plt.figure()
            self._plot_estimate_response_map(estimate, unit_label)

            plt.figure()
            self._plot_estimate_rf(estimate, unit_label)

        plt.show()

        return results

    def _load_trials_dataset(self, hdf_path: Path):
        with h5.File(DATASET_ROOT / hdf_path) as metadata_file:
            trials_array = np.array(metadata_file["trials"])

        trials = pd.DataFrame.from_records(trials_array).rename(
            columns={"condition": "condition_id"}
        )

        trials["rank"] = trials.groupby("condition_id")[["start"]].rank()
        trials["start"] = quantity_to_pint_array(
            trials["start"].values / self.timestamp_sampling
        )
        trials["stop"] = quantity_to_pint_array(
            trials["stop"].values / self.timestamp_sampling
        )
        trials["duration"] = quantity_to_pint_array(
            trials["duration"].values / self.timestamp_sampling
        )

        return trials.convert_dtypes()

    def _load_sorting_dataset(
        self, hdf: HdfResource, channel: int
    ) -> NDArray[(Any,), UInt8]:
        dataset_name = f"{hdf.hdf_group}/{hdf.hdf_prefix}{channel:02d}/labels"
        with h5.File(DATASET_ROOT / hdf.hdf_path) as sorting_file:
            labels = np.array(sorting_file[dataset_name])
        return labels

    def _load_spikes_dataset(self, hdf: HdfResource, channel: int) -> Time:
        with h5.File(DATASET_ROOT / hdf.hdf_path) as detection_file:
            timestamps = np.array(
                detection_file[f"{hdf.hdf_group}/{hdf.hdf_prefix}{channel:02d}"][
                    "timestamps"
                ]
            )
        spike_times = timestamps / self.timestamp_sampling
        return spike_times

    def _unit_estimate(self, mask: NDArray[(Any,), bool]) -> ReceptiveFieldEstimate:
        _, trial_spikes = spike_matrix(
            self.spike_times[mask],
            self.trials,
            self.conditions,
            points=self.resolution,
            where="condition_is_stimulus & ~condition_is_neutral",
            group_by="condition_direction",
        )
        base_rate = self.background_estimator(
            self.spike_times[mask],
            self.trials,
            self.conditions,
            self.kernel_window,
            time_sampling=self.time_sampling,
        )
        estimate = self.rf_estimator.estimate(trial_spikes, baseline=base_rate)
        return estimate

    def _save_all_estimates(
        self, rf_estimates: List[Tuple[str, ReceptiveFieldEstimate]]
    ):
        rf_estimates_with_progress = tqdm(
            rf_estimates, desc=f"Recording RFs for {self.recording_title}"
        )

        self.output_path.parent.mkdir(parents=True, exist_ok=True)
        with _hdf_lock, h5.File(self.output_path, "a") as h5_file:
            for unit_name, unit_estimate in rf_estimates_with_progress:
                estimate_group = "%s/%s" % (
                    self.output_group,
                    re.sub(r"[\s-]", "_", unit_name.upper()),
                )
                estimate_saver = Hdf5DataSaver(h5_file.create_group(estimate_group))

                estimate_saver.save_attribute(
                    "response_cutoff", unit_estimate.response_cutoff
                )
                estimate_saver.save_attribute(
                    "base_rate", unit_estimate.base_rate, unit="hertz"
                )

                estimate_saver.save_dataset(
                    "response_map", unit_estimate.response_map, compressed=True,
                )

                if unit_estimate.was_located:
                    scalar_properties = unit_estimate.properties.drop(
                        columns="contour polar_contour polargram".split()
                    ).pint.dequantify()

                    estimate_saver.save_attribute(
                        "is_ambiguous", unit_estimate.is_ambiguous
                    )
                    estimate_saver.save_attribute(
                        "latency", unit_estimate.latency, unit="ms"
                    )

                    estimate_saver.save_dataset(
                        "active_responses", unit_estimate.active_responses
                    )
                    estimate_saver.save_dataset("responses", unit_estimate.responses)
                    estimate_saver.save_dataset(
                        "spike_counts", unit_estimate.spike_counts
                    )
                    estimate_saver.save_dataset(
                        "properties", scalar_properties.to_records(index=False)
                    )
                    estimate_saver.save_dataset(
                        "firing_rates", unit_estimate.firing_rates, unit="hertz"
                    )
                    estimate_saver.save_dataset(
                        "mean_rates", unit_estimate.mean_rates, unit="hertz"
                    )
                    estimate_saver.save_dataset(
                        "rate_variances", unit_estimate.rate_variances, unit="hertz**2",
                    )

                    polar_contours = np.stack(
                        unit_estimate.properties["polar_contour"].values, axis=0
                    )
                    estimate_saver.save_dataset(
                        "polar_contours", polar_contours, unit="deg"
                    )

                    polargrams = unit_estimate.properties[
                        "polargram"
                    ].values.quantity.magnitude.astype(float)
                    estimate_saver.save_dataset("polargram", polargrams)

                    contours = unit_estimate.properties["contour"].values
                    contours_x = [row[:, 0] for row in contours]
                    contours_y = [row[:, 1] for row in contours]
                    estimate_saver.save_dataset(
                        "contours_x", contours_x, unit="deg", vlen_dtype=float
                    )
                    estimate_saver.save_dataset(
                        "contours_y", contours_y, unit="deg", vlen_dtype=float
                    )

    def _plot_estimate_response_map(
        self, estimate: ReceptiveFieldEstimate, unit_label: str
    ):
        plt.imshow(estimate.response_map, cmap="inferno")
        plt.title(
            f"{unit_label}\nResponse map ({self.rf_estimator.back_projector.value})"
        )
        plt.colorbar()

    def _plot_estimate_rf(self, estimate: ReceptiveFieldEstimate, unit_label: str):
        if estimate.was_located:
            n = len(estimate.properties)
            for i in range(n):
                rf_center = estimate.properties[["center_x", "center_y"]].values[i]
                rf_contour = estimate.properties["polar_contour"].values[i]

                if not estimate.is_ambiguous and i == 0:
                    color = "black"
                    label = "\n".join(
                        [
                            rf"Response_peak: ${estimate.response_peak:.2f}~\sigma$",
                            rf"Latency: ${estimate.latency.to('ms'):.2L}$",
                        ]
                    )
                else:
                    color = f"C{i}"
                    label = None

                plt.scatter(rf_center[0], rf_center[1], color=color, label=label)
                curve = np.vstack((rf_contour, rf_contour[0])).T
                plt.plot(curve[0], curve[1], color=color)

            plt.xlim(
                -0.5 * self.protocol["amplitude"], 0.5 * self.protocol["amplitude"],
            )
            plt.ylim(
                -0.5 * self.protocol["amplitude"], 0.5 * self.protocol["amplitude"],
            )
            plt.title(f"{unit_label}\nRF center and contour")
            plt.legend()


def select_jobs(*recording_titles: str) -> List[EstimationJob]:
    metadata = select_metadata(*recording_titles)
    jobs = [
        job_for(row, channel)
        for _, row in metadata.iterrows()
        for channel in range(1, row["recording_channels"] + 1)
    ]
    return jobs


def select_metadata(*recording_titles: str) -> pd.DataFrame:
    estimations = (
        load_derived_entity("property", subtypes=True)
        .query("type == 'MOVING_BARS'")
        .dropna(axis=1)
    )
    new_sortings = load_derived_entity("new_sorting", subtypes=True)
    detections = load_derived_entity("detection")

    sources = load_original_entity("source")
    recordings = load_original_entity("recording")

    full_df = fk_join(
        estimations, new_sortings, detections, sources, recordings
    ).convert_dtypes()

    if recording_titles:
        final_df = full_df[full_df["recording_title"].isin(recording_titles)]
    else:
        final_df = full_df

    return final_df


def job_for(row: pd.Series, channel: int) -> EstimationJob:
    recording_title = row["recording_title"]
    detection_hdf_path = derived_data_path(row["detection_hdf_path"])
    detection_hdf_group = row["detection_hdf_group"]
    detection_hdf_prefix = row["detection_hdf_prefix"]
    sorting_hdf_path = derived_data_path(row["new_sorting_hdf_path"])
    sorting_hdf_group = row["new_sorting_hdf_group"]
    sorting_hdf_prefix = row["new_sorting_hdf_prefix"]

    hdf_path = derived_data_path(row["hdf_path"])
    hdf_group = row["hdf_group"]
    hdf_prefix = row["hdf_prefix"]
    resolution = row["resolution"]
    response_threshold = row["response_threshold"]
    kernel_window_in_ms = row["kernel_window_in_ms"]
    max_latency_in_ms = row["max_latency_in_ms"]
    min_peak_disparity = row["min_peak_disparity"]
    latency_precision_in_ms = row["latency_precision_in_ms"]
    background_estimator = BackgroundActivityEstimator(row["background_estimator"])
    back_projector = BackProjector(row["back_projector"])
    trials_hdf_path = original_data_path(row["source_hdf_path"]).parent / "metadata.h5"

    job = EstimationJob(
        recording_title=recording_title,
        channel=channel,
        resolution=resolution,
        timestamps_hdf=HdfResource(
            hdf_path=detection_hdf_path,
            hdf_group=detection_hdf_group,
            hdf_prefix=detection_hdf_prefix,
        ),
        labels_hdf=HdfResource(
            hdf_path=sorting_hdf_path,
            hdf_group=sorting_hdf_group,
            hdf_prefix=sorting_hdf_prefix,
        ),
        trials_hdf_path=trials_hdf_path,
        output_hdf=HdfResource(
            hdf_path=hdf_path, hdf_group=hdf_group, hdf_prefix=hdf_prefix,
        ),
        response_threshold=response_threshold,
        kernel_window_in_ms=kernel_window_in_ms,
        max_latency_in_ms=max_latency_in_ms,
        min_peak_disparity=min_peak_disparity,
        background_estimator=background_estimator,
        back_projector=back_projector,
        latency_precision_in_ms=latency_precision_in_ms,
    )

    return job


def run_job(job: EstimationJob):
    Estimator(job).run()


if __name__ == "__main__":
    main()
