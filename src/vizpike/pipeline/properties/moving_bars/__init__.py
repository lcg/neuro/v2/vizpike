from enum import Enum
from functools import lru_cache
from typing import Any, List, Optional, Tuple, Union

import attr
import numpy as np
import pandas as pd
import scipy.ndimage
import skimage.measure
from nptyping import NDArray, Number
from pint import Quantity, UnitRegistry
from scipy import integrate
from scipy.ndimage import convolve1d
from scipy.optimize import minimize_scalar
from scipy.stats import zscore
from skimage.measure._regionprops import RegionProperties
from skimage.transform import iradon

from vizpike.pipeline.sigproc import gaussian_kernel
from vizpike.pipeline.typeann import (
    FiringRate,
    FunctionalProperties,
    ResponseMap,
    SpikeTrain,
    Time,
    TimeFrequency,
    VisualFieldPoint,
)
from vizpike.pipeline.utils import dict_like_enum, quantity_to_pint_array, strategy


@dict_like_enum
@strategy
class BackProjector(Enum):
    PROFILE_SUM = "PROFILE_SUM"
    INVERSE_RADON = "INVERSE_RADON"


@attr.s(auto_attribs=True)
class MovingBarStimulus:
    excursion: Quantity
    length: Quantity
    speed: Quantity
    directions: Quantity
    trials: int
    units: UnitRegistry

    @classmethod
    def from_dataframes(
        cls, protocol: pd.DataFrame, conditions: pd.DataFrame, units: UnitRegistry,
    ) -> "MovingBarStimulus":
        return MovingBarStimulus(
            excursion=protocol["amplitude"],
            length=protocol["bar_length"],
            speed=protocol["bar_speed"],
            units=units,
            directions=conditions.query("is_stimulus & ~is_neutral")[
                "direction"
            ].values.quantity,
            trials=protocol["trials_per_condition"],
        )

    @property
    def duration(self):
        """Stimulus duration, computed from excursion and speed.

        Returns
        -------
        duration : :class:`pint.Quantity`

        """
        return self.excursion / self.speed


@attr.s(auto_attribs=True)
class ReceptiveFieldEstimate:
    spike_counts: NDArray[(Any, Any), int]
    response_map: NDArray[(Any, Any), float]
    responses: NDArray[(Any,), float]
    active_responses: NDArray[(Any,), float]
    response_peak: float
    response_cutoff: float
    base_rate: Time
    firing_rates: Union[NDArray[(Any,), float], Quantity]
    mean_rates: Union[NDArray[(Any,), float], Quantity]
    rate_variances: Union[NDArray[(Any,), float], Quantity]
    was_located: bool
    is_ambiguous: bool
    latency: Optional[Quantity] = None
    properties: Optional[FunctionalProperties] = None
    center: Optional[Union[Quantity, NDArray[(2,), float]]] = None
    contour: Optional[Union[Quantity, NDArray[(Any, 2), float]]] = None
    polar_contour: Optional[Union[Quantity, NDArray[(Any, 2), float]]] = None

    @classmethod
    def ambiguous(
        cls,
        *,
        spike_counts: NDArray[(Any, Any), int],
        response_map: ResponseMap,
        response_cutoff: float,
        active_responses: NDArray[(Any,), float],
        responses: NDArray[(Any,), float],
        base_rate: TimeFrequency,
        firing_rates: NDArray[(Any, Any), float],
        latency: Time,
        properties: FunctionalProperties,
    ) -> "ReceptiveFieldEstimate":
        return ReceptiveFieldEstimate(
            was_located=True,
            is_ambiguous=True,
            spike_counts=spike_counts,
            response_map=response_map,
            response_peak=np.max(response_map),
            response_cutoff=response_cutoff,
            base_rate=base_rate,
            firing_rates=firing_rates,
            responses=responses,
            active_responses=active_responses,
            mean_rates=np.mean(firing_rates, axis=1),
            rate_variances=np.var(firing_rates, axis=1),
            properties=properties,
            latency=latency,
        )

    @classmethod
    def empty(
        cls,
        *,
        spike_counts: NDArray[(Any, Any), int],
        response_map: ResponseMap,
        response_cutoff: float,
        active_responses: NDArray[(Any,), float],
        responses: NDArray[(Any,), float],
        base_rate: TimeFrequency,
        firing_rates: NDArray[(Any, Any), float],
    ) -> "ReceptiveFieldEstimate":
        return ReceptiveFieldEstimate(
            was_located=False,
            is_ambiguous=False,
            spike_counts=spike_counts,
            response_map=response_map,
            response_peak=np.max(response_map),
            response_cutoff=response_cutoff,
            responses=responses,
            active_responses=active_responses,
            base_rate=base_rate,
            firing_rates=firing_rates,
            mean_rates=np.mean(firing_rates, axis=1),
            rate_variances=np.var(firing_rates, axis=1),
        )

    @classmethod
    def non_ambiguous(
        cls,
        *,
        spike_counts: NDArray[(Any, Any), int],
        response_map: ResponseMap,
        response_cutoff: float,
        active_responses: NDArray[(Any,), float],
        responses: NDArray[(Any,), float],
        base_rate: TimeFrequency,
        firing_rates: NDArray[(Any, Any), float],
        latency: Time,
        properties: FunctionalProperties,
    ) -> "ReceptiveFieldEstimate":
        return ReceptiveFieldEstimate(
            was_located=True,
            is_ambiguous=False,
            spike_counts=spike_counts,
            response_map=response_map,
            response_peak=np.max(response_map),
            response_cutoff=response_cutoff,
            responses=responses,
            active_responses=active_responses,
            base_rate=base_rate,
            firing_rates=firing_rates,
            mean_rates=np.mean(firing_rates, axis=1),
            rate_variances=np.var(firing_rates, axis=1),
            properties=properties,
            latency=latency,
            center=np.hstack(
                [properties.loc[0, "center_x"], properties.loc[0, "center_y"]]
            ),
            contour=properties.loc[0, "contour"],
            polar_contour=properties.loc[0, "polar_contour"],
        )


class ReceptiveFieldLocator:
    r"""Estimates a Receptive Field upon instantiation, following the algorithm by :cite:`LFC:2014:Fiorani:RF`.

    A first estimate is produced by considering zero latency and baseline activity equal to the mean of all activity.
    Further iterations refine the latency value and use previous RF estimates to separate foreground from background
    activity, increasing the precision of the RF estimate. The iteration process is controlled by the ``max_iterations``
    parameter, and driven by the :class:`fiorani_rf.Estimate.LatencyEstimator` class. When
    initialization is finished, a number of attributes holds the latest values of estimated quantities.

    Parameters
    ----------
    stimulus : :class:`fiorani_rf.MovingBarStimulus`
        Model of stimulus presented to test subject.

    sampling : :class:`fiorani_rf.sampling`
        The sampling specification. This affects the processing time and the resolution of the generated response map.

    spike_trains : dict
        A dictionary that maps stimulus directions to lists of spike histograms (one histogram per trial). The set of
        keys should correspond to ``stimulus.directions.value`` and the corresponding values should be
        ``(M,N)``-shaped ``np.ndarray`` instances, with ``M = sampling.samples`` and ``N = stimulus.trials``.

    gauss_window : :class:`pint.Quantity`, optional
        Aperture of the Gaussian kernel used for estimating the firing rate for each stimulus direction. This
        corresponds to :math:`3\sigma`. The unit of measure should be a temporal unit.

    response_threshold : float (between 0 and 1), optional
        Percentage of normalized response peak height above which responses should be considered as belonging to the
        estimated receptive field.

    max_iterations : int, optional
        Maximum number of iterations for latency correction.

    Attributes
    ----------
    bg_activity : dict of float to ``(M,N)`` :class:`np.ndarray`
        A dictionary that maps each stimulus direction magnitude to a list of ``M`` histograms of background
        activity, where ``M = stimulus.trials`` and ``N = sampling.samples``. These histograms are complementary to
        ``fg_activity`` in relation to ``response_vectors``.

    fg_activity : dict of float to ``(M,N)`` :class:`np.ndarray`
        A dictionary that maps each stimulus direction magnitude to a list of ``M`` histograms of foreground
        activity, where ``M = stimulus.trials`` and ``N = sampling.samples``. These histograms are complementary to
        ``bg_activity`` in relation to ``response_vectors``.

    latency : :class:`pint.Quantity`
        Latency in receptive field firing. The unit of measure is a temporal unit, and the magnitude is always
        non-negative.

    response_curves : dict of float to ``(N,)`` :class:`np.ndarray`
        A dictionary that maps stimulus direction magnitudes to normalized response curves, where
        ``N = sampling.samples``.

    response_map : (N,N) :class:`np.ndarray` of float
        A response map, produced by back-projecting all response curves (after latency correction) along their
        corresponding stimulus excursion directions, with ``N = sampling.samples``. This response map is returned
        in (r, c) image coordinates, that is, the center of the visual field is at (N/2, N/2), and its top left
        corner is at (0, 0).

    response_peak : float
        Maximum value of response map.

    rf_center : :class:`pint.Quantity`
        Receptive field center coordinates. The unit of measure is an angular unit, and the magnitude is a
        ``(2,)``-shaped :class:`np.ndarray`.

    rf_contour : :class:`pint.Quantity`
        Receptive field contour coordinates. The unit of measure is an angular unit, and the magnitude is a
        ``(K,2)``-shaped :class:`np.ndarray`, where ``K`` is the number of contour points.
    """

    def __init__(
        self,
        stimulus: MovingBarStimulus,
        resolution: int,
        units: UnitRegistry,
        gauss_window: Time = None,
        response_threshold=0.5,
        min_peak_disparity=0.5,
        max_latency: Time = None,
        back_projector: BackProjector = BackProjector.PROFILE_SUM,
        latency_precision: Time = None,
    ):
        self._stimulus = stimulus

        self.units = units
        self.directions = stimulus.directions
        self.excursion = stimulus.excursion
        self.gaussian_window = gauss_window or 150 * units.ms
        self.max_latency = max_latency or 200 * units.ms
        self.latency_precision = latency_precision or 5 * units.ms
        self.min_response_disparity = min_peak_disparity
        self.samples = resolution
        self.sampling_rate = self.samples / stimulus.duration
        self.spatial_rate = self.samples / stimulus.excursion
        self.back_projector = back_projector
        self.response_threshold = response_threshold

        self.motion_vectors = np.vstack(
            [np.cos(self.directions), np.sin(self.directions)]
        ).T

    def estimate(
        self, spike_trains: SpikeTrain, baseline: TimeFrequency = None
    ) -> ReceptiveFieldEstimate:
        baseline = baseline or (0 * self.units.hertz)
        mean_spike_trains = np.mean(spike_trains, axis=1)
        spike_counts = np.sum(spike_trains, axis=2)

        latency = self._estimate_latency(mean_spike_trains, baseline)
        (firing_rates, response_map, response_peak,) = self._response_rate_map_and_peak(
            mean_spike_trains, latency, baseline
        )

        responses = integrate.trapz(firing_rates, dx=1 / self.sampling_rate, axis=1)
        active_responses = integrate.trapz(
            firing_rates - baseline, dx=1 / self.sampling_rate, axis=1
        )

        response_cutoff = response_peak * self.response_threshold
        try:
            rf_properties = self._locate_rf(firing_rates, response_map, response_cutoff)

            rf = ReceptiveFieldEstimate.non_ambiguous(
                spike_counts=spike_counts,
                response_map=response_map,
                response_cutoff=self.response_threshold * response_peak,
                responses=responses,
                active_responses=active_responses,
                base_rate=baseline,
                firing_rates=firing_rates,
                latency=latency,
                properties=rf_properties,
            )
        except AmbiguousReceptiveField as exc:
            rf = ReceptiveFieldEstimate.ambiguous(
                spike_counts=spike_counts,
                response_map=response_map,
                response_cutoff=self.response_threshold * response_peak,
                responses=responses,
                active_responses=active_responses,
                base_rate=baseline,
                firing_rates=firing_rates,
                latency=latency,
                properties=exc.properties,
            )
        except NoReceptiveField:
            rf = ReceptiveFieldEstimate.empty(
                spike_counts=spike_counts,
                response_map=response_map,
                response_cutoff=response_cutoff,
                responses=responses,
                active_responses=active_responses,
                base_rate=baseline,
                firing_rates=firing_rates,
            )

        return rf

    def _estimate_latency(
        self, mean_spike_trains: NDArray[(Any, Any), float], baseline: TimeFrequency,
    ) -> Time:
        def offset_cost(offset):
            _, _, response_peak = self._response_rate_map_and_peak(
                mean_spike_trains, offset / self.sampling_rate, baseline
            )
            return -response_peak

        max_offset = int(self.max_latency * self.sampling_rate)
        offset_tolerance = float(0.5 * self.latency_precision * self.sampling_rate)
        optimization = minimize_scalar(
            offset_cost,
            bounds=(0, max_offset),
            method="bounded",
            options=dict(xatol=offset_tolerance),
        )
        latency = optimization.x / self.sampling_rate

        return latency

    def _response_rate_map_and_peak(
        self, mean_spike_trains: SpikeTrain, latency: Time, baseline: TimeFrequency,
    ) -> Tuple[FiringRate, ResponseMap, float]:
        hz = self.units.hertz

        firing_rates = np.clip(
            convolve1d(mean_spike_trains, self._gaussian_kernel(latency).magnitude) * hz
            - baseline,
            0 * hz,
            np.inf * hz,
        )
        response_map = self.back_projector(
            zscore(firing_rates.magnitude, axis=1), self.directions
        )
        response_peak = np.max(response_map)

        return firing_rates, response_map, response_peak

    def _locate_rf(
        self,
        firing_rates: FiringRate,
        response_map: ResponseMap,
        response_cutoff: float,
    ) -> pd.DataFrame:
        (
            candidate_count,
            candidate_labels,
            candidate_props,
        ) = self._candidate_rf_regions(response_map, response_cutoff)

        if candidate_count >= 1:
            candidate_properties = self._candidate_rf_properties(
                candidate_labels, candidate_props, firing_rates, response_map
            )

            top2_responses = candidate_properties["total_response"].values[:2]
            top2_responses_disparity = (
                1 - np.diff(top2_responses[::-1]) / top2_responses[0]
            )

            if (candidate_count > 1) and (
                top2_responses_disparity < self.min_response_disparity
            ):
                raise AmbiguousReceptiveField(candidate_properties)
            else:
                return candidate_properties
        else:
            raise NoReceptiveField()

    def _candidate_rf_properties(
        self,
        candidate_labels: NDArray[(Any, Any), int],
        candidate_props: List[RegionProperties],
        firing_rates: FiringRate,
        response_map: ResponseMap,
    ) -> pd.DataFrame:
        df = pd.DataFrame(index=np.arange(len(candidate_props)))

        pure_number = self.units.Unit("dimensionless")
        scalar_properties = [
            ("area", [p.area for p in candidate_props], 1 / self.spatial_rate ** 2,),
            (
                "convex_area",
                [p.convex_area for p in candidate_props],
                1 / self.spatial_rate ** 2,
            ),
            (
                "perimeter",
                [p.perimeter for p in candidate_props],
                1 / self.spatial_rate,
            ),
            (
                "equivalent_diameter",
                [p.equivalent_diameter for p in candidate_props],
                1 / self.spatial_rate,
            ),
            (
                "major_axis",
                [p.major_axis_length for p in candidate_props],
                1 / self.spatial_rate,
            ),
            (
                "minor_axis",
                [p.minor_axis_length for p in candidate_props],
                1 / self.spatial_rate,
            ),
            ("euler_number", [p.euler_number for p in candidate_props], pure_number,),
            ("peak_response", [p.max_intensity for p in candidate_props], pure_number,),
            (
                "mean_response",
                [p.mean_intensity for p in candidate_props],
                pure_number,
            ),
            (
                "total_response",
                [
                    np.sum(response_map[p.coords[:, 0], p.coords[:, 1]])
                    for p in candidate_props
                ],
                pure_number,
            ),
        ]

        centers = self._pixel2deg(np.array([p.centroid for p in candidate_props]))
        df["center_x"] = quantity_to_pint_array(centers[:, 0])
        df["center_y"] = quantity_to_pint_array(centers[:, 1])

        for name, values, unit in scalar_properties:
            df[name] = quantity_to_pint_array(np.array(values) * unit)

        df["aspect_ratio"] = df["minor_axis"] / df["major_axis"]
        df["contour"] = [
            self._pixel2deg(
                skimage.measure.find_contours(candidate_labels == i + 1, 0.5)[0]
            )
            for i, p in enumerate(candidate_props)
        ]
        df["polar_contour"] = [
            self._simplified_rf_contour(centers[i], df.loc[i, "contour"])
            for i in df.index
        ]

        df["polargram"] = quantity_to_pint_array(
            np.stack(
                [
                    self._polargram(firing_rates, df.loc[i, "polar_contour"], i)
                    for i in range(len(df))
                ],
                axis=0,
            )
        )

        sorted_df = df.sort_values("total_response", ascending=False).reset_index(
            drop=True
        )

        return sorted_df

    def _candidate_rf_regions(
        self, response_map: NDArray[(Any, Any), float], response_minimum: float
    ) -> Tuple[int, NDArray[(Any, Any), int], List[RegionProperties]]:
        labels, count = skimage.measure.label(
            response_map >= response_minimum, return_num=True
        )
        props = skimage.measure.regionprops(labels, intensity_image=response_map)
        return count, labels, props

    def _polargram(
        self, firing_rates: FiringRate, polar_contour: NDArray[(Any, Any), float], i
    ) -> NDArray[(Any, Any), float]:
        n = len(polar_contour)
        polar_projections = 0.5 * self.excursion + np.sum(
            polar_contour * self.motion_vectors, axis=-1
        )
        upper_integral_limits = (
            np.round(polar_projections * self.spatial_rate).astype(int).magnitude
        )
        lower_integral_limits = self.samples - np.roll(upper_integral_limits, n // 2)
        polargrams = np.hstack(
            [
                integrate.trapz(
                    firing_rates[i, lower:upper], dx=1 / self.sampling_rate,
                )
                / integrate.trapz(firing_rates[i], dx=1 / self.sampling_rate)
                for i, (lower, upper) in enumerate(
                    zip(lower_integral_limits, upper_integral_limits)
                )
            ]
        )
        return polargrams

    def _simplified_rf_contour(
        self, center: NDArray[(2,), float], contour: NDArray[(Any, 2), float]
    ) -> NDArray[(Any, 2), float]:
        contour_projections = np.dot(self.motion_vectors, (contour - center).T)
        indices = np.argmax(contour_projections, axis=1)
        polar_points = contour[indices]
        return polar_points

    @lru_cache()
    def _gaussian_kernel(self, latency: Quantity) -> NDArray[(Any,), float]:
        return gaussian_kernel(self.gaussian_window, self.sampling_rate, latency)

    def _pixel2deg(self, coords: NDArray[(Any, 2), Number]) -> VisualFieldPoint:
        """This function implements the following change of coordinates:
        
        |x|   (|0 1||r|             |1| )         (| 1|)
        |y| = (|1 0||c| - samples/2 |1| ) hadprod (|-1|) excursion/samples
        """
        fix_coords = self._stimulus.excursion * (coords[..., ::-1] / self.samples - 0.5)
        fix_coords[..., 1] *= -1
        return fix_coords


class NoReceptiveField(Exception):
    pass


class AmbiguousReceptiveField(Exception):
    def __init__(self, properties: pd.DataFrame):
        super().__init__()
        self.properties = properties


@BackProjector.PROFILE_SUM.handler
def _simple_response_map(
    responses: NDArray[(Any, Any), float], directions: NDArray[(Any,), float]
) -> ResponseMap:
    profile_maps = [
        _profile_response_map(response, orient)
        for orient, response in zip(directions, responses)
    ]
    simple_map = np.mean(profile_maps, axis=0)
    return simple_map


def _profile_response_map(response: FiringRate, orient: Quantity) -> ResponseMap:
    n = len(response)
    response_map = np.tile(response, (n, 1))
    rotated_response_map = scipy.ndimage.rotate(
        response_map, orient.to("deg").magnitude, reshape=False
    )
    return rotated_response_map


@BackProjector.INVERSE_RADON.handler
def _iradon_response_map(
    responses: NDArray[(Any, Any), float], directions: NDArray[(Any,), float]
) -> ResponseMap:
    iradon_map = iradon(responses.T, directions.to("deg").magnitude)
    return iradon_map
