import atexit
import re
from itertools import chain, groupby
from multiprocessing import Lock, Pool, cpu_count
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, Dict, Iterable, List, Optional, Tuple

import attr
import click
import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from nptyping import NDArray, UInt8
from tqdm import tqdm

from vizpike.pipeline.dataset import (
    DATASET_ROOT,
    HdfResource,
    derived_data_path,
    fk_join,
    load_derived_entity,
    load_original_entity,
    load_protocols,
    original_data_path,
    units,
)
from vizpike.pipeline.properties import Hdf5DataSaver
from vizpike.pipeline.properties.gratings import (
    GratingsProperties,
    GratingsPropertiesEstimator,
    GratingStimulus,
)
from vizpike.pipeline.sigproc import BackgroundActivityEstimator
from vizpike.pipeline.typeann import Time
from vizpike.pipeline.utils import quantity_to_pint_array

_GRATING_RECORDING_TITLES = load_original_entity("recording").query(
    "protocol_key.str.contains('GRATINGS')"
)["title"]
_hdf_lock = Lock()


@click.group()
def main():
    """Gratings functional properties."""
    pass


@main.command(name="dvc")
@click.argument("recording_titles", type=str, nargs=-1)
def main_dvc(recording_titles: Tuple[str]):
    """Print DVC CLI commands for creating stages for all of the available metadata."""

    def job_key(j):
        return j.recording_title, j.output_hdf.hdf_path

    jobs = select_jobs(*(recording_titles or tuple(_GRATING_RECORDING_TITLES)))
    grouped_jobs = groupby(sorted(jobs, key=job_key), job_key)

    for (recording_title, output_path), job_group in grouped_jobs:
        constant_args = [
            f"-n {recording_title}_properties",
            "-d derived/metadata/property.csv",
            "-d derived/metadata/property/gratings.csv",
            "-d src/properties/gratings",
            f"-o {output_path.relative_to(DATASET_ROOT)}",
        ]
        dependency_args = list(
            set(
                [
                    dependency
                    for job in job_group
                    for dependency in [
                        f"-d {job.labels_hdf.hdf_path.relative_to(DATASET_ROOT)}",
                        f"-d {job.timestamps_hdf.hdf_path.relative_to(DATASET_ROOT)}",
                        f"-d {job.trials_hdf_path.relative_to(DATASET_ROOT)}",
                    ]
                ]
            )
        )
        command = " ".join(
            [
                "dvc run",
                " ".join(chain(constant_args, dependency_args)),
                f"PYTHONPATH=src python -m {__package__} run {recording_title}",
            ]
        )
        print(command)


@main.command(name="run")
@click.argument("recording_title", type=str)
@click.option("--workers", "-w", type=int, default=max(cpu_count() - 2, 1))
def main_run(recording_title, workers: int):
    """Estimate functional properties for for all spike sorting instances of the indicated gratings-stimulus recording."""
    jobs = select_jobs(recording_title)

    with Pool(workers) as pool:
        job_results = pool.imap_unordered(run_job, jobs)

        for _ in tqdm(job_results, total=len(jobs), desc="Jobs"):
            pass


@main.command(name="test")
@click.option(
    "--background_estimator",
    type=click.Choice(BackgroundActivityEstimator.values()),
    default=BackgroundActivityEstimator.WEIGHTED_NON_TRIAL.value,
)
@click.option("--channel", type=int, default=47)
@click.option("--detection_type", type=str, default="type=OMNIPLEX")
@click.option("--kernel_window_in_ms", type=float, default=150)
@click.option("--output", type=click.Path(dir_okay=False, writable=True), default=None)
@click.option(
    "--recording_title",
    type=click.Choice(_GRATING_RECORDING_TITLES),
    default="grating-v206-he1800-hd2600",
)
@click.option(
    "--sorting_type",
    type=str,
    default="type=OMNIPLEX_RE projection=PCA(3) clustering=KMEANS(SCAN) features=WAVEFORM_FEATURES",
)
def main_test(
    background_estimator: str,
    channel: int,
    detection_type: str,
    kernel_window_in_ms: float,
    output: Optional[Path],
    recording_title: str,
    sorting_type: str,
):
    background_estimator = BackgroundActivityEstimator(background_estimator)

    output_type = f"type=GRATINGS"
    detection_group = f"phase=DETECTION/{detection_type}"
    sorting_group = f"{detection_group}/phase=SORTING/{sorting_type}/"
    output_group = f"{detection_group}/phase=ESTIMATION/{output_type}"

    if output is None:
        temp_file = NamedTemporaryFile(prefix=recording_title, suffix=".h5")
        atexit.register(lambda: temp_file.close())
        output_path = Path(temp_file.name)
    else:
        output_path = output

    Estimator(
        EstimationJob(
            recording_title=recording_title,
            channel=channel,
            timestamps_hdf=HdfResource(
                hdf_path=Path(
                    f"derived/data/V206/gratings-105/{recording_title}/detections.h5"
                ),
                hdf_group=detection_group,
                hdf_prefix="SPK",
            ),
            labels_hdf=HdfResource(
                hdf_path=Path(
                    f"derived/data/V206/gratings-105/{recording_title}/sortings.h5"
                ),
                hdf_group=sorting_group,
                hdf_prefix="UNITS",
            ),
            trials_hdf_path=Path(
                f"original/data/V206/gratings-105/{recording_title}.hdf/metadata.h5"
            ),
            output_hdf=HdfResource(
                hdf_path=Path(
                    "/home/pedro/Git/lab/lcg/neuro/v2/vizpike/dataset/derived/data/V206/gratings-105/grating-v206-he1100-hd1900/properties"
                ),
                hdf_group=output_group,
                hdf_prefix="RF",
            ),
            kernel_window_in_ms=kernel_window_in_ms,
            background_estimator=background_estimator,
        )
    ).run_and_save()


@attr.s(auto_attribs=True)
class EstimationJob:
    recording_title: str
    channel: int
    timestamps_hdf: HdfResource
    labels_hdf: HdfResource
    trials_hdf_path: Path
    output_hdf: HdfResource
    background_estimator: BackgroundActivityEstimator
    kernel_window_in_ms: float = 150
    sampling_rate_in_hz: float = 200


class Estimator:
    def __init__(self, job: EstimationJob):
        self.recording_title = job.recording_title
        self.background_estimator = job.background_estimator
        self.output_path = job.output_hdf.hdf_path
        self.output_group = (
            f"{job.output_hdf.hdf_group}/{job.output_hdf.hdf_prefix}{job.channel:02d}"
        )

        self.kernel_window = job.kernel_window_in_ms * units.ms
        self.spike_sampling = job.sampling_rate_in_hz * units.hertz

        self.recording = (
            load_original_entity("recording")
            .query(f"title == {self.recording_title!r}")
            .iloc[0]
        )
        self.timestamp_sampling = self.recording["timestamp_frequency"]

        protocols, conditions = load_protocols(False)
        self.protocol = protocols.loc[self.recording["protocol_key"]].to_dict()
        self.conditions = (
            conditions.loc[self.recording["protocol_key"]]
            .reset_index()
            .rename(columns={"code": "condition_id"})
            .set_index("condition_id")
        )
        stimulus = GratingStimulus.from_dataframes(self.protocol, self.conditions)

        self.properties_estimator = GratingsPropertiesEstimator(
            stimulus,
            gauss_window=self.kernel_window,
            spike_sampling=self.spike_sampling,
        )

        self.spike_times = self._load_spikes_dataset(job.timestamps_hdf, job.channel)
        self.labels = self._load_sorting_dataset(job.labels_hdf, channel=job.channel)
        self.trials = self._load_trials_dataset(job.trials_hdf_path)

    def run(self) -> Iterable[Tuple[str, GratingsProperties]]:
        unit_labels_and_masks = [
            ("Multi-unit", Ellipsis),
            ("Multi-unit without unsorted", self.labels > 0),
        ] + [
            (
                f"Single-unit {chr(ord('a') + l - 1)}" if l > 0 else "Unsorted",
                self.labels == l,
            )
            for l in np.unique(self.labels)
        ]

        unit_estimates = [
            (
                unit_label,
                self.properties_estimator.estimate(
                    self.spike_times[mask], self.trials, self.background_estimator,
                ),
            )
            for unit_label, mask in tqdm(
                unit_labels_and_masks,
                desc=f"Estimating properties for {self.recording_title}",
                leave=False,
            )
        ]

        return unit_estimates

    def run_and_save(self) -> Dict[str, GratingsProperties]:
        unit_estimates = self.run()

        results = {}
        for unit_name, unit_estimate in unit_estimates:
            results[unit_name] = unit_estimate
            self._save_unit_estimate(unit_estimate, unit_name)

        return results

    def run_and_plot(self) -> List[Tuple[str, GratingsProperties]]:
        results = self.run()

        # plt.switch_backend("TKAgg")
        plt.style.use("ggplot")
        units.setup_matplotlib()

        raise Exception(f"{self.__class__.__name__}.run_and_plot() not implemented")

        plt.show()

        return results

    def _load_trials_dataset(self, hdf_path: Path):
        with h5.File(DATASET_ROOT / hdf_path) as metadata_file:
            trials_array = np.array(metadata_file["trials"])

        trials = pd.DataFrame.from_records(trials_array).rename(
            columns={"condition": "condition_id"}
        )

        trials["rank"] = trials.groupby("condition_id")[["start"]].rank()
        trials["start"] = quantity_to_pint_array(
            trials["start"].values / self.timestamp_sampling
        )
        trials["stop"] = quantity_to_pint_array(
            trials["stop"].values / self.timestamp_sampling
        )
        trials["duration"] = quantity_to_pint_array(
            trials["duration"].values / self.timestamp_sampling
        )

        return trials.convert_dtypes()

    def _load_sorting_dataset(
        self, hdf: HdfResource, channel: int
    ) -> NDArray[(Any,), UInt8]:
        dataset_name = f"{hdf.hdf_group}/{hdf.hdf_prefix}{channel:02d}/labels"
        with h5.File(DATASET_ROOT / hdf.hdf_path) as sorting_file:
            labels = np.array(sorting_file[dataset_name])
        return labels

    def _load_spikes_dataset(self, hdf: HdfResource, channel: int) -> Time:
        with h5.File(DATASET_ROOT / hdf.hdf_path) as detection_file:
            timestamps = np.array(
                detection_file[f"{hdf.hdf_group}/{hdf.hdf_prefix}{channel:02d}"][
                    "timestamps"
                ]
            )
        spike_times = timestamps / self.timestamp_sampling
        return spike_times

    def _save_unit_estimate(self, unit_estimate: GratingsProperties, unit_name: str):
        self.output_path.parent.mkdir(parents=True, exist_ok=True)
        with _hdf_lock, h5.File(self.output_path, "a") as h5_file:
            estimate_group = "%s/%s" % (
                self.output_group,
                re.sub(r"[\s-]", "_", unit_name.upper()),
            )
            estimate_saver = Hdf5DataSaver(h5_file.require_group(estimate_group))
            estimate_saver.save_attributes(unit_estimate.attributes)
            estimate_saver.save_tables(unit_estimate.tables)


def select_jobs(*recording_titles: str) -> List[EstimationJob]:
    metadata = select_metadata(*recording_titles)
    jobs = [
        job_for(row, channel)
        for _, row in metadata.iterrows()
        for channel in range(1, row["recording_channels"] + 1)
    ]
    return jobs


def select_metadata(*recording_titles: str) -> pd.DataFrame:
    properties = (
        load_derived_entity("property", subtypes=True)
        .query("type == 'GRATINGS'")
        .dropna(axis=1)
    )
    new_sortings = load_derived_entity("new_sorting", subtypes=True)
    detections = load_derived_entity("detection")

    sources = load_original_entity("source")
    recordings = load_original_entity("recording")

    full_df = fk_join(
        properties, new_sortings, detections, sources, recordings
    ).convert_dtypes()

    if recording_titles:
        final_df = full_df[full_df["recording_title"].isin(recording_titles)]
    else:
        final_df = full_df

    return final_df


def job_for(row: pd.Series, channel: int) -> EstimationJob:
    job = EstimationJob(
        recording_title=row["recording_title"],
        channel=channel,
        timestamps_hdf=HdfResource(
            hdf_path=derived_data_path(row["detection_hdf_path"]),
            hdf_group=row["detection_hdf_group"],
            hdf_prefix=row["detection_hdf_prefix"],
        ),
        labels_hdf=HdfResource(
            hdf_path=derived_data_path(row["new_sorting_hdf_path"]),
            hdf_group=row["new_sorting_hdf_group"],
            hdf_prefix=row["new_sorting_hdf_prefix"],
        ),
        trials_hdf_path=original_data_path(row["recording_hdf_path"]).with_suffix(
            ".hdf"
        )
        / "metadata.h5",
        output_hdf=HdfResource(
            hdf_path=derived_data_path(row["hdf_path"]),
            hdf_group=row["hdf_group"],
            hdf_prefix=row["hdf_prefix"],
        ),
        kernel_window_in_ms=row["kernel_window_in_ms"],
        background_estimator=BackgroundActivityEstimator(row["background_estimator"]),
    )

    return job


def run_job(job: EstimationJob):
    Estimator(job).run_and_save()


if __name__ == "__main__":
    main()
