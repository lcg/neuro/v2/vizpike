from functools import lru_cache
from typing import Any, Dict, List, Sequence, Tuple, Union

import attr
import h5py as h5
import numpy as np
import pandas as pd
from nptyping import NDArray
from pandas import Int8Dtype, Int16Dtype, Int32Dtype, Int64Dtype, StringDtype
from pint import Quantity
from scipy import integrate
from scipy.ndimage import convolve1d

from vizpike.pipeline.dataset import units
from vizpike.pipeline.sigproc import (
    BackgroundActivityEstimator,
    gaussian_kernel,
    spike_matrix,
)
from vizpike.pipeline.typeann import Time, TimeFrequency


@attr.s(auto_attribs=True)
class GratingsProperties:
    attributes: dict
    tables: Dict[str, np.ndarray]


@attr.s(auto_attribs=True)
class GratingStimulus:
    protocol: pd.DataFrame
    conditions: pd.DataFrame

    @classmethod
    def from_dataframes(
        cls, protocol: pd.DataFrame, conditions: pd.DataFrame
    ) -> "GratingStimulus":
        return GratingStimulus(protocol, conditions)


@attr.s(auto_attribs=True)
class GratingsPropertiesEstimator:
    stimulus: GratingStimulus
    gauss_window: Time
    spike_sampling: TimeFrequency

    def estimate(
        self,
        spike_times: Union[NDArray[(Any,), float], Quantity],
        trials: pd.DataFrame,
        background_estimator: BackgroundActivityEstimator,
    ) -> GratingsProperties:
        import warnings

        warnings.simplefilter("ignore")

        hz = units.hertz

        base_rate = background_estimator(
            spike_times,
            trials,
            self.stimulus.conditions,
            self.gauss_window,
            time_sampling=self.spike_sampling,
        )
        condition_ids, spike_trains = spike_matrix(
            spike_times,
            trials,
            self.stimulus.conditions,
            sampling_rate=self.spike_sampling,
            where="condition_is_stimulus & ~condition_is_neutral",
            group_by="condition_id",
        )
        conditions = self.stimulus.conditions.loc[condition_ids]

        firing_rates, responses = self.firing_rates_and_responses(spike_trains)
        (
            active_firing_rates,
            active_responses,
        ) = self.active_firing_rates_and_responses(base_rate, firing_rates)

        properties = np.empty(
            len(conditions),
            dtype=[
                ("code", "u2"),
                ("spike_counts", f"{spike_trains.shape[1:2]!r}i4"),
                ("firing_rate_in_hz", f"{firing_rates.shape[1:]}f4"),
                ("active_firing_rate_in_hz", f"{active_firing_rates.shape[1:]}f4",),
                ("response", "f4"),
                ("active_response", "f4"),
            ],
        )

        conditions["spike_counts"] = np.sum(spike_trains, axis=2).tolist()
        conditions["firing_rate_in_hz"] = firing_rates.to("hertz").m.tolist()
        conditions["active_firing_rate_in_hz"] = active_firing_rates.to(
            "hertz"
        ).m.tolist()
        conditions["response"] = responses
        conditions["active_response"] = active_responses

        properties["code"] = conditions.index
        properties["spike_counts"] = np.vstack(conditions["spike_counts"].values)
        properties["firing_rate_in_hz"] = np.vstack(
            conditions["firing_rate_in_hz"].values
        )
        properties["active_firing_rate_in_hz"] = np.vstack(
            conditions["active_firing_rate_in_hz"].values
        )
        properties["response"] = np.array(conditions["response"].values)
        properties["active_response"] = np.array(conditions["active_response"].values)

        directional_properties = DirectionalAndOrientationalProperties(conditions)

        attributes = {
            "base_rate": base_rate,
            **directional_properties.global_pref_cv_index_for_direction_and_orientation(
                "response"
            ),
            **directional_properties.global_pref_cv_index_for_direction_and_orientation(
                "active_response", "active_"
            ),
        }

        local_indices = pd.concat(
            [
                directional_properties.local_pref_cv_index_for_direction_and_orientation(
                    "response"
                ),
                directional_properties.local_pref_cv_index_for_direction_and_orientation(
                    "active_response", "active_"
                ),
            ],
            axis=1,
        )

        local_indices_array = self._bigass_messy_conversion_to_please_h5py(
            local_indices
        )

        dataframes = {
            "properties": properties,
            "local_indices": local_indices_array,
        }

        return GratingsProperties(attributes, dataframes)

    @staticmethod
    def _bigass_messy_conversion_to_please_h5py(
        local_indices: pd.DataFrame,
    ) -> np.ndarray:
        """This is messy and ugly, but here's the point: h5py can't handle UTF-8 strings very well, specially if they're
        columns in a record array. I read the docs, searched Github and banged my head against the wall for hours. So
        here's the deal: I auto-convert data-types in the input dataframe to get a pandas.StringDtype dtype on string
        columns. Then, I encode those columns as UTF-8 whilst creating a conversion map to take these columns into
        carefully-sized Numpy S-dtypes, and also filling NAs and registering int columns for a float-conversion (again, to dodge np.dtype('O')). Finally, I convert the dataframe into an array twice (the first pass to get
        general dtypes and the second one to convert just the necessary ones). Why not simply converting from pandas
        using pandas.Dataframe.astype()? Because the changed types are note preserved upon calling to_recordds(). Is it
        ugly? Hell yes. Do I look like I care?
        """
        local_indices_converted = local_indices.reset_index().convert_dtypes()

        converted_cols = {}
        for col in local_indices_converted.columns:
            if type(local_indices_converted[col].dtype) is StringDtype:
                local_indices_converted[col] = (
                    local_indices_converted[col]
                    .apply(lambda s: s.encode("utf-8"))
                    .fillna(b"")
                )
                converted_cols[col] = np.dtype(
                    f"<S{max(local_indices_converted[col].str.len())}"
                )
            elif type(local_indices_converted[col].dtype) in [
                Int8Dtype,
                Int16Dtype,
                Int32Dtype,
                Int64Dtype,
            ]:
                local_indices_converted[col] = local_indices_converted[col].fillna(
                    np.nan
                )
                converted_cols[col] = np.dtype("f4")

        local_indices_array = local_indices.reset_index().to_records()
        local_indices_array = (
            local_indices.reset_index()
            .to_records()
            .astype(
                [
                    (name, converted_cols[name] if name in converted_cols else dtype,)
                    for name, (dtype, _,) in local_indices_array.dtype.fields.items()
                ]
            )
        )

        return local_indices_array

    def active_firing_rates_and_responses(
        self, base_rate: Quantity, firing_rates: NDArray[(Any, Any), float]
    ) -> Tuple[NDArray[(Any, Any), float], NDArray[(Any,), float]]:
        hz = units.hertz

        active_firing_rates = np.clip(firing_rates - base_rate, 0 * hz, np.inf * hz,)
        active_responses = integrate.trapz(
            active_firing_rates, dx=1 / self.spike_sampling, axis=1
        )

        return active_firing_rates, active_responses

    def firing_rates_and_responses(
        self, spike_trains
    ) -> Tuple[NDArray[(Any, Any), float], NDArray[(Any,), float]]:
        hz = units.hertz

        mean_spike_trains = np.mean(spike_trains, axis=1)
        firing_rates = (
            convolve1d(
                mean_spike_trains,
                gaussian_kernel(self.gauss_window, self.spike_sampling)
                .to(hz)
                .magnitude,
            )
            * hz
        )
        responses = integrate.trapz(firing_rates, dx=1 / self.spike_sampling, axis=1)

        return firing_rates, responses


class DirectionalAndOrientationalProperties:
    def __init__(self, conditions: pd.DataFrame):
        self.conditions = conditions
        self.orientation_keys = (
            "orientation_pref",
            "orientation_cv",
            "orientation_index",
        )
        self.direction_keys = (
            "direction_pref",
            "direction_cv",
            "direction_index",
        )

    def global_pref_cv_index_for_direction_and_orientation(
        self, response_column: str, key_prefix=""
    ) -> Dict[str, float]:
        direction_values = pref_cv_index_for_direction(
            self.conditions.groupby("direction_in_deg")[[response_column]].sum(),
            response_column,
        )
        orientation_values = pref_cv_index_for_orientation(
            self.conditions.groupby("orientation_in_deg")[[response_column]].sum(),
            response_column,
        )
        attributes = {
            **dict(
                zip(
                    self.prefixed_keys(self.orientation_keys, key_prefix),
                    orientation_values,
                )
            ),
            **dict(
                zip(
                    self.prefixed_keys(self.direction_keys, key_prefix),
                    direction_values,
                )
            ),
        }
        return attributes

    @lru_cache()
    def prefixed_keys(self, keys: Sequence[str], key_prefix: str) -> List[str]:
        return [f"{key_prefix}{key}" for key in keys]

    def local_pref_cv_index_for_direction_and_orientation(
        self, response_column: str, key_prefix=""
    ) -> pd.DataFrame:
        direction_keys = self.prefixed_keys(self.direction_keys, key_prefix)
        orientation_keys = self.prefixed_keys(self.orientation_keys, key_prefix)

        parameters = [
            "contrast",
            "spatial_freq_in_invdeg",
            "speed_in_hz",
            "bg_color_in_hex",
            "fg_color_in_hex",
        ]
        local_indices = self.conditions.groupby(parameters)[[response_column]].max()
        local_indices[direction_keys] = 0
        local_indices[orientation_keys] = 0

        conditions_direction = self.conditions.set_index(
            parameters + ["direction_in_deg"]
        )
        conditions_orientation = self.conditions.set_index(
            parameters + ["orientation_in_deg"]
        )
        for idx in local_indices.index:
            local_indices.loc[idx, direction_keys] = pref_cv_index_for_direction(
                conditions_direction.loc[idx], response_column
            )
            local_indices.loc[idx, orientation_keys] = pref_cv_index_for_orientation(
                conditions_orientation.loc[idx], response_column
            )

        return local_indices


def group_indices(
    conditions: pd.DataFrame, group_conditions: pd.DataFrame, group: str
) -> dict:
    filtered_conditions = conditions.loc[
        group_conditions.query(f"group == {group!r}")["condition_id"].values
    ]

    orientation_keys = "orientation_pref orientation_cv orientation_index".split()
    direction_keys = "direction_pref direction_cv direction_index".split()

    orientation_values = pref_cv_index_for_orientation(filtered_conditions)
    direction_values = pref_cv_index_for_direction(filtered_conditions)

    result = {
        **{"group": group},
        **dict(zip(orientation_keys, orientation_values)),
        **dict(zip(direction_keys, direction_values)),
    }

    return result


def pref_cv_index_for_direction(
    conditions: pd.DataFrame, response_column: str
) -> Tuple[Quantity, float, float]:
    directions = np.array(conditions.index).astype(float)
    responses = conditions[response_column].values
    circular_variance = 1 - np.abs(
        np.sum(responses * np.exp(1j * directions / 180)) / np.sum(responses)
    )

    pref_direction = conditions[response_column].idxmax()
    perp_direction = (pref_direction + 90) % 360
    pref_response = np.array(conditions.loc[pref_direction, response_column])
    perp_response = np.array(conditions.loc[perp_direction, response_column])
    direction_index = np.mean(
        (pref_response - perp_response) / (pref_response + perp_response)
    )

    return pref_direction, circular_variance, direction_index


def pref_cv_index_for_orientation(
    conditions: pd.DataFrame, response_column: str
) -> Tuple[Quantity, float, float]:
    orientations = np.array(conditions.index).astype(float)
    responses = conditions[response_column].values
    circular_variance = 1 - np.abs(
        np.sum(responses * np.exp(2j * orientations / 180)) / np.sum(responses)
    )

    pref_orientation = conditions[response_column].idxmax()
    perp_orientation = (pref_orientation + 90) % 180
    pref_response = np.array(conditions.loc[pref_orientation, response_column])
    perp_response = np.array(conditions.loc[perp_orientation, response_column])
    direction_index = np.mean(
        (pref_response - perp_response) / (pref_response + perp_response)
    )

    return pref_orientation, circular_variance, direction_index


def groupby_matrix(
    dataframe: pd.DataFrame, group_key: str, column: str
) -> Tuple[NDArray[(Any,)], NDArray[(Any, Any)]]:
    group_keys = np.hstack(sorted(dataframe[group_key].unique()))
    group_matrix = np.vstack(
        [group[column].values.quantity for _, group in dataframe.groupby(group_key)]
    )
    return group_keys, group_matrix
