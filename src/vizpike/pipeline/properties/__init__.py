from typing import Any, Dict, Tuple, Union

import attr
import h5py as h5
import numpy as np
from pint import Quantity, Unit


@attr.s(auto_attribs=True)
class Hdf5DataSaver:
    h5_group: h5.Group

    def save_attribute(
        self,
        name: str,
        value: Union[int, float, str, Quantity],
        *,
        unit: Union[str, Unit] = None,
    ) -> Tuple[str, Union[int, float, str]]:
        try:
            saved_unit = str(unit or value.units)
            saved_value = value.to(saved_unit).magnitude
            self.save_attribute(f"{name}_unit", str(saved_unit))
        except AttributeError:
            saved_value = value

        self.h5_group.attrs[name] = saved_value
        return name, saved_value

    def save_attributes(
        self, attributes: Dict[str, Union[int, float, str, Quantity]]
    ) -> Dict[str, Union[int, float, str]]:
        saved_attrs = dict(
            [self.save_attribute(name, value) for name, value in attributes.items()]
        )
        return saved_attrs

    def save_dataset(
        self,
        name: str,
        data: Any,
        *,
        unit: Union[str, Unit] = None,
        compressed=False,
        vlen_dtype=None,
    ) -> h5.Dataset:
        options = {}

        if compressed:
            options.update({"chunks": True, "compression": "gzip"})

        if unit is None:
            saved_data = data
        else:
            self.save_attribute(f"{name}_unit", str(unit))
            if vlen_dtype is None:
                saved_data = data.to(unit).magnitude
            else:
                saved_data = [row.to(unit).magnitude for row in data]

        if vlen_dtype is None:
            dataset = self.h5_group.create_dataset(name, data=saved_data, **options)
        else:
            dataset = self.h5_group.create_dataset(
                name,
                shape=(len(saved_data),),
                dtype=h5.vlen_dtype(np.dtype(vlen_dtype)),
                **options,
            )
            dataset[...] = saved_data

        return dataset

    def save_table(self, name: str, table: np.ndarray) -> h5.Dataset:
        dataset = self.h5_group.create_dataset(name, data=table)
        return dataset

    def save_tables(self, tables: Dict[str, np.ndarray]) -> Dict[str, h5.Dataset]:
        datasets = {
            name: self.save_table(name, table) for name, table in tables.items()
        }
        return datasets
