from enum import Enum
from typing import Union

from pydantic import BaseModel

from vizpike.pipeline.dataset import HdfResource


class DspFilteringMode(Enum):
    LEFT2RIGHT = "LEFT2RIGHT"
    BIDIRECTIONAL = "BIDIRECTIONAL"


class DspFilterType(Enum):
    BESSEL = "BESSEL"


class WaveformAlignment(Enum):
    THRESHOLD_CROSSING = "THRESHOLD_CROSSING"
    FIRST_VALLEY = "FIRST_VALLEY"


class DetectionType(Enum):
    OMNIPLEX = "OMNIPLEX"
    OMNIPLEX_RE_SPKC = "OMNIPLEX_RE_SPKC"
    OMNIPLEX_RE_WB = "OMNIPLEX_RE_WB"


class CommonParams(BaseModel):
    sampling_in_hz: int
    waveform_alignment: WaveformAlignment
    waveform_samples: int
    max_overlapping_samples: int
    samples_before_alignment: int


class OmniplexParams(CommonParams):
    pass


class OmniplexReParams(CommonParams):
    threshold_in_stdev: float


class OmniplexReSpkcParams(OmniplexReParams):
    pass


class OmniplexReWbParams(OmniplexReParams):
    dsp_filter_highpass_cutoff_in_hz: float
    dsp_filtering_mode: DspFilteringMode
    dsp_filter_order: int
    dsp_filter_type: DspFilterType


class DetectionTask(BaseModel):
    input: HdfResource
    output: HdfResource
    params: Union[OmniplexReWbParams, OmniplexReSpkcParams, OmniplexParams]
    type: DetectionType
