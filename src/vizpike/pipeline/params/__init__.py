from typing import Dict, List

import yaml
from pydantic import BaseModel

from vizpike.pipeline.dataset import DATASET_ROOT
from vizpike.pipeline.params.detection import DetectionTask


class Recording(BaseModel):
    channels: int
    detections: List[DetectionTask]


def load_dvc_params() -> Dict[str, Recording]:
    with open(DATASET_ROOT / "params.yaml") as params_file:
        params = yaml.safe_load(params_file)
    mapping = {key: Recording(**obj) for key, obj in params.items()}
    return mapping
