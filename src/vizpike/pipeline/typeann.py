from typing import Any

import pandas as pd
from nptyping import NDArray, UInt64
from pint import Quantity

# class QuantityArrayType:
#     def __getitem__(self, shape, scalar, unit):
#         return Union[Quantity, NDArray[shape, scalar]]
#
# QuantityArray = QuantityArrayType()
# TimestampVector = QuantityArray[(Any,), float, "tick"]
# TimestampMatrix = QuantityArray[(Any, Any), float, "tick"]

TimeFrequency = Quantity
ResponseMap = NDArray[(Any, Any), float]
SpikeTrain = NDArray[(Any), int]
SpikeTrains = NDArray[(Any, Any), int]
SpikeMatrix = NDArray[(Any, Any, Any), int]
FiringRate = NDArray[(Any,), float]
Timestamp = Quantity
Timestamps = NDArray[(Any,), UInt64]
TimeSupport = NDArray[(Any,), float]
Time = Quantity
Tick = Quantity
VisualFieldPoint = NDArray[(2,), float]
FunctionalProperties = pd.DataFrame
